package config;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Antinomy on 16/12/1.
 */
public class ThreadPool {

    private static final int poolNum= 64;

    private static final ExecutorService executor = Executors.newFixedThreadPool(poolNum);

    public static void execute(Runnable command) {
         executor.execute(command);
    }
}
