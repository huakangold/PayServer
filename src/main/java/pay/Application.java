package pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import pay.common.BaseRepositoryFactoryBean;

/**
 * 
 * Title:Application Description: Company:HK
 * 
 * @author Sam
 * @date 2016年7月14日下午2:51:27
 */
@SpringBootApplication
@EnableJpaRepositories(repositoryFactoryBeanClass = BaseRepositoryFactoryBean.class)
@EnableScheduling
@EnableDiscoveryClient
@EnableAsync
public class Application {
	/**
	 * @SpringBootApplication 
	 *                        相当于@EnableAutoConfiguration、@ComponentScan和@Configuration的合集。
	 * @Configuration 告诉Spring这是一个配置类，里面的所有标注了@Bean的方法的返回值将被注册为一个Bean
	 * @EnableAutoConfiguration 告诉Spring基于class path的设置、其他bean以及其他设置来为应用添加各种Bean
	 * @ComponentScan 告诉Spring扫描Class path下所有类来生成相应的Bean
	 * @EnableScheduling 告诉Spring创建一个task executor，如果我们没有这个标注，所有@Scheduled标注都不会执行
	 *
	 */
	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

	@Bean	@LoadBalanced	
	RestTemplate restTemplate() {return new RestTemplate();	}
	
	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory() {
		return new HibernateJpaSessionFactoryBean();
	}
}
