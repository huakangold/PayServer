package pay.base;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pay.portal.web.message.SessionConstants;
import pay.portal.web.message.Token;



public class BaseCtrl {
	protected Logger logger = LoggerFactory
			.getLogger(this.getClass().getName());
	
	public final static String MgrRootPath = "/payServer/mgr";
	public final static String CommonRootPath = "/payServer/common";
	public final static String App = "/payServer/app";

	protected HttpServletRequest getRequest() {

		return ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
	}

	protected HttpSession getSession() {

		return getRequest().getSession(true);
	}
	
	protected Token getToken() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		return token;
	}
	
	protected Long getCurrentTime() {
		return System.currentTimeMillis();
	}
	
	protected Long getUserId() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		if(token==null)
			return 1l;
		return token.getId();
	}
	
	protected String getUserName() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		return token.getName();
	}
	
	protected String getAccount() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		if(token==null)
			return "admin";
		return token.getName();
	}
	
	protected List<String> getTypeAndFlag(){
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		return token.getTypeAndFlag();
	}
	
	protected String getRoleIds() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		if(token==null)
			return "";
		return token.getRoleIds();
	}
	
	protected Long getDeptId() {
		Token token =  (Token) getSession().getAttribute(SessionConstants.Token);
		if(token==null)
			return 0l;
		return token.getDeptId();
	}
}
