package pay.base;

import java.io.Serializable;
import java.util.Collection;

import pay.common.ResultCode;

 



public class Response<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9070633319859110370L;

	private String resultCode;

	private String msg;

	private T result;

	private long size;
	
	
	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getResultCode() {
		if (result instanceof Collection<?>) {
			if (((Collection<?>) result).size() == 0) {
				this.resultCode = ResultCode.NO_DATA.getCode();
			} else {
				this.resultCode = ResultCode.SUCC.getCode();
			}

		} else if (result instanceof Boolean) {
			if ((Boolean) result) {
				this.resultCode = ResultCode.SUCC.getCode();
			} else {
				this.resultCode = ResultCode.FAILED.getCode();
			}
		} else if (result instanceof String) {
			if (result.equals("0001")) {
				this.resultCode = ResultCode.FAILED.getCode();
			} else {
				this.resultCode = ResultCode.SUCC.getCode();
			}
		}
		
		
		return resultCode;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getMsg() {
		return msg;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
	public void setResultCode(Integer resultCode) {
		this.resultCode = String.valueOf(resultCode);
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
