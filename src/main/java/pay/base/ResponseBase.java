package pay.base;

import java.io.Serializable;

import pay.common.ResultCode;
import pay.utils.StringHelper;


public class ResponseBase<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9070633319859110370L;

	private String resultCode;

	private String msg = "";

	private T result;

	private long size;

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getResultCode() {
		return resultCode;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public String getMsg() {
		if(StringHelper.isNotEmpty(this.msg)){
			msg =  this.msg;
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.SUCC.getCode() )){
			msg =   ResultCode.SUCC.getMsg();
		
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.FAILED.getCode() )){
			msg =   ResultCode.FAILED.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.PARAM_ERROR.getCode() )){
			msg =   ResultCode.PARAM_ERROR.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.NO_DATA.getCode() )){
			msg =   ResultCode.NO_DATA.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.NO_RIGHT.getCode() )){
			msg =   ResultCode.NO_RIGHT.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.OTHER.getCode() )){
			msg =   ResultCode.OTHER.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.USER_EXIST.getCode() )){
			msg =  ResultCode.USER_EXIST.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.PHONE_CHANGE.getCode() )){
			msg =   ResultCode.PHONE_CHANGE.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.USER_NOT_EXIST.getCode() )){
			msg =   ResultCode.USER_NOT_EXIST.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.ILLEGAL_TOKEN.getCode() )){
			msg =   ResultCode.ILLEGAL_TOKEN.getMsg();	
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.PARAM_LACK.getCode() )){
			msg =   ResultCode.PARAM_LACK.getMsg();
			
		}else if(this.resultCode.equalsIgnoreCase(ResultCode.IREECT_SIGN.getCode() )){
			msg =   ResultCode.IREECT_SIGN.getMsg();	
		}
		
		return msg;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public void setResultCode(Integer resultCode) {
		this.resultCode = String.valueOf(resultCode);
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
