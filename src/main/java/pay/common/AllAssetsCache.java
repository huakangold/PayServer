package pay.common;

import java.math.BigDecimal;

/**
 * Created by lipei on 2017/9/19.
 */
public class AllAssetsCache {

    int virtualAccount = 0;

    BigDecimal virtualLeftMoney = BigDecimal.valueOf(0.00);

    BigDecimal historyProfits = BigDecimal.valueOf(0.00);

    BigDecimal payAmounts = BigDecimal.valueOf(0.00);

    BigDecimal allAssets = BigDecimal.valueOf(0.00);



    int refreshInterval = 60;

    private String scale =  "0%,0%, 0%, 0%，0%";


    public int fixedAssetsCounts = 0;


    public BigDecimal fixedAssetsSuccessAmts =  BigDecimal.valueOf(0.00);


    public BigDecimal fixedAssetsCurrentDividends = BigDecimal.valueOf(0.00);

    private int fundsAllCount = 0;

    private BigDecimal allFundsMoney =  BigDecimal.valueOf(0.00);

    private BigDecimal fundConfirmAllProfit =  BigDecimal.valueOf(0.00);


    private int brokerAllCount = 0;

    private BigDecimal  allBrokerMoney = new BigDecimal("0.00");

    private BigDecimal brokerConfirmAllProfit =  new BigDecimal("0.00");


    private BigDecimal allYingMiWallet;

    private BigDecimal YingMiWalletConfirmAllProfit;

    public BigDecimal getAllBrokerMoney() {
        return allBrokerMoney;
    }

    public void setAllBrokerMoney(BigDecimal allBrokerMoney) {
        this.allBrokerMoney = allBrokerMoney;
    }

    public int getBrokerAllCount() {
        return brokerAllCount;
    }

    public void setBrokerAllCount(int brokerAllCount) {
        this.brokerAllCount = brokerAllCount;
    }

    public BigDecimal getBrokerConfirmAllProfit() {
        return brokerConfirmAllProfit;
    }

    public void setBrokerConfirmAllProfit(BigDecimal brokerConfirmAllProfit) {
        this.brokerConfirmAllProfit = brokerConfirmAllProfit;
    }

    public BigDecimal getAllAssets() {
        return allAssets;
    }

    public void setAllAssets(BigDecimal allAssets) {
        this.allAssets = allAssets;
    }

    public BigDecimal getAllFundsMoney() {
        return allFundsMoney;
    }

    public void setAllFundsMoney(BigDecimal allFundsMoney) {
        this.allFundsMoney = allFundsMoney;
    }

    public int getFixedAssetsCounts() {
        return fixedAssetsCounts;
    }

    public void setFixedAssetsCounts(int fixedAssetsCounts) {
        this.fixedAssetsCounts = fixedAssetsCounts;
    }

    public BigDecimal getFixedAssetsCurrentDividends() {
        return fixedAssetsCurrentDividends;
    }

    public void setFixedAssetsCurrentDividends(BigDecimal fixedAssetsCurrentDividends) {
        this.fixedAssetsCurrentDividends = fixedAssetsCurrentDividends;
    }

    public BigDecimal getFixedAssetsSuccessAmts() {
        return fixedAssetsSuccessAmts;
    }

    public void setFixedAssetsSuccessAmts(BigDecimal fixedAssetsSuccessAmts) {
        this.fixedAssetsSuccessAmts = fixedAssetsSuccessAmts;
    }

    public BigDecimal getFundConfirmAllProfit() {
        return fundConfirmAllProfit;
    }

    public void setFundConfirmAllProfit(BigDecimal fundConfirmAllProfit) {
        this.fundConfirmAllProfit = fundConfirmAllProfit;
    }

    public int getFundsAllCount() {
        return fundsAllCount;
    }

    public void setFundsAllCount(int fundsAllCount) {
        this.fundsAllCount = fundsAllCount;
    }

    public BigDecimal getHistoryProfits() {
        return historyProfits;
    }

    public void setHistoryProfits(BigDecimal historyProfits) {
        this.historyProfits = historyProfits;
    }

    public BigDecimal getPayAmounts() {
        return payAmounts;
    }

    public void setPayAmounts(BigDecimal payAmounts) {
        this.payAmounts = payAmounts;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public int getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(int virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public BigDecimal getVirtualLeftMoney() {
        return virtualLeftMoney;
    }

    public void setVirtualLeftMoney(BigDecimal virtualLeftMoney) {
        this.virtualLeftMoney = virtualLeftMoney;
    }

    public String getScale() {
        return scale;
    }


    public void setScale(String scale) {
        this.scale = scale;
    }

    public BigDecimal getAllYingMiWallet() {
        return allYingMiWallet;
    }

    public void setAllYingMiWallet(BigDecimal allYingMiWallet) {
        this.allYingMiWallet = allYingMiWallet;
    }

    public BigDecimal getYingMiWalletConfirmAllProfit() {
        return YingMiWalletConfirmAllProfit;
    }

    public void setYingMiWalletConfirmAllProfit(BigDecimal yingMiWalletConfirmAllProfit) {
        YingMiWalletConfirmAllProfit = yingMiWalletConfirmAllProfit;
    }

    public AllAssetsDto  toDto( ){
        AllAssetsDto allAssetsDto = new AllAssetsDto();

        allAssetsDto.setVirtualAccount( this.getVirtualAccount());
        allAssetsDto.setVirtualLeftMoney(this.getVirtualLeftMoney());
        allAssetsDto.setHistoryProfits(this.getHistoryProfits());
        allAssetsDto.setPayAmounts(this.getPayAmounts());
        allAssetsDto.setAllAssets(this.getAllAssets());
        allAssetsDto.setRefreshInterval(this.getRefreshInterval());
        allAssetsDto.setScale(this.getScale());

        FixedAssetsDto fixedAssetsDto = new FixedAssetsDto();

        fixedAssetsDto.setFixedAssetsCounts(this.getFixedAssetsCounts());
        fixedAssetsDto.setFixedAssetsSuccessAmts(this.getFixedAssetsSuccessAmts());
        fixedAssetsDto.setFixedAssetsCurrentDividends(this.getFixedAssetsCurrentDividends());

        FundsDto fundsDto = new FundsDto();

        fundsDto.setFundsAllCount(this.getFundsAllCount());
        fundsDto.setAllFundsMoney(this.getAllFundsMoney());
        fundsDto.setFundConfirmAllProfit(this.getFundConfirmAllProfit());

        BrokerDto brokerDto = new BrokerDto();
        brokerDto.setBrokerAllCount(this.getBrokerAllCount());
        brokerDto.setBrokerConfirmAllProfit(this.getBrokerConfirmAllProfit());
        brokerDto.setAllBrokerMoney(this.getAllBrokerMoney());


        YingMiWalletDTO walletDTO = new YingMiWalletDTO();
        walletDTO.setYingMiWalletConfirmAllProfit(this.getYingMiWalletConfirmAllProfit());
        walletDTO.setAllYingMiWallet(this.getAllYingMiWallet());


        allAssetsDto.setFixedAssets(fixedAssetsDto);
        allAssetsDto.setFunds(fundsDto);
        allAssetsDto.setBrokerDto(brokerDto);
        allAssetsDto.setWalletDTO(walletDTO);

        return allAssetsDto;
    }

}
