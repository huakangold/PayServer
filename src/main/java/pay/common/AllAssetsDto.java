package pay.common;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by lipei on 2017/9/19.
 */
public class AllAssetsDto {
    
    
    int virtualAccount = 0;

    BigDecimal virtualLeftMoney = BigDecimal.valueOf(0.00);

    BigDecimal historyProfits = BigDecimal.valueOf(0.00);

    BigDecimal payAmounts = BigDecimal.valueOf(0.00);

    BigDecimal allAssets = BigDecimal.valueOf(0.00);

    int refreshInterval =  60;

    private FixedAssetsDto fixedAssets = new FixedAssetsDto();

    private FundsDto funds = new FundsDto();

    private BrokerDto brokerDto = new BrokerDto();

    private YingMiWalletDTO walletDTO = new YingMiWalletDTO();

    private String scale =  "0%,0%,0%,0%,0%";

    public BigDecimal getAllAssets() {
        return allAssets;
    }

    public void setAllAssets(BigDecimal allAssets) {
        this.allAssets = allAssets;
    }

    public FixedAssetsDto getFixedAssets() {
        return fixedAssets;
    }

    public void setFixedAssets(FixedAssetsDto fixedAssets) {
        this.fixedAssets = fixedAssets;
    }

    public FundsDto getFunds() {
        return funds;
    }

    public void setFunds(FundsDto funds) {
        this.funds = funds;
    }

    public BigDecimal getHistoryProfits() {
        return historyProfits;
    }

    public void setHistoryProfits(BigDecimal historyProfits) {
        this.historyProfits = historyProfits;
    }

    public BigDecimal getPayAmounts() {
        return payAmounts;
    }

    public void setPayAmounts(BigDecimal payAmounts) {
        this.payAmounts = payAmounts;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public int getVirtualAccount() {
        return virtualAccount;
    }

    public void setVirtualAccount(int virtualAccount) {
        this.virtualAccount = virtualAccount;
    }

    public BigDecimal getVirtualLeftMoney() {
        return virtualLeftMoney;
    }

    public void setVirtualLeftMoney(BigDecimal virtualLeftMoney) {
        this.virtualLeftMoney = virtualLeftMoney;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }


    public BrokerDto getBrokerDto() {
        return brokerDto;
    }

    public void setBrokerDto(BrokerDto brokerDto) {
        this.brokerDto = brokerDto;
    }

    public YingMiWalletDTO getWalletDTO() {
        return walletDTO;
    }

    public void setWalletDTO(YingMiWalletDTO walletDTO) {
        this.walletDTO = walletDTO;
    }

    public AllAssetsCache  toCache(){
        AllAssetsCache allAssetsCache = new AllAssetsCache();
        allAssetsCache.setVirtualAccount( this.getVirtualAccount());
        allAssetsCache.setVirtualLeftMoney(this.getVirtualLeftMoney());
        allAssetsCache.setHistoryProfits(this.getHistoryProfits());
        allAssetsCache.setPayAmounts(this.getPayAmounts());
        allAssetsCache.setAllAssets(this.getAllAssets());
        allAssetsCache.setRefreshInterval(this.getRefreshInterval());
        allAssetsCache.setScale(this.getScale());

        allAssetsCache.setFixedAssetsCounts(this.getFixedAssets().getFixedAssetsCounts());
        allAssetsCache.setFixedAssetsSuccessAmts(this.getFixedAssets().getFixedAssetsSuccessAmts());
        allAssetsCache.setFixedAssetsCurrentDividends(this.getFixedAssets().getFixedAssetsCurrentDividends());

        //基金
        allAssetsCache.setFundsAllCount(this.getFunds().getFundsAllCount());
        allAssetsCache.setAllFundsMoney(this.getFunds().getAllFundsMoney());
        allAssetsCache.setFundConfirmAllProfit(this.getFunds().getFundConfirmAllProfit());

        //券商类产品
        allAssetsCache.setBrokerAllCount(this.getBrokerDto().getBrokerAllCount());
        allAssetsCache.setAllBrokerMoney(this.getBrokerDto().getAllBrokerMoney());
        allAssetsCache.setBrokerConfirmAllProfit(this.getBrokerDto().getBrokerConfirmAllProfit());

        //华康宝
        allAssetsCache.setAllYingMiWallet(this.getWalletDTO().getAllYingMiWallet());
        allAssetsCache.setYingMiWalletConfirmAllProfit(this.getWalletDTO().getYingMiWalletConfirmAllProfit());

        return allAssetsCache;
    }
}
