package pay.common;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryMetadata;

import pay.serviceImpl.dao.base.impl.BaseRepositoryImpl;

public class BaseRepositoryFactory extends JpaRepositoryFactory {

	public BaseRepositoryFactory(EntityManager entityManager) {
		super(entityManager);
		// TODO Auto-generated constructor stub
	}

	// @Override
	@SuppressWarnings("unchecked")
	protected JpaRepository<?, ?> getTargetRepository(
			RepositoryMetadata metadata, EntityManager em) {

		JpaEntityInformation<Object, Serializable> entityMetadata = mock(JpaEntityInformation.class);
		when(entityMetadata.getJavaType()).thenReturn(
				(Class<Object>) metadata.getDomainType());
		return new BaseRepositoryImpl<Object, Serializable>(entityMetadata, em);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.data.repository.support.RepositoryFactorySupport#
	 * getRepositoryBaseClass()
	 */
	@Override
	protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {

		return BaseRepositoryImpl.class;
	}
}
