package pay.common;

import java.math.BigDecimal;

/**
 * Created by lipei on 2017/8/8.
 */
public class BrokerDto {

    private int brokerAllCount = 0;

    private BigDecimal  allBrokerMoney = new BigDecimal("0.00");

    private BigDecimal brokerConfirmAllProfit =  new BigDecimal("0.00");

    public BigDecimal getAllBrokerMoney() {
        return allBrokerMoney;
    }

    public void setAllBrokerMoney(BigDecimal allBrokerMoney) {
        this.allBrokerMoney = allBrokerMoney;
    }

    public int getBrokerAllCount() {
        return brokerAllCount;
    }

    public void setBrokerAllCount(int brokerAllCount) {
        this.brokerAllCount = brokerAllCount;
    }

    public BigDecimal getBrokerConfirmAllProfit() {
        return brokerConfirmAllProfit;
    }

    public void setBrokerConfirmAllProfit(BigDecimal brokerConfirmAllProfit) {
        this.brokerConfirmAllProfit = brokerConfirmAllProfit;
    }
}
