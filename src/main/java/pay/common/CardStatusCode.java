package pay.common;

public class CardStatusCode {
	public static final int UNBIND_CARD = -1;// 解绑。 适用于盈米，火柴
	public static final int PAYMENT_DISPLAY_SHOW = 1;// 卡状态
	public static final int PAYMENT_DISPLAY_HIDE = 0;// 卡状态
	public static final int FY_CHANGE_CARDING = 10;// 换卡中
	public static final int FY_CARD_WASCHANGED = -10;// 原卡被替换,适用于富友
	public static final int NEVER_BIND_CARD = 0;// 从未绑卡
	public static final int FY_CHANGE_CARD_FAIL = -200;// 换卡失败
	public static final int FY_CHANGE_CARD_withdraw = -1111;// 注销

	
	public static final String PAYMENT_TYPE_CHANGE = "CHANGECARD";//换卡
	public static final String PAYMENT_TYPE_ADD = "ADDCARD";//新增卡
	public static final String PAYMENT_TYPE_UNBIND = "UNBIND";//解卡
	public static final String PAYMENT_TYPE_DELACCOUNT = "DELACCOUNT";//销户
}
