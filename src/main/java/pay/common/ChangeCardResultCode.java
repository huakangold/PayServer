package pay.common;

/**
 * @author Antinomy
 */
public class ChangeCardResultCode {

    public static final int FAILED = 500;// 换卡失败
    public static final int CHANGED = 200;// 换卡成功
    public static final int ORIGINAL = 0;// 未换卡

}
