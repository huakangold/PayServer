package pay.common;

public class Constant {

	public static final int LOGIN_SESSION_TIMEOUT = 8 * 60 * 60;

	/**
	 * 获取获取标识,规格
	 */
	public static final String IDHOLDER_SEEDING_SPEC_CODE = "seeding_spec_code";

	
	/**
	 * 是否热销产品: 1 是
	 */
	public static final int PRODUCT_HOT_SELLING_TRUE = 1;

	/**
	 * 是否热销产品: 0否
	 */
	public static final int PRODUCT_HOT_SELLING_FALSE = 0;
	
	/**
	 * 是否上架销售: 0 不上架, 1 上架 , 2 测试中
	 */
	public static final int PRODUCT_RELEASE_STATE_TEST = 2;
	
	/**
	 * 是否上架销售: 0 不上架, 1 上架 , 2 测试中
	 */
	public static final int PRODUCT_RELEASE_STATE_TRUE = 1;

	/**
	 * 是否上架销售: 0 不上架, 1 上架 , 2 测试中
	 */
	public static final int PRODUCT_RELEASE_STATE_FALSE = 0;
	
	/**
	 * 手工下架 １上架
	 * 
	 */
	public static final int PRODUCT_MANUAL_OFF_UP = 1;
	/**
	 * 手工下架 ２下架
	 */
	public static final int PRODUCT_MANUAL_OFF_UN = 2;
	/**
	 * 下架状态：１上架
	 */
	public static final int PRODUCT_UNDO_STATE_UP = 1;
	/**
	 * 下架状态：２下架
	 */
	public static final int PRODUCT_UNDO_STATE_UN = 2;
	/**
	 * 获取获取标识,规格
	 */
	public static final String IDHOLDER_TYPE_SPEC_CODE = "type_spec_code";

	/**
	 * 1 待审 ,
	 */
	public static final int SEEDING_RESOURCE_STATE_WAIT = 1;
	/**
	 * 2 审核通过
	 */
	public static final int SEEDING_RESOURCE_STATE_PASS = 2;
	/**
	 * 3审核未通过
	 */
	public static final int SEEDING_RESOURCE_STATE_NOPASS = 3;

	/**
	 * 状态： 1 未追加
	 */
	public static final int PRODUCT_REVIEW_STATE_FASLE = 1;
	/**
	 * 状态： 2 已追加
	 */
	public static final int PRODUCT_REVIEW_STATE_TRUE = 2;
	
	
	/**
	 * 状态： 1 内部员工
	 */
	public static final int USER_TYPE_INSIDE = 1;
	
	/**
	 * 状态： 2 普通用户
	 */
	public static final int USER_TYPE_NORMAL = 2;
	
	/**
	 * 状态： 0 未绑卡
	 */
	public static final int USER_TIE_CARD_FALSE = 0;
	
	/**
	 * 状态： 1 绑卡
	 */
	public static final int USER_TIE_CARD_TRUE = 1;


	
	
	
	
	
	/** 预约情况：自己预约 */
	public static final int BESPEAK_TYPE_SELF = 1;
	
	/** 预约情况，帮他们预约 */
	public static final int BESPEAK_TYPE_OTHER = 2;
	
	/** 预约付款情况, 0:未付款 */
	public static final int BESPEAK_SATUS_NO_PAY = 0;
	
	/** 预约付款情况, 1:部分付款 */
	public static final int BESPEAK_SATUS_PART = 1;
	
	/** 预约付款情况, 2:已付款 */
	public static final int BESPEAK_SATUS_FULL = 2;
	
	/** 意见建议的状态，回复 */
	public static final int SUGGESTION_RESPONSE = 1;
	
	/** 意见建议的状态，未回复 */
	public static final int SUGGESTION_NO_RESPONSE = 0;
	
	/** 产品状态 , 测试中*/
	public static final int PRODUCT_STATUS_TEST = 2;
	
	/** 产品状态 , 已上架 */
	public static final int PRODUCT_STATUS_SALE = 1;
	
	/** 产品状态 , 未上架 */
	public static final int PRODUCT_STATUS_NO_SALE = 0; 
	
	/** 产品类型 ,  01公募基金类**/
	public static final int PRODUCT_TYPE_1 = 1; 
	
	/** 产品类型 ,  02固定收益类**/
	public static final int PRODUCT_TYPE_2 = 2; 
	
	/** 产品类型 ,  03浮动收益类**/
	public static final int PRODUCT_TYPE_3 = 3; 
	
	/** 产品类型 ,  04固定+浮动类**/
	public static final int PRODUCT_TYPE_4 = 4; 
	
	/** 产品类型 ,  05海外保险类**/
	public static final int PRODUCT_TYPE_5 = 5; 
	
	/** 组织结构类型 ,  01 集团**/
	public static final int ORGANIZATION_TYPE_GROUP = 1; 
	
	/** 组织结构类型 ,  02 分公司**/
	public static final int ORGANIZATION_TYPE_COMPANY = 2; 
	
	/** 组织结构类型 ,  03 部门**/
	public static final int ORGANIZATION_TYPE_DEPARTMENT = 3; 
	
	/** 组织结构类型 ,  04 职务**/
	public static final int ORGANIZATION_TYPE_DUTIES = 4; 
	
	/** 组织结构类型 ,  05 个人**/
	public static final int ORGANIZATION_TYPE_PERSON = 5; 
	
	public static final String ALL_DUTIES_LIST_KEY = "ALL_DUTIES_LIST";
	
	/** excel格式，xls **/
	public static final String OFFICE_EXCEL_2003_POSTFIX = "xls";
	
	/** excel格式， xlsx **/
	public static final String OFFICE_EXCEL_2010_POSTFIX = "xlsx";
	
	/** 文件读取， 小数点 **/
	public static final String POINT = ".";
	
	/** 文件读取，文件不存在 **/
	public static final String NOT_EXCEL_FILE = " : Not the Excel file!";
	
	/** 文件读取， 处理中**/
	public static final String PROCESSING = "Processing...";
	
	/**
	 * 活动状态:发布
	 */
	public static final int ACTIVITY_ACTIVE = 1;
	
	/**
	 * 活动状态:未发布
	 */
	public static final int ACTIVITY_NO_ACTIVE = 0;
	
	/**
	 * 积分规则:发布
	 */
	public static final int POINTS_ROULE_ACTIVE = 1;
	
	/**
	 * 积分规则:未发布
	 */
	public static final int POINTS_ROULE_NO_ACTIVE = 0;
}
