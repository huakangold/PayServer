package pay.common;

/**
 * 富友金账户用的的常量
 * @author jeff
 *
 */
public class ConstantFYJZH {
	/**
	 * 交易类型，充值
	 */
	public static final String  BUSI_TY_CZ = "PW11";
	
	/**
	 * 交易类型，提现
	 */
	public static final String  BUSI_TY_TX = "PWTX";

	/**
	 * 交易类型，退票
	 */
	public static final String BUSI_TY_TP = "PWTP";
	
	/**
	 * 交易状态，交易成功
	 */
	public static final String  TXN_ST_SUCC = "1";
	
	/**
	 * 交易状态，交易失败
	 */
	public static final String  TXN_ST_FAIL = "0";
	
	/**
	 * 充值
	 */
	public static final int  TRADE_TY_CZ = 1;
	
	/**
	 * 提现
	 */
	public static final int  TRADE_TY_TX = 2;
	
	/**
	 * 预授权
	 */
	public static final int  TRADE_TY_YSQ = 3;

	/**
	 * 取消预授权
	 */
	public static final int  TRADE_TY_YSQ_CANCEL = 6;
	
	/**
	 * 购买
	 */
	public static final int  TRADE_TY_GM = 4;
	
	/**
	 * 回款
	 */
	public static final int  TRADE_TY_HK = 5;
	
	
	/**
	 * 账户金额增加
	 */
	public static final String  BALANCE_ADD = "+";
	
	
	/**
	 * 账户金额减少
	 */
	public static final String  BALANCE_CUT = "-";
	 
	/**
	 * 交易记录
	 */
	public static final String TRADE_RECORD = "TRADE_RECORD_";
	
	/**
	 * 请求异常
	 */
	public static final String HTTP_ERROR_RETURN = "-1111";
	
	/**
	 * 充值
	 */
	public static final String MSSN_CZ = "1111";
	
	/**
	 * 提现
	 */
	public static final String MSSN_TX = "2222";
	
	/**
	 * 预授权
	 */
	public static final String MSSN_YSQ = "3333";
	
	/**
	 * 购买
	 */
	public static final String MSSN_GM = "4444";
	
	/**
	 * 回款
	 */
	public static final String MSSN_HK = "5555";

	/**
	 * 预授权取消
	 */
	public static final String MSSN_YSQ_QUXIAO = "6666";

	/**
	 * 换卡
	 */
	public static final String MSSN_CHANGE_CARD = "7777";

	/**
	 * 待划拨订单KEY
	 */
	public static final String FYJZH_TRANSFER_ORDER_LIST = "FYJZH_TRANSFER_ORDER_LIST";
	
	/**
	 * 划拨状态：待划拨
	 */
	public static final int FYJZH_TRANSFER_STATUS_WAITE = 1;
	
	/**
	 * 划拨状态：划拨失败
	 */
	public static final int FYJZH_TRANSFER_STATUS_FAIL = 2;
	
	
	/**
	 * 待回款订单KEY
	 */
	public static final String FYJZH_PAYBACK_ORDER_LIST = "FYJZH_PAYBACK_ORDER_LIST";
	
	/**
	 * 富友请求成功返回状态码
	 */
	public static final String FY_REQCODE_SUCC = "0000";

	/**
	 * 富友转账到债权人成功返回状态码
	 */
	public static final String FY_ZZTO_CREDITOR = "FY_ZZTO_CREDITOR";
	/**
	 * 富友转账到华康大虚账成功返回状态码
	 */
	public static final String FY_ZZTO_HOMEACCOUNT = "FY_ZZTO_HOMEACCOUNT";
	
}
