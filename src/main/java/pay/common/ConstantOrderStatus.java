package pay.common;

public class ConstantOrderStatus {
	/** 打款状态 ：未输入金额，可以定义为无效订单 */
	public static final int PAY_STATUS_ERR = 400;

	/** 打款状态 ：未确认 */
	public static final int PAY_STATUS_FALSE = 0;

	/** 打款状态 ：部分确认 */
	public static final int PAY_STATUS_PART = 1;

	/** 打款状态 ：已确认 */
	public static final int PAY_STATUS_TRUE = 2;

	/** 打款状态 :打款状态待确认 */
	public static final int PAY_STATUS_NO_CONFIRM = 3;

	/** 打款状态 : 确认回款 */
	public static final int PAY_STATUS_PAYBACK = 4;

	/** 打款状态 : 支付失败 */
	public static final int PAY_STATUS_FAIL = 5;

	/** 确认状态 ：未确认 */
	public static final int CONFIRM_STATUS_FALSE = 0;

	/** 确认状态 ：部分确认 */
	public static final int CONFIRM_STATUS__PART = 1;

	/** 确认状态 ：已确认 */
	public static final int CONFIRM_STATUS__TRUE = 2;

	/** 确认状态 : 订单状态待确认 */
	public static final int CONFIRM_STATUS__NO_CONFIRM = 3;

	/** 确认状态 : 订单还本付息中 */
	public static final int CONFIRM_STATUS__PAYBACK = 4;

	/** 错误状态 ：没有错误 */
	public static final int CREATE_ORDER_NO_ERROR = 200;

	/** 错误状态 ：传入信息有误 */
	public static final int CREATE_ORDER_ERROR_01 = 1;

	/** 错误状态 ：有未支付的订单 */
	public static final int CREATE_ORDER_ERROR_02 = 2;
}
