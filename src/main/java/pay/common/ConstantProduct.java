package pay.common;

public class ConstantProduct {

	/**
	 * 是否热销产品: 1 是
	 */
	public static final int PRODUCT_HOT_SELLING_TRUE = 1;

	/**
	 * 是否热销产品: 0否
	 */
	public static final int PRODUCT_HOT_SELLING_FALSE = 0;

	/**
	 * 新上架
	 */
	public static final int PRO_STATUS_2_NEW = 2;

	/**
	 * 已下架
	 */
	public static final int PRO_STATUS_4_OFF = 4;

	/**
	 * 待销
	 */
	public static final int PRO_STATUS_3_WAITSALE = 3;

	/**
	 * 开始计息
	 */
	public static final int PRO_START_DIV = 1;

	/**
	 * 未开始计息
	 */
	public static final int PRO_NOT_START_DIV = 0;

	/**
	 * 自动起息
	 */
	public static final int PRO_AUTO_START_DIV = 1;

	/**
	 * 不自动起息
	 */
	public static final int PRO_NO_AUTO_START_DIV = 0;

	/**
	 * 产品售罄
	 */
	public static final int PRO_SALE_OUT = 1;

	/**
	 * 产品未售罄
	 */
	public static final int PRO_NO_SALE_OUT = 0;

	/**
	 * 产品起息方式 D + 1
	 */
	public static final int PRO_DIV_START_TYPE_DADD1 = 1;

	/**
	 * 产品起息方式 D + 2
	 */
	public static final int PRO_DIV_START_TYPE_DADD2 = 2;

	/**
	 * 产品起息方式 T + 1
	 */
	public static final int PRO_DIV_START_TYPE_TADD1 = 3;

	/**
	 * 产品起息方式 T + 2
	 */
	public static final int PRO_DIV_START_TYPE_TADD2 = 4;

	/**
	 * 是否上架销售: 0 未上架
	 */
	public static final int PRODUCT_RELEASE_STATE_FALSE = 0;

	/**
	 * 是否上架销售: 1 上架
	 */
	public static final int PRODUCT_RELEASE_STATE_TRUE = 1;

	/**
	 * 是否上架销售: 2 测试中
	 */
	public static final int PRODUCT_RELEASE_STATE_TEST = 2;
	/**
	 * 是否上架销售: 4已下架
	 */
	public static final int PRODUCT_RELEASE_STATE_OFF = 4;
	
	/**
	 * 产品回款状态：未回款
	 */
	public static final int PRODUCT_PAYBACK_STATUS_FALSE = 0;
	/**
	 * 产品回款状态：已回款
	 */
	public static final int PRODUCT_PAYBACK_STATUS_TRUE = 1;
	
	/**
	 * 体验标
	 */
	public static final int  PRODUCT_TEST_PRODUCT = 2;
	
	/**
	 * 非体验标
	 */
	public static final int  PRODUCT_NO_TEST_PRODUCT = 1;

}
