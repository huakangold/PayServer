package pay.common;

public class ConstantRoute {

	/** 成功的返回结果码 **/
	public static final int CB_SUCCESS_CODE = 200;

	/** 公司: 华康 **/
	public static final int COMPANY_HUAKANG = 1;

	/** 公司: 中融信托 **/
	public static final int COMPANY_ZHONGRONG = 2;

	/** 公司: 火柴 **/
	public static final int COMPANY_HUOCHAI = 3;

	/** 公司: 盈米 **/
	public static final int COMPANY_YINGMI = 4;
}
