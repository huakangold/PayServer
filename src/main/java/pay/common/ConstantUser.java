package pay.common;

/**
 * @author Evan
 * @date 2017/10/23
 */
public class ConstantUser {
    /** 系统角色： 测试员 **/
    public static final String USER_ROLE_TESTER = "USER_ROLE_TESTER";


    /** 系统角色： 运营人员 **/
    public static final String USER_ROLE_OPERA = "USER_ROLE_OPERA";

    /** 系统角色： 渠道用户录入员**/
    public static final String USER_CHANNEL_REGIS = "USER_CHANNEL_REGIS";


    /** 系统角色： 安全设置**/
    public static final String USER_ROLE_SAFETY = "USER_ROLE_SAFETY";


    /** 系统角色： 团队负责人**/
    public static final String USER_ROLE_TEAM_LEADER = "USER_ROLE_TEAM_LEADER";


    /** 系统角色： 权限负责人**/
    public static final String USER_ROLE_AUTH = "USER_ROLE_AUTH";



    public static final int USER_ORG_WUZHU = 568;


    /** 已离职 **/
    public static final int USER_LEAVE = 1;

    /** 未离职 **/
    public static final int USER_NOT_LEAVE = 0;

    /** 已销户**/
    public static final int CLOSE_ACCOUNT = 1;

    /** 未销户**/
    public static final int NOT_CLOSE_ACCOUNT = 0;

}
