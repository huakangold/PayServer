package pay.common;

public class DataMasking {

	/**
	 * 数据隐藏， data 原始数据， headLength 开始前几个数据不隐藏， endLength末尾几个数据不隐藏
	 * 
	 * @param data
	 * @param headLength
	 * @param endLength
	 * @return
	 */
	public static String getMaskingData(String data, int headLength,
			int endLength) {
		char[] charArray = data.toCharArray();
		Integer totalLength = charArray.length;
		for (int i = 0; i < charArray.length; i++) {
			if (i < headLength || i >= totalLength - endLength) {
				charArray[i] = charArray[i];
			} else {
				charArray[i] = '*';
			}
		}

		return new String(charArray);
	}

	/**
	 * 数据隐藏， data 原始数据， endLength末尾几个数据不隐藏
	 * 
	 * @param data
	 * @param lastLength
	 * @return
	 */
	public static String getMaskingData(String data, int lastLength) {
		char[] charArray = data.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if (i < charArray.length - lastLength) {
				charArray[i] = '*';
			} else {
				charArray[i] = charArray[i];
			}
		}

		return new String(charArray);
	}

	public static void main(String[] args) throws Exception {
		String data = "1234567890";
		String name01 = "李培";
		String name02 = "李培培";
		System.out.println(getMaskingData(data, 4, 4));
		System.out.println(getMaskingData(name01, 1));
		System.out.println(getMaskingData(name02, 1));

	}
}
