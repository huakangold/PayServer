package pay.common;

import java.math.BigDecimal;

/**
 * Created by lipei on 2017/8/8.
 */
public class FixedAssetsDto {
    public int fixedAssetsCounts;

    public BigDecimal fixedAssetsSuccessAmts;


    public BigDecimal fixedAssetsCurrentDividends;

    public int getFixedAssetsCounts() {
        return fixedAssetsCounts;
    }

    public void setFixedAssetsCounts(int fixedAssetsCounts) {
        this.fixedAssetsCounts = fixedAssetsCounts;
    }

    public BigDecimal getFixedAssetsCurrentDividends() {
        return fixedAssetsCurrentDividends;
    }

    public void setFixedAssetsCurrentDividends(BigDecimal fixedAssetsCurrentDividends) {
        this.fixedAssetsCurrentDividends = fixedAssetsCurrentDividends;
    }

    public BigDecimal getFixedAssetsSuccessAmts() {
        return fixedAssetsSuccessAmts;
    }

    public void setFixedAssetsSuccessAmts(BigDecimal fixedAssetsSuccessAmts) {
        this.fixedAssetsSuccessAmts = fixedAssetsSuccessAmts;
    }
}
