package pay.common;

import java.math.BigDecimal;

/**
 * Created by lipei on 2017/8/8.
 */
public class FundsDto {
    private int fundsAllCount;

    private BigDecimal  allFundsMoney;

    private BigDecimal fundConfirmAllProfit;


    public BigDecimal getAllFundsMoney() {
        return allFundsMoney;
    }

    public void setAllFundsMoney(BigDecimal allFundsMoney) {
        this.allFundsMoney = allFundsMoney;
    }

    public BigDecimal getFundConfirmAllProfit() {
        return fundConfirmAllProfit;
    }

    public void setFundConfirmAllProfit(BigDecimal fundConfirmAllProfit) {
        this.fundConfirmAllProfit = fundConfirmAllProfit;
    }


    public int getFundsAllCount() {
        return fundsAllCount;
    }

    public void setFundsAllCount(int fundsAllCount) {
        this.fundsAllCount = fundsAllCount;
    }
}
