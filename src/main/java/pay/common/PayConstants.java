package pay.common;

public class PayConstants {
	// 用户充值记录
	public static final String PAY_RECHARGE_RECORD = "PAY_RECHARGE_RECORD";
	
	// 用户提现记录
	public static final String PAY_WITHDRAW_RECORD = "PAY_WITHDRAW_RECORD";

}
