package pay.common;

public enum ResultCode {
	SUCC("200", "成功"), FAILED("1001", "失败"), PARAM_ERROR("1002", " 请求参数异常"), NO_DATA(
			"1003", "没有数据"), NO_RIGHT("1004", "没有权限访问资源"), OTHER("3001", "其他"), USER_EXIST(
			"4001", "用户已存在"), PHONE_CHANGE("1005", "手机已更换,请用短信验证登录"), USER_NOT_EXIST(
			"4002", "用户不存在"), ILLEGAL_TOKEN("9999", "TOKEN不存在"), PARAM_LACK(
			"1006", "必传项不能为空"), IREECT_SIGN("1010", "加密串不匹配"),ILLEGAL_ACCESSTOKEN("9998", "accessToken不存在或错误");

	private String code;

	private String msg;

	private ResultCode(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}
}
