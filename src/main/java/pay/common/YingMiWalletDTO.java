package pay.common;

import java.math.BigDecimal;

/**
 * 资产页-华康宝
 * @author Evan
 * @date 2017/11/10
 */
public class YingMiWalletDTO {
    /**
     * 总资产
     */
    private BigDecimal allYingMiWallet;

    /**
     * 累计收益
     */
    private BigDecimal YingMiWalletConfirmAllProfit;

    public BigDecimal getAllYingMiWallet() {
        return allYingMiWallet;
    }

    public void setAllYingMiWallet(BigDecimal allYingMiWallet) {
        this.allYingMiWallet = allYingMiWallet;
    }

    public BigDecimal getYingMiWalletConfirmAllProfit() {
        return YingMiWalletConfirmAllProfit;
    }

    public void setYingMiWalletConfirmAllProfit(BigDecimal yingMiWalletConfirmAllProfit) {
        YingMiWalletConfirmAllProfit = yingMiWalletConfirmAllProfit;
    }

    @Override
    public String toString() {
        return "YingMiWalletDTO{" +
                "allYingMiWallet=" + allYingMiWallet +
                ", YingMiWalletConfirmAllProfit=" + YingMiWalletConfirmAllProfit +
                '}';
    }
}
