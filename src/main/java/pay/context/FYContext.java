package pay.context;

import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pay.common.ConstantFYJZH;
import pay.portal.web.message.CZTXRecord;
import pay.utils.FYApiUtil;
import pay.utils.GetCZTXRecordFromXml;
import pay.utils.SecurityUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 富友上下文
 * @author Evan
 * @date 2018/1/8
 */
@Service
public class FYContext {
    /**
     * 商户代码，从配置文件中读取
     */

    @Value("${fy.mchntCd}")
    private String mchnt_cd;

    @Value("${server.scheme}")
    private String serverScheme;

    @Value("${fy.goldAccount.pathBaseUrl}")
    private String pathBaseUrl; // 测试环境为/jzh 生产无。

    /**
     * 金账户baseURl，从配置文件中读取
     */
    @Value("${fy.goldAccount.baseUrl}")
    private String jzhBaseUrl;

    private final Logger log = LoggerFactory.getLogger(FYContext.class);

    /**
     * 从富有获得查询充值提现结果
     *
     * @param busi_tp 交易类型
     * @param cust_no 用户账号
     * @param start_time
     * @param end_time
     * @return
     */
    public List<CZTXRecord> getResult(String busi_tp, String cust_no,
                                      String start_time, String end_time) {

        String mchnt_txn_ssn = cust_no
                + String.valueOf(System.currentTimeMillis());

        log.info(
                "busi_tp={}, cust_no={}, start_time={}, end_time={}, mchnt_txn_ssn={}",
                busi_tp, cust_no, start_time, end_time, mchnt_txn_ssn);
        // 交易状态
        String txn_st = ConstantFYJZH.TXN_ST_SUCC;

        // 页码
        String page_no = "1";

        // 每页条数
        String page_size = "100";

        String pathUrl = pathBaseUrl + "/querycztx.action";
        URIBuilder builder = new URIBuilder().setScheme("https")
                .setHost(jzhBaseUrl).setPath(pathUrl);
        log.info(builder.toString());
        Map<String, String> trimmedParams = new HashMap<>();

        String allUrl = busi_tp + "|" + cust_no + "|" + end_time + "|"
                + mchnt_cd + "|" + mchnt_txn_ssn + "|" + page_no + "|"
                + page_size + "|" + start_time + "|" + txn_st;

        SecurityUtils.initPrivateKey();
        SecurityUtils.initPublicKey();
        String signature = SecurityUtils.sign(allUrl);

        trimmedParams.put("mchnt_txn_ssn", mchnt_txn_ssn);
        trimmedParams.put("busi_tp", busi_tp);
        trimmedParams.put("cust_no", cust_no);
        trimmedParams.put("end_time", end_time);
        trimmedParams.put("mchnt_cd", mchnt_cd);
        trimmedParams.put("page_no", page_no);
        trimmedParams.put("page_size", page_size);
        trimmedParams.put("start_time", start_time);
        trimmedParams.put("txn_st", txn_st);
        trimmedParams.put("signature", signature);

        String resultStr = FYApiUtil.doPost(builder, trimmedParams);

        List<CZTXRecord> recordsList = GetCZTXRecordFromXml.getRecords(
                resultStr, busi_tp);
        return recordsList;
    }

}
