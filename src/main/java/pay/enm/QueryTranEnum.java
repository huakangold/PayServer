/**
 * 表示一个订单的支付状态。
 * @author jeff
 * 
 */

package pay.enm;

public enum QueryTranEnum {
	
	TYPES_PW11("PW11", "充值", "+"),
	
	TYPES_PWTX("PWTX", "提现", "-"),
	
	TYPES_PWPC("PWPC", "转账", "-"),
	TYPES_PW13("PW13", "预授权", "-"),
	TYPES_PW03("PW03", "划拨", "-"),
	TYPES_PW14("PW14", "转账冻结", "-"),
	TYPES_PW15("PW15", "划拨冻结", "-"),
	TYPES_PWDJ("PWDJ", "冻结", "-"),
	TYPES_PWJD("PWJD", "解冻", "+"),
	TYPES_PW19("PW19", "冻结付款到冻结", "-");
	
	private String typeCode;
	
	private String typeStr;
	
	private String typeFlag;
	
	private QueryTranEnum(String typeCode, String typeStr, String typeFlag) {
		this.typeCode = typeCode;
		this.typeStr = typeStr;
		this.typeFlag = typeFlag;
	}

	
	
	public String getTypeCode() {
		return typeCode;
	}



	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}



	public String getTypeStr() {
		return typeStr;
	}



	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	
	
	public String getTypeFlag() {
		return typeFlag;
	}



	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}



	public static String getTypeStr(String typeCode){
		String returnStr = null;
		if(typeCode.equalsIgnoreCase("PW11")){
			  returnStr = QueryTranEnum.TYPES_PW11.getTypeStr();  
		}else if(typeCode.equalsIgnoreCase("PWTX")){
			  returnStr = QueryTranEnum.TYPES_PWTX.getTypeStr();  
		}if(typeCode.equalsIgnoreCase("PW13")){
			  returnStr = QueryTranEnum.TYPES_PW13.getTypeStr();  
		}else if(typeCode.equalsIgnoreCase("PW03")){
			  returnStr = QueryTranEnum.TYPES_PW03.getTypeStr();  
		}
		
		return returnStr;
	}
	
	public static String getTypeFlag(String typeCode){
		String returnStr = null;
		if(typeCode.equalsIgnoreCase("PW11")){
			  returnStr = QueryTranEnum.TYPES_PW11.getTypeFlag();  
		}else if(typeCode.equalsIgnoreCase("PWTX")){
			  returnStr = QueryTranEnum.TYPES_PWTX.getTypeFlag(); 
		}if(typeCode.equalsIgnoreCase("PW13")){
			  returnStr = QueryTranEnum.TYPES_PW13.getTypeFlag();  
		}else if(typeCode.equalsIgnoreCase("PW03")){
			  returnStr = QueryTranEnum.TYPES_PW03.getTypeFlag(); 
		}
		return returnStr;
	}
}
