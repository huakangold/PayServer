package pay.enm;

/**
 * 富友提现状态
 * @author Evan
 * @date 2018/1/8
 */
public enum WithdrawStatusEnum {
    SUCCUSS("成功"),
    AUDITING("审核中"),
    FAIL("失败");

    private String value;

    WithdrawStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
