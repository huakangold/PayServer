package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_apiSecret_code")
public class ApiSecret implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -74549944710623355L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;
	
	/**
	 * 生成的API Key
	 * 生成规则，UUID
	 */
	private String apikey;
	
	/**
	 * 生成API secret
	 * 生成规则 ，org.apache.commons.codec.digest.Md5Crypt.md5Crypt
	 */
	private String apiSecret;
	
	/**
	 * 1 生效 ， 2 失效 ， 0 待审核
	 */
	private int type;
	
	/**
	 * 接入渠道  1 Android 2 IOS 3 PC 4 微信
	 */
	private int channel;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getChannel() {
		return channel;
	}

	public void setChannel(int channel) {
		this.channel = channel;
	}
	
	
	
	
	
	
}
