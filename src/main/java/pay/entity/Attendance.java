package pay.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Attendance")
public class Attendance implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6617898979860644889L;

	@Id
	@Column(name = "Id", length = 20)
	private Long id;

	/**考勤类别*/
	@Column(name = "Type", length = 20)
	private String type;

	/**起始时间*/
	@Column(name = "StartTime")
	private Timestamp startTime;

	/**截止时间*/
	@Column(name = "EndTime")
	private Timestamp endTime;
	
	/** 用户id */
	@Column(name = "UserId", length = 10)
	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}

