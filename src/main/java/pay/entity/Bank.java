package pay.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 职务信息表
 * 
 * @author saga
 * 
 */
@Entity
@Table(name = "t_bank_info")
public class Bank implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7428378998667927113L;

	@Id
    @GeneratedValue (generator =  "paymentableGenerator" )    
    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" )   
	public Integer id;

	/**银行名称*/
	private String bankName;
	
	/**职务名称*/
	private String bankCode;

	/**是否显示*/
	private Integer showStatus;

	
	public Integer getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(Integer showStatus) {
		this.showStatus = showStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
}
