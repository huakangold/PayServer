package pay.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "t_card")
public class Card implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1578350895690889930L;

	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "increment")
	private Integer id;

	/**
	 * 加息率、代金券金额
	 */
	@Column(precision = 18 , scale = 4)
	private BigDecimal rate;

	/**
	 * 积分规则的ID
	 */
	private Integer pointsRuleId;

	/**
	 * 创建时间
	 */
	private Long cts;

	/**
	 * 创建人
	 */
	private Long creator;

	/**
	 * 卡券的类型， 1为增加利息， 2是代替本金
	 */
	private Integer type;

	/**
	 * 活动开始时间
	 */
	private Long startDate;

	/**
	 * 活动结束时间
	 */
	private Long endDate;

	/**
	 * 卡券有效期截止时间
	 */
	private Long expireDate;

	/**
	 * 发放送数
	 */
	private Integer totalNum = 0;

	/**
	 * 已领取数目
	 */
	private Integer usedNum = 0;

	/**
	 * 描述内容
	 */
	private String cardDesc;

	@Version
	private int version;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public Long getCts() {
		return cts;
	}

	public void setCts(Long cts) {
		this.cts = cts;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(Integer totalNum) {
		this.totalNum = totalNum;
	}

	public Integer getUsedNum() {
		return usedNum;
	}

	public void setUsedNum(Integer usedNum) {
		this.usedNum = usedNum;
	}

	public String getCardDesc() {
		return cardDesc;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public void setCardDesc(String cardDesc) {
		this.cardDesc = cardDesc;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Integer getPointsRuleId() {
		return pointsRuleId;
	}

	public void setPointsRuleId(Integer pointsRuleId) {
		this.pointsRuleId = pointsRuleId;
	}

	/**
	 * @return the expireDate
	 */
	public Long getExpireDate() {
		return expireDate;
	}

	/**
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpireDate(Long expireDate) {
		this.expireDate = expireDate;
	}

}

