package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "t_city_info")
public class City implements Serializable {

	/**
	 *
	 *
	 */
	private static final long serialVersionUID = 4768461577384242898L;

	@Id
	@GeneratedValue (generator =  "paymentableGenerator" )    
    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" )   
	public Integer id;

	public Integer cityCode;
	
	public Integer proCode;
	
	public String cityName;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getCityCode() {
		return cityCode;
	}


	public void setCityCode(Integer cityCode) {
		this.cityCode = cityCode;
	}


	public String getCityName() {
		return cityName;
	}


	public void setCityName(String cityName) {
		this.cityName = cityName;
	}


	public Integer getProCode() {
		return proCode;
	}


	public void setProCode(Integer proCode) {
		this.proCode = proCode;
	}


	 
}
