package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * @author 华康
 *
 */
@Entity
@Table(name="t_error_log")
public class ErrorLog implements Serializable{
	private static final long serialVersionUID = 5579518370742859098L;
	
	@Id
	private Long id;	
	private String errorTime;	
	private String accountName;	
	private Long userId;	
	private String errorMsg;
	private String innerParams;
	private String mchntTxnSsn;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getErrorTime() {
		return errorTime;
	}
	public void setErrorTime(String errorTime) {
		this.errorTime = errorTime;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getInnerParams() {
		return innerParams;
	}
	public void setInnerParams(String innerParams) {
		this.innerParams = innerParams;
	}
	public String getMchntTxnSsn() {
		return mchntTxnSsn;
	}
	public void setMchntTxnSsn(String mchntTxnSsn) {
		this.mchntTxnSsn = mchntTxnSsn;
	}
	
}
