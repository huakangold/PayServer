package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="t_fy_resultCode")
public class FYResultCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3258839839160856613L;

	@Id
    @GeneratedValue (generator =  "paymentableGenerator" )    
    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" ) 
	private Integer id;

	private String code;

	private String codeDesc;

	private String codeMsg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCodeDesc() {
		return codeDesc;
	}

	public void setCodeDesc(String codeDesc) {
		this.codeDesc = codeDesc;
	}

	public String getCodeMsg() {
		return codeMsg;
	}

	public void setCodeMsg(String codeMsg) {
		this.codeMsg = codeMsg;
	}

	 
}
