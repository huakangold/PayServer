package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "t_history_payment")
public class HistoryPayment implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7309457115139099134L;

	@Id
	private Long id;

	/**
	 * 用户id
	 */
	private Long userId;
	private int company;
	private Long cts;
	private String visibleCts;
	private String oldBankName;
	private String newBankName;
	private String oldBankNo;
	private String newBankNo;
	private Long paymentId;
	/**
	 * "CHANGECARD";//换卡
	 * "ADDCARD";//新增卡
	 * "UNBIND";//解卡
	 * "DELACCOUNT";//销户
	 */
	private String paymentType;//历史类型

	private Integer status=0; //操作状态 0：失败，1：成功

	private String failMsg;//失败原因

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public int getCompany() {
		return company;
	}
	public void setCompany(int company) {
		this.company = company;
	}
	public Long getCts() {
		return cts;
	}
	public void setCts(Long cts) {
		this.cts = cts;
	}
	public String getOldBankNo() {
		return oldBankNo;
	}
	public void setOldBankNo(String oldBankNo) {
		this.oldBankNo = oldBankNo;
	}
	public String getNewBankNo() {
		return newBankNo;
	}
	public void setNewBankNo(String newBankNo) {
		this.newBankNo = newBankNo;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getVisibleCts() {
		return visibleCts;
	}
	public void setVisibleCts(String visibleCts) {
		this.visibleCts = visibleCts;
	}
	public Long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}
	public String getOldBankName() {
		return oldBankName;
	}
	public void setOldBankName(String oldBankName) {
		this.oldBankName = oldBankName;
	}
	public String getNewBankName() {
		return newBankName;
	}
	public void setNewBankName(String newBankName) {
		this.newBankName = newBankName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFailMsg() {
		return failMsg;
	}

	public void setFailMsg(String failMsg) {
		this.failMsg = failMsg;
	}
}
