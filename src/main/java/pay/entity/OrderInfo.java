package pay.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_order_info")
public class OrderInfo implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = -7544780170624948565L;

	@Id
	private Long id;

	private Long userId;

	/**
	 * 订单所属盈米账户ID
	 */
	private String accountId;

	/**
	 * 订单流水号
	 */
	private String brokerOrderNo;

	/** 产品提供商ID */
	private Integer companyId;

	/** 产中ID */
	private Long productId;

	/**
	 * 是否可以撤单
	 */
	private Integer canCelable;

	/**
	 * 订单确认状态，详见确认状态 0 未确认 1 确认失败 2 确认成功 3 部分确认成功 4 认购成功，份额待基金成立时确认（即“行为确认”） 9
	 * 已撤单 11 组合订单专用 - 全部子订单确认 12 组合订单专用 - 部分子订单确认
	 */
	private Integer confirmStatus;

	/**
	 * 成功转换的金额，转换类订单会存在此参数
	 */
	private BigDecimal convertSuccessAmount;

	/**
	 * 成功转换的目标基金份额，转换类订单会存在此参数
	 */
	private BigDecimal convertSuccessShare;

	/**
	 * 转换目标基金代码，转换类订单会存在此参数
	 */
	private String destFundCode;

	/**
	 * 目标付费类型，转换类订单会存在此参数，详见收费类型
	 *
	 * A 前端收费 当认购、申购开放式基金时就需支付认/申购费的付费方式 B 后端收费
	 * 在购买开放式基金时并不支付认/申购费，等到赎回时才支付的认/申购费方式。 C C类收费
	 * 不收取申购费率，按照持有时间分类收取赎回费，并收取销售服务费的一种收费模式。
	 */
	private String destShareType;

	/**
	 * 订单错误码
	 */
	private String errorCode;

	/**
	 * 订单错误信息
	 */
	private String errorMessage;

	/**
	 * 交易费用，以元为单位
	 */
	private BigDecimal fee;

	/**
	 * 订单交易类型，详见交易类型
	 */
	private String fundOrderCode;

	/**
	 * 订单所属确认日
	 */
	private Long orderConfirmDate;

	/**
	 * 订单创建时间
	 */
	private Long orderCreatedOn;

	/**
	 * 订单ID
	 */
	private String orderId;

	/**
	 * 订单所属交易日
	 */
	private Long orderTradeDate;

	/**
	 * 订单支付方式ID
	 */
	private String paymentMethodId;

	/**
	 * 订单支付状态，详见支付状态 本字段只表示用户支付时的扣款状态，只对购买类订单有效。 对于如赎回、转换类订单无任何意义。 0 未打款 1 部分打款
	 * 2 已打款 3 打款状态待确认 4 还本付息中
	 */
	private Integer payStatus;

	/**
	 * 收费方式，详见收费类型 A 前端收费 当认购、申购开放式基金时就需支付认/申购费的付费方式 B 后端收费
	 * 在购买开放式基金时并不支付认/申购费，等到赎回时才支付的认/申购费方式。 C C类收费
	 * 不收取申购费率，按照持有时间分类收取赎回费，并收取销售服务费的一种收费模式。
	 */
	private String shareType;

	/**
	 * 赎回类，购买类及转换类订单的实际成功的成交金额，尚未确认的订单此字段为null。
	 */
	private BigDecimal successAmount;

	/**
	 * 赎回类，购买类及转换类订单的实际成功的成交份额，尚未确认的订单此字段为null。
	 */
	private BigDecimal successShare;

	/**
	 * 交易金额，购买类订单会存在此参数
	 */
	private BigDecimal tradeAmount;

	/**
	 * 预期总收益=产品收益+加息收益+额外收益（addRateDividend）
	 */
	private BigDecimal dividend= BigDecimal.valueOf(0.00);

	/**
	 * 交易金额币种，购买类订单会存在此参数，详见币种 156 人民币 CNY 840 美元 USD 344 港币 HKD 954 欧元 EUR 392
	 * 日元 JPY 826 英镑 GBP 250 法郎 280 马克
	 */
	private String tradeCurrency;

	/**
	 * 交易份额，赎回和转换类订单会存在此参数，单位“份”
	 */
	private BigDecimal tradeShare;

	/**
	 * 巨额赎回标记，赎回／转换类订单会存在此参数，详见巨额赎回标志 0 当赎回额度超过可以最大赎回份额时，超出部分取消赎回 1
	 * 当赎回份额超过可以最大赎回份额时，超出的部分顺延赎回
	 */
	private String vastlyRedeemFlag;

	/** 理财师ID */
	private Long financialPlannerId;

	/**
	 * 支付信息
	 */
	private Long paymentId;

	/**
	 * 还本付息时间
	 */
	private Long dividendDate;

	private String userName;

	/**
	 * 产品加息收益率
	 */
	@Column(columnDefinition = "DECIMAL(10,4)")
	private BigDecimal prodAddRate= BigDecimal.valueOf(0.00);


	/**
	 * 加息收益
	 */
	@Column(columnDefinition = "DECIMAL(10,2)")
	private BigDecimal addRateDividend= BigDecimal.valueOf(0.00);


	/**
	 * 优惠券收益率
	 */
	@Column(columnDefinition = "DECIMAL(10,4)")
	private BigDecimal userCardRate= BigDecimal.valueOf(0.00);

	/**
	 * 优惠券面值
	 */
	@Column(columnDefinition = "DECIMAL(10,4)")
	private BigDecimal userCardFaceValue = BigDecimal.valueOf(0.00);



	/**
	 * 优惠券收益
	 */
	@Column(columnDefinition = "DECIMAL(10,2)")
	private BigDecimal userCardDividend= BigDecimal.valueOf(0.00);

	/**
	 * 优惠券和产品额外加息收益
	 */
	@Column(columnDefinition = "DECIMAL(10,4)")
	private BigDecimal extraDividend= BigDecimal.valueOf(0.00);


	/**
	 * 产品收益
	 */
	private BigDecimal prodDividend= BigDecimal.valueOf(0.00);




	/**
	 * 优惠券id
	 */
	private Long userCardId;
	/**
	 * 渠道名称
	 */
	private String channelName;

	private Integer channel;
	/**
	 * 机构id
	 */
	private Integer orgId;

	/**
	 * 预授权合同号
	 */
	private String contract_no;

	/**
	 * 债权人手机号
	 *
	 */
	private String creditorPhoneNum;

	/**
	 *
	 * 佣金发放状态
	 */
	private Integer commissionStatus=0;


	/**
	 *
	 * 佣金发放时间
	 */
	private Integer commissionPayTime;


	/**
	 * 实际付款金额
	 */
	private BigDecimal realPayAmt = BigDecimal.valueOf(0.00);


	/**
	 * 优惠付款金额
	 */

	private BigDecimal benefitAmt= BigDecimal.valueOf(0.00);

	/**
	 * 总回款金额
	 */
	@Column(columnDefinition = "DECIMAL(19,4)")
	private BigDecimal payBackAmt = BigDecimal.valueOf(0.00);

	/**
	 * 产品收益率
	 */
	@Column(columnDefinition = "DECIMAL(6,4)")
	private BigDecimal proRate= BigDecimal.valueOf(0.00);


	/**
	 * 加息收益率
	 */
	@Column(columnDefinition = "DECIMAL(6,4)")
	private BigDecimal extRate= BigDecimal.valueOf(0.00);


	/**
	 * 总收益率
	 */
	@Column(columnDefinition = "DECIMAL(6,4)")
	private BigDecimal totalRate= BigDecimal.valueOf(0.00);

	/**
	 * 订单优惠类型：1,加息券; 2:体验金; 5:贴息券, 默认为什么都没有使用
	 */
	private int benefitType = 0;

	@Column(columnDefinition = "comment '加息券生效天数'  default 0 ")
	private int extraTerm=0;

	private String contractTitle;//合同标题

	private String contractUrl;//合同Url

	private String contractUrlHTML;//合同Url HTML 格式orderinf

	private String confirmationTitle; //投资确认函标题

	//投资确认函 PDF
	private String confirmationPdf;

	//投资确认函 HTML
	private String confirmationHtml;

	//订单投资期限
	private Integer term;

	//分行名称
	private String subBankName;

	public BigDecimal getUserCardFaceValue() {
		return userCardFaceValue;
	}

	public void setUserCardFaceValue(BigDecimal userCardFaceValue) {
		this.userCardFaceValue = userCardFaceValue;
	}

	public String getConfirmationTitle() {
		return confirmationTitle;
	}

	public void setConfirmationTitle(String confirmationTitle) {
		this.confirmationTitle = confirmationTitle;
	}

	public String getConfirmationPdf() {
		return confirmationPdf;
	}

	public void setConfirmationPdf(String confirmationPdf) {
		this.confirmationPdf = confirmationPdf;
	}

	public String getConfirmationHtml() {
		return confirmationHtml;
	}

	public void setConfirmationHtml(String confirmationHtml) {
		this.confirmationHtml = confirmationHtml;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

	public String getSubBankName() {
		return subBankName;
	}

	public void setSubBankName(String subBankName) {
		this.subBankName = subBankName;
	}

	public String getContractUrlHTML() {
		return contractUrlHTML;
	}

	public void setContractUrlHTML(String contractUrlHTML) {
		this.contractUrlHTML = contractUrlHTML;
	}

	public String getContractTitle() {
		return contractTitle;
	}

	public void setContractTitle(String contractTitle) {
		this.contractTitle = contractTitle;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getBrokerOrderNo() {
		return brokerOrderNo;
	}

	public void setBrokerOrderNo(String brokerOrderNo) {
		this.brokerOrderNo = brokerOrderNo;
	}

	public Integer getCancelable() {
		return canCelable;
	}

	public void setCancelable(Integer cancelable) {
		this.canCelable = cancelable;
	}

	public Integer getConfirmStatus() {
		return confirmStatus;
	}

	public void setConfirmStatus(Integer confirmStatus) {
		this.confirmStatus = confirmStatus;
	}

	public BigDecimal getConvertSuccessAmount() {
		return convertSuccessAmount;
	}

	public void setConvertSuccessAmount(BigDecimal convertSuccessAmount) {
		this.convertSuccessAmount = convertSuccessAmount;
	}

	public BigDecimal getConvertSuccessShare() {
		return convertSuccessShare;
	}

	public void setConvertSuccessShare(BigDecimal convertSuccessShare) {
		this.convertSuccessShare = convertSuccessShare;
	}

	public String getDestFundCode() {
		return destFundCode;
	}

	public void setDestFundCode(String destFundCode) {
		this.destFundCode = destFundCode;
	}

	public String getDestShareType() {
		return destShareType;
	}

	public void setDestShareType(String destShareType) {
		this.destShareType = destShareType;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public String getFundOrderCode() {
		return fundOrderCode;
	}

	public void setFundOrderCode(String fundOrderCode) {
		this.fundOrderCode = fundOrderCode;
	}

	public Long getOrderConfirmDate() {
		return orderConfirmDate;
	}

	public void setOrderConfirmDate(Long orderConfirmDate) {
		this.orderConfirmDate = orderConfirmDate;
	}

	public Long getOrderCreatedOn() {
		return orderCreatedOn;
	}

	public void setOrderCreatedOn(Long orderCreatedOn) {
		this.orderCreatedOn = orderCreatedOn;
	}

	public String getOrderId() {
		return orderId;
	}

	public Long getDividendDate() {
		return dividendDate;
	}

	public void setDividendDate(Long dividendDate) {
		this.dividendDate = dividendDate;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getOrderTradeDate() {
		return orderTradeDate;
	}

	public void setOrderTradeDate(Long orderTradeDate) {
		this.orderTradeDate = orderTradeDate;
	}

	public String getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public String getShareType() {
		return shareType;
	}

	public void setShareType(String shareType) {
		this.shareType = shareType;
	}

	public BigDecimal getSuccessAmount() {
		return successAmount;
	}

	public void setSuccessAmount(BigDecimal successAmount) {
		this.successAmount = successAmount;
	}

	public BigDecimal getSuccessShare() {
		return successShare;
	}

	public void setSuccessShare(BigDecimal successShare) {
		this.successShare = successShare;
	}

	public BigDecimal getTradeAmount() {
		return tradeAmount;
	}

	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}

	public String getTradeCurrency() {
		return tradeCurrency;
	}

	public void setTradeCurrency(String tradeCurrency) {
		this.tradeCurrency = tradeCurrency;
	}

	public BigDecimal getTradeShare() {
		return tradeShare;
	}

	public void setTradeShare(BigDecimal tradeShare) {
		this.tradeShare = tradeShare;
	}

	public String getVastlyRedeemFlag() {
		return vastlyRedeemFlag;
	}

	public void setVastlyRedeemFlag(String vastlyRedeemFlag) {
		this.vastlyRedeemFlag = vastlyRedeemFlag;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getCanCelable() {
		return canCelable;
	}

	public void setCanCelable(Integer canCelable) {
		this.canCelable = canCelable;
	}

	public Long getFinancialPlannerId() {
		return financialPlannerId;
	}

	public void setFinancialPlannerId(Long financialPlannerId) {
		this.financialPlannerId = financialPlannerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public BigDecimal getDividend() {
		return dividend;
	}

	public void setDividend(BigDecimal dividend) {
		this.dividend = dividend;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public BigDecimal getExtraDividend() {
		return extraDividend;
	}

	public void setExtraDividend(BigDecimal extraDividend) {
		this.extraDividend = extraDividend;
	}

	public BigDecimal getProdDividend() {
		return prodDividend;
	}

	public void setProdDividend(BigDecimal prodDividend) {
		this.prodDividend = prodDividend;
	}

	public Long getUserCardId() {
		return userCardId;
	}

	public void setUserCardId(Long userCardId) {
		this.userCardId = userCardId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getChannel() {
		return channel;
	}

	public void setChannel(Integer channel) {
		this.channel = channel;
	}

	public String getContract_no() {
		return contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	public String getCreditorPhoneNum() {
		return creditorPhoneNum;
	}

	public void setCreditorPhoneNum(String creditorPhoneNum) {
		this.creditorPhoneNum = creditorPhoneNum;
	}



	public Integer getCommissionStatus() {
		return commissionStatus;
	}

	public void setCommissionStatus(Integer commissionStatus) {
		this.commissionStatus = commissionStatus;
	}

	public Integer getCommissionPayTime() {
		return commissionPayTime;
	}

	public void setCommissionPayTime(Integer commissionPayTime) {
		this.commissionPayTime = commissionPayTime;
	}


	public BigDecimal getRealPayAmt() {
		return realPayAmt;
	}

	public void setRealPayAmt(BigDecimal realPayAmt) {
		this.realPayAmt = realPayAmt;
	}

	public BigDecimal getBenefitAmt() {
		return benefitAmt;
	}

	public void setBenefitAmt(BigDecimal benefitAmt) {
		this.benefitAmt = benefitAmt;
	}

	public BigDecimal getPayBackAmt() {
		return payBackAmt;
	}

	public void setPayBackAmt(BigDecimal payBackAmt) {
		this.payBackAmt = payBackAmt;
	}

	public BigDecimal getProRate() {
		return proRate;
	}

	public void setProRate(BigDecimal proRate) {
		this.proRate = proRate;
	}



	public int getBenefitType() {
		return benefitType;
	}

	public void setBenefitType(int benefitType) {
		this.benefitType = benefitType;
	}

	public BigDecimal getExtRate() {
		return extRate;
	}

	public void setExtRate(BigDecimal extRate) {
		this.extRate = extRate;
	}

	public BigDecimal getTotalRate() {
		return totalRate;
	}

	public void setTotalRate(BigDecimal totalRate) {
		this.totalRate = totalRate;
	}

	public int getExtraTerm() {
		return extraTerm;
	}

	public void setExtraTerm(int extraTerm) {
		this.extraTerm = extraTerm;
	}


	public String getContractUrl() {
		return contractUrl;
	}

	public void setContractUrl(String contractUrl) {
		this.contractUrl = contractUrl;
	}

	public BigDecimal getProdAddRate() {
		return prodAddRate;
	}

	public void setProdAddRate(BigDecimal prodAddRate) {
		this.prodAddRate = prodAddRate;
	}

	public BigDecimal getAddRateDividend() {
		return addRateDividend;
	}

	public void setAddRateDividend(BigDecimal addRateDividend) {
		this.addRateDividend = addRateDividend;
	}

	public BigDecimal getUserCardRate() {
		return userCardRate;
	}

	public void setUserCardRate(BigDecimal userCardRate) {
		this.userCardRate = userCardRate;
	}

	public BigDecimal getUserCardDividend() {
		return userCardDividend;
	}

	public void setUserCardDividend(BigDecimal userCardDividend) {
		this.userCardDividend = userCardDividend;
	}

	@Override
	public String toString() {
		return "OrderInfo [id=" + id + ", userId=" + userId + ", accountId="
				+ accountId + ", brokerOrderNo=" + brokerOrderNo
				+ ", companyId=" + companyId + ", productId=" + productId
				+ ", canCelable=" + canCelable + ", confirmStatus="
				+ confirmStatus + ", convertSuccessAmount="
				+ convertSuccessAmount + ", convertSuccessShare="
				+ convertSuccessShare + ", destFundCode=" + destFundCode
				+ ", destShareType=" + destShareType + ", errorCode="
				+ errorCode + ", errorMessage=" + errorMessage + ", fee=" + fee
				+ ", fundOrderCode=" + fundOrderCode + ", orderConfirmDate="
				+ orderConfirmDate + ", orderCreatedOn=" + orderCreatedOn
				+ ", orderId=" + orderId + ", orderTradeDate=" + orderTradeDate
				+ ", paymentMethodId=" + paymentMethodId + ", payStatus="
				+ payStatus + ", shareType=" + shareType + ", successAmount="
				+ successAmount + ", successShare=" + successShare
				+ ", tradeAmount=" + tradeAmount + ", dividend=" + dividend
				+ ", tradeCurrency=" + tradeCurrency + ", tradeShare="
				+ tradeShare + ", vastlyRedeemFlag=" + vastlyRedeemFlag
				+ ", financialPlannerId=" + financialPlannerId + ", paymentId="
				+ paymentId + ", dividendDate=" + dividendDate + ", userName="
				+ userName + ", extraDividend=" + extraDividend
				+ ", prodDividend=" + prodDividend + ", userCardId="
				+ userCardId + ", channelName=" + channelName + ", channel="
				+ channel + ", orgId=" + orgId + ", contract_no=" + contract_no
				+ ", creditorPhoneNum=" + creditorPhoneNum
				+ ", commissionStatus=" + commissionStatus
				+ ", commissionPayTime=" + commissionPayTime + ", realPayAmt="
				+ realPayAmt + ", benefitAmt=" + benefitAmt + ", payBackAmt="
				+ payBackAmt + ", proRate=" + proRate + ", extRate=" + extRate
				+ ", totalRate=" + totalRate + ", benefitType=" + benefitType
				+ "]";
	}



}
