package pay.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_parameter_info")
public class Parameter implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6640313513737382149L;
	
	@Id
    @GeneratedValue (generator =  "paymentableGenerator" )    
    @GenericGenerator(name =  "paymentableGenerator" , strategy =  "increment" )
	private Integer id;
	
	/** 参数名**/
	private String name;
	
	/** 标识符**/
	private String flag;
	
	/** 参数值01**/
	private String value01;
	
	/** 参数值02**/
	private String value02;
	
	/** 参数值03**/
	private String value03;
	
	/** 参数值04**/
	private String value04;
	 
	/** 描述 **/
	private String descStr;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getValue01() {
		return value01;
	}

	public void setValue01(String value01) {
		this.value01 = value01;
	}

	public String getValue02() {
		return value02;
	}

	public void setValue02(String value02) {
		this.value02 = value02;
	}

	public String getValue03() {
		return value03;
	}

	public void setValue03(String value03) {
		this.value03 = value03;
	}

	public String getValue04() {
		return value04;
	}

	public void setValue04(String value04) {
		this.value04 = value04;
	}

	public String getDescStr() {
		return descStr;
	}

	public void setDescStr(String descStr) {
		this.descStr = descStr;
	}

}
