//package pay.entity;
//
//import java.io.Serializable;
// 
//
//import java.sql.Timestamp;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.GenericGenerator;
//
////用户充值记录
//@Entity
//@Table(name = "t_preAuth_info")
//public class PreAuth implements Serializable {
//
//	/**
//   * 
//   */
//	private static final long serialVersionUID = 7142926961257113998L;
//
//	@Id
//	@GeneratedValue (generator =  "paymentableGenerator" )    
//    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" )   
//	private Long id;
//
//	/**
//	 * 响应码
//	 */
//	private String resp_code;
//
//	/**
//	 * 商户代码
//	 */
//	private String mchnt_cd;
//	
//	/**
//	 * 请求流水号
//	 */
//	private String mchnt_txn_ssn;
//	
//	/**
//	 * 预授权合同号
//	 */
//	private String contract_no;
//	
//	/**
//	 * 金额
//	 */
//	private String amount;
//	
//	/**
//	 * 产品名称
//	 */
//	public String productName;
//	
//	/**
//	 * 描述
//	 */
//	private String preAuthDesc;
//	
//	private Long userId;
//	
//
//	private Timestamp cts;
//	
//	private Long orderId;
//	
//	
//	
//	
//
//	public Long getOrderId() {
//		return orderId;
//	}
//
//	public void setOrderId(Long orderId) {
//		this.orderId = orderId;
//	}
//
//	public Long getUserId() {
//		return userId;
//	}
//
//	public void setUserId(Long userId) {
//		this.userId = userId;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getResp_code() {
//		return resp_code;
//	}
//
//	public void setResp_code(String resp_code) {
//		this.resp_code = resp_code;
//	}
//
//
//	public String getMchnt_cd() {
//		return mchnt_cd;
//	}
//
//	public void setMchnt_cd(String mchnt_cd) {
//		this.mchnt_cd = mchnt_cd;
//	}
//
//	public String getMchnt_txn_ssn() {
//		return mchnt_txn_ssn;
//	}
//
//	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
//		this.mchnt_txn_ssn = mchnt_txn_ssn;
//	}
//
//	public Timestamp getCts() {
//		return cts;
//	}
//
//	public void setCts(java.sql.Timestamp timestamp) {
//		this.cts = timestamp;
//	}
//
//	public String getAmount() {
//		return amount;
//	}
//
//	public void setAmount(String amount) {
//		this.amount = amount;
//	}
//	
//	public String getContract_no() {
//		return contract_no;
//	}
//
//	public void setContract_no(String contract_no) {
//		this.contract_no = contract_no;
//	}
//
//	public String getPreAuthDesc() {
//		return preAuthDesc;
//	}
//
//	public void setPreAuthDesc(String preAuthDesc) {
//		this.preAuthDesc = preAuthDesc;
//	}
//
//	public String getProductName() {
//		return productName;
//	}
//
//	public void setProductName(String productName) {
//		this.productName = productName;
//	}
//
//	@Override
//	public String toString() {
//		return "PreAuth [id=" + id + ", resp_code=" + resp_code + ", mchnt_cd="
//				+ mchnt_cd + ", mchnt_txn_ssn=" + mchnt_txn_ssn
//				+ ", contract_no=" + contract_no + ", amount=" + amount
//				+ ", productName=" + productName + ", preAuthDesc="
//				+ preAuthDesc + ", userId=" + userId + ", cts=" + cts
//				+ ", orderId=" + orderId + "]";
//	}
//
//	
//	
//}
//
