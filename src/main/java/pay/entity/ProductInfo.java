package pay.entity;


import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "t_product_info")
public class ProductInfo  implements Serializable,  Comparable<ProductInfo> {

	/**
	 *
	 *
	 */
	private static final long serialVersionUID = -279155347042731997L;

	@Id
	public Long id;

	/**
	 * 产品编码
	 * */
	public String proNo;

	/**
	 * 产品名称
	 * */
	public String name;

	/**
	 * 产品状态, 默认为未上架
	 * */
	public Integer proStatus = 0;

	/** 产品类型 股票、债券、股权、债权、房地产、衍生品、文化传媒、其他 */
	public Integer prodType;

	/** 产品一级目录 */
	public Integer prodTopType;

	/**
	 * 产品期限 , 用于计算
	 * */
	public Integer proTerm;

	/**
	 * 产品期限 ,用于显示
	 * */
	public String proTermStr;


	/**
	 * 是否自动起息, 0 为手动起息,1为自动起息
	 */
	private Integer autStartDiv;


	/**
	 * 是否起息, 0 为没有起息,1为起息, 默认为未起息
	 */
	private Integer startDiv = 0;

	/**
	 * 是否为测试标, 0 为否， 1为是, 默认为不是测试标
	 *
	 * */
	private Integer isTest = 0;

	/**
	 * 产品起息日
	 * */
	public Timestamp proDividStartDate;

	/**
	 * 还本付息时间
	 * */
	public Timestamp proDividendDate;

	/**
	 * 发行方
	 * */
	public Integer issuerId;

	/**
	 * 募集开始时间
	 * */
	public Timestamp raiseBeginDate;

	/**
	 * 募集结束时间
	 * */
	public Timestamp raiseEndDate;


	/**
	 * 上架时间
	 * */
	public Timestamp startSaleTime;

	/**
	 * 下架时间
	 * */
	public Timestamp endSaleTime;


	/**
	 * 改变buyType 时间，用于将代售产品变为可售产品
	 * */
	public Timestamp  chgBuyTypeTime ;

	/**
	 * 自动满标时间
	 * */
	public Timestamp saleOutTime;

	/**
	 * 是否为父标，1：是， 0：否
	 */
	public Integer isParent = 0;


	/**
	 * 管理人
	 * */
	public String custodian;

	/**
	 * 托管人
	 */
	public String trustee;

	/**
	 * 产品费用
	 * */
	public BigDecimal proCost;

	/**
	 * 债权推荐方
	 * */
	public String claimRecommend;

	/**
	 * 债权推荐方介绍
	 * */
	@Column(length = 500)
	public String claimRecommendInfo;

	/**
	 * 债权转让服务方
	 * */
	public String creditAssignment;

	/**
	 * 债权转让服务方简介
	 * */
	@Column(length = 500)
	public String creditAssignmentInfo;

	/**
	 * 收益分配方式
	 * */
	public String incomeDisType;

	/**
	 * 投资范围
	 */
	public String fundUse;

	/**
	 * 认购起点 信托、私募基金、公募基金、基金子公司产品、券商资管、有限合伙、债权转让、其他
	 * */
	public String subscriptionOrigin;

	/**
	 * 发行方式
	 * */
	public String publishType;

	/**
	 * 融资金额
	 * */
	public BigDecimal financingAccount;

	/**
	 * 已募集额度
	 * */
	public BigDecimal yraiseAccount;

	/**
	 * 剩余额度
	 * */
	public BigDecimal sraiseAccount;

	/**
	 * 预约额度
	 * */
	public BigDecimal bespeakAccount;

	/**
	 * 购买渠道 0:只能线上预约, 1:火柴支付， 2:富有支付
	 */
	public Integer buyChannel;

	/**
	 * 是否待售， 1：待售产品，0：非待售产品
	 */
	public Integer waitSale = 0;


	/**
	 * 购买类型， 1：购买， 2：预约
	 */
	public Integer buyType;

	/**
	 * 是否定向，1：是，0：否(默认)
	 */
	private Integer directional = 0;

	/**
	 * 债权人性质（0:个人；1:企业）
	 */
	private Integer creditorType = 0;

	/**
	 * 回款手续费(债权人性质为企业时才用上)
	 */
	private BigDecimal repayPoundage ;

	/**
	 * 风险等级 R1-R5（R1为低风险、R2为中低风险、R3中风险、R4为中高风险、R5为高风险）
	 */

	public String riskLevel;

	/**
	 * 回款来源
	 * */
	@Column(length = 500)
	public String paymentSource;

	/**
	 * 最低年化收益
	 * */
	public String minYearRate;

	/**
	 * 最高年化收益
	 * */
	public String maxYearRate;

	/**
	 * 产品介绍
	 * */
	@Column(name = "introduce", columnDefinition = "MediumBlob")
	public byte[] introduce;

	/**
	 * 投资者收益（年化）
	 * */
	@Column(name = "income", columnDefinition = "MediumBlob")
	public byte[] income;

	/**
	 * 产品风控
	 * */
	@Column(name = "riskctrl", columnDefinition = "MediumBlob")
	public byte[] riskctrl;



	/**
	 * 如何签约
	 * */
	@Column(name = "signFlow", columnDefinition = "MediumBlob")
	public byte[] signFlow;


	/**
	 * 佣金系数
	 * */
	public String commission;

	/**
	 * 前端基础佣金系数
	 * */
	public String baseCommissionFront;

	/**
	 * 后端基础佣金系数
	 * */
	public String baseCommissionBack;

	/**
	 * 折标系数
	 * */
	public String niggerHead;

	/**
	 * 币种 美元、人民币、澳元、韩元、加元、法币、英镑
	 */
	public Integer currency;

	/**
	 * 备注
	 * */
	@Column(length = 500)
	public String remark;

	/**
	 * 对产品进行排序, 倒序排列
	 */
	public Integer sequence = Integer.MAX_VALUE;


	/**
	 * 最低追加金额
	 * */
	public BigDecimal minAppendAccount;

	/** 成立日期 */
	public Timestamp establishDate;

	/** 申请上线日期 */
	public Timestamp onlineDate;

	/** 终止日期 */
	public Timestamp endDate;

	/** 上线发行日期 */
	public Timestamp releasesDate;

	/** 结算日期 */
	public Timestamp balanceDate;

	/** 预计清算时间 */
	public Timestamp clearDate;

	/**
	 * 对公募集账号
	 * */
	public String openRaiseAccount;

	/**
	 * 募集账户开户人姓名
	 * */
	public String openRaisePersonName;



	/**
	 * 募集账户开户行
	 * */
	public String openRaiseBankName;

	/**
	 * 是否适应海外人群
	 * */
	public Integer isOversea;

	/**
	 * 贴息 0无、1有
	 * */
	public Integer discount;

	/**
	 * 贴息截止日期
	 * */

	// 建议删除
	public Timestamp discountDate;

	/**
	 * 贴息开始日期
	 * */
	public Timestamp discountBeginDate;

	/**
	 * 贴息截止日期
	 * */
	public Timestamp discountEndDate;

	/**
	 * 贴息率 0无、1有
	 * */
	public String discountRate;

	/**
	 * 投资方向
	 * */
	public String investmentDirect;

	/**
	 * 产品亮点
	 * */
	@Column(length = 500)
	public String salePoint;

	/**
	 * 创建日期
	 * */
	public Timestamp createDate;

	/**
	 * 是否热销,0为非热销, 1为热销, 2为新上架, 3 代售
	 */
	public Integer hotSale;

	/**
	 * 是否放在首页销售,0为非, 1为是
	 */
	public Integer TopSale;

	/**
	 * 根据渠道确定是否放在首页显示
	 */
	public String topSaleStr;

	/**
	 * 是否可用优惠券， 0为不可用，1为可用
	 */

	public Integer useCard = 0;

	/**
	 * 是否售罄， 0为否，1为是
	 */
	public Integer saleOut = 0;

	/**
	 * 债权人手机号
	 *
	 */
	public String creditorPhoneNum;


	/**
	 * 起息方式
	 * @return
	 */
	public Integer divStartType;

	/**
	 * 合同id
	 */
	public Long contractId;

	/**
	 * 父标id
	 */
	public Long  parId;


	/**
	 * 是否已回款, 0:未回款，1:已回款
	 */
	public Integer payBackStatus;



	/**
	 * 产品售卖类型， 1：普通产品， 2：体验标
	 */
	public Integer saleType;


	/**
	 * 限购笔数
	 */
	public Integer limitNum;

	/**
	 * 限购金额
	 */
	public BigDecimal limitAmount;

	/**
	 * 标签01
	 */
	public String label01;

	/**
	 * 华彩通显示颜色
	 */
	public String label01Color01;

	/**
	 * 理财师显示颜色
	 */
	public String label01Color02;

	/**
	 * PC显示颜色
	 */
	public String label01Color03;

	/**
	 * 标签02
	 */
	public String label02;

	/**
	 * 华彩通显示颜色
	 */
	public String label02Color01;

	/**
	 * 理财师显示颜色
	 */
	public String label02Color02;

	/**
	 * PC显示颜色
	 */
	public String label02Color03;

	/**
	 * 标签03
	 */
	public String label03;

	/**
	 * 华彩通显示颜色
	 */
	public String label03Color01;

	/**
	 * 理财师显示颜色
	 */
	public String label03Color02;

	/**
	 * PC显示颜色
	 */
	public String label03Color03;

	/**
	 * 标签03
	 */
	public String label04;

	/**
	 * 华彩通显示颜色
	 */
	public String label04Color01;

	/**
	 * 理财师显示颜色
	 */
	public String label04Color02;

	/**
	 * PC显示颜色
	 */
	public String label04Color03;


	public int version;

	public Long addRateProduct01;

	public Long addRateProduct02;

	private int isPayedFee=0;

	private Long updateTime;

	//活动标签内容
	private String actFlag;

	//活动标签背景
	private Integer actBack;


	//产品标签01
	private String prodFlag01;


	//产品标签02
	private String prodFlag02;

	//额外加息收益
	private String addRate;

	//协议产品名称
	private String agreementProductName;

	public String getAgreementProductName() {
		return agreementProductName;
	}

	public void setAgreementProductName(String agreementProductName) {
		this.agreementProductName = agreementProductName;
	}

	public String getProdFlag01() {
		return prodFlag01;
	}

	public void setProdFlag01(String prodFlag01) {
		this.prodFlag01 = prodFlag01;
	}

	public String getProdFlag02() {
		return prodFlag02;
	}

	public void setProdFlag02(String prodFlag02) {
		this.prodFlag02 = prodFlag02;
	}

	public String getAddRate() {
		return addRate;
	}

	public void setAddRate(String addRate) {
		this.addRate = addRate;
	}

	public String getActFlag() {
		return actFlag;
	}

	public void setActFlag(String actFlag) {
		this.actFlag = actFlag;
	}

	public Integer getActBack() {
		return actBack;
	}

	public void setActBack(Integer actBack) {
		this.actBack = actBack;
	}

	public Integer getLimitNum() {
		return limitNum;
	}

	public void setLimitNum(Integer limitNum) {
		this.limitNum = limitNum;
	}

	public Long getAddRateProduct01() {
		return addRateProduct01;
	}

	public void setAddRateProduct01(Long addRateProduct01) {
		this.addRateProduct01 = addRateProduct01;
	}

	public Long getAddRateProduct02() {
		return addRateProduct02;
	}

	public void setAddRateProduct02(Long addRateProduct02) {
		this.addRateProduct02 = addRateProduct02;
	}

	public Integer getSaleType() {
		return saleType;
	}

	public void setSaleType(Integer saleType) {
		this.saleType = saleType;
	}

	public String getCreditorPhoneNum() {
		return creditorPhoneNum;
	}

	public void setCreditorPhoneNum(String creditorPhoneNum) {
		this.creditorPhoneNum = creditorPhoneNum;
	}

	public Long getId() {
		return id;
	}

	public byte[] getSignFlow() {
		return signFlow;
	}

	public void setSignFlow(byte[] signFlow) {
		this.signFlow = signFlow;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProNo() {
		return proNo;
	}

	public void setProNo(String proNo) {
		this.proNo = proNo;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProStatus() {
		return proStatus;
	}

	public void setProStatus(Integer proStatus) {
		this.proStatus = proStatus;
	}

	public Integer getProdType() {
		return prodType;
	}

	public void setProdType(Integer prodType) {
		this.prodType = prodType;
	}

	public Integer getProTerm() {
		return proTerm;
	}

	public String getProTermStr() {

		return proTermStr;
	}

	public void setProTerm(Integer proTerm) {
		this.proTerm = proTerm;
	}

	public String getClaimRecommend() {
		return claimRecommend;
	}

	public void setClaimRecommend(String claimRecommend) {
		this.claimRecommend = claimRecommend;
	}

	public String getClaimRecommendInfo() {
		return claimRecommendInfo;
	}

	public void setClaimRecommendInfo(String claimRecommendInfo) {
		this.claimRecommendInfo = claimRecommendInfo;
	}

	public String getCreditAssignment() {
		return creditAssignment;
	}

	public void setCreditAssignment(String creditAssignment) {
		this.creditAssignment = creditAssignment;
	}

	public String getCreditAssignmentInfo() {
		return creditAssignmentInfo;
	}

	public void setCreditAssignmentInfo(String creditAssignmentInfo) {
		this.creditAssignmentInfo = creditAssignmentInfo;
	}

	public String getIncomeDisType() {
		return incomeDisType;
	}

	public void setIncomeDisType(String incomeDisType) {
		this.incomeDisType = incomeDisType;
	}

	public String getFundUse() {
		return fundUse;
	}

	public void setFundUse(String fundUse) {
		this.fundUse = fundUse;
	}

	public Integer getUseCard() {
		return useCard;
	}

	public void setUseCard(Integer useCard) {
		this.useCard = useCard;
	}

	public String getSubscriptionOrigin() {
		return subscriptionOrigin;
	}

	public void setSubscriptionOrigin(String subscriptionOrigin) {
		this.subscriptionOrigin = subscriptionOrigin;
	}

	public String getPublishType() {
		return publishType;
	}

	public Timestamp getDiscountEndDate() {
		return discountEndDate;
	}

	public Integer getTopSale() {
		return TopSale;
	}

	public void setTopSale(Integer topSale) {
		TopSale = topSale;
	}

	public void setDiscountEndDate(Timestamp discountEndDate) {
		this.discountEndDate = discountEndDate;
	}

	public Timestamp getDiscountBeginDate() {
		return discountBeginDate;
	}

	public void setDiscountBeginDate(Timestamp discountBeginDate) {
		this.discountBeginDate = discountBeginDate;
	}

	public void setPublishType(String publishType) {
		this.publishType = publishType;
	}

	public BigDecimal getFinancingAccount() {
		return financingAccount;
	}

	public Integer getHotSale() {
		return hotSale;
	}

	public void setHotSale(Integer hotSale) {
		this.hotSale = hotSale;
	}

	public void setFinancingAccount(BigDecimal financingAccount) {
		this.financingAccount = financingAccount;
	}

	public String getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}

	public String getPaymentSource() {
		return paymentSource;
	}

	public void setPaymentSource(String paymentSource) {
		this.paymentSource = paymentSource;
	}

	public String getMinYearRate() {
		return minYearRate;
	}

	public void setMinYearRate(String minYearRate) {
		this.minYearRate = minYearRate;
	}

	public String getMaxYearRate() {
		return maxYearRate;
	}

	public void setMaxYearRate(String maxYearRate) {
		this.maxYearRate = maxYearRate;
	}

	public byte[] getIntroduce() {
		return introduce;
	}

	public void setIntroduce(byte[] introduce) {
		this.introduce = introduce;
	}

	public byte[] getIncome() {
		return income;
	}

	public void setIncome(byte[] income) {
		this.income = income;
	}

	public byte[] getRiskctrl() {
		return riskctrl;
	}

	public void setRiskctrl(byte[] riskctrl) {
		this.riskctrl = riskctrl;
	}

	public String getNiggerHead() {
		return niggerHead;
	}

	public void setNiggerHead(String niggerHead) {
		this.niggerHead = niggerHead;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public BigDecimal getMinAppendAccount() {
		return minAppendAccount;
	}

	public void setMinAppendAccount(BigDecimal minAppendAccount) {
		this.minAppendAccount = minAppendAccount;
	}

	public Timestamp getEstablishDate() {
		return establishDate;
	}

	public void setEstablishDate(Timestamp establishDate) {
		this.establishDate = establishDate;
	}

	public Timestamp getOnlineDate() {
		return onlineDate;
	}

	public void setOnlineDate(Timestamp onlineDate) {
		this.onlineDate = onlineDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Timestamp getReleasesDate() {
		return releasesDate;
	}

	public void setReleasesDate(Timestamp releasesDate) {
		this.releasesDate = releasesDate;
	}

	public Timestamp getBalanceDate() {
		return balanceDate;
	}

	public void setBalanceDate(Timestamp balanceDate) {
		this.balanceDate = balanceDate;
	}

	public Timestamp getClearDate() {
		return clearDate;
	}

	public void setClearDate(Timestamp clearDate) {
		this.clearDate = clearDate;
	}

	public String getOpenRaiseAccount() {
		return openRaiseAccount;
	}

	public void setOpenRaiseAccount(String openRaiseAccount) {
		this.openRaiseAccount = openRaiseAccount;
	}

	public Integer getIsOversea() {
		return isOversea;
	}

	public void setIsOversea(Integer isOversea) {
		this.isOversea = isOversea;
	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public Timestamp getDiscountDate() {
		return discountDate;
	}

	public void setDiscountDate(Timestamp discountDate) {
		this.discountDate = discountDate;
	}

	public String getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(String discountRate) {
		this.discountRate = discountRate;
	}

	public String getInvestmentDirect() {
		return investmentDirect;
	}

	public void setInvestmentDirect(String investmentDirect) {
		this.investmentDirect = investmentDirect;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getRaiseBeginDate() {
		return raiseBeginDate;
	}

	public void setRaiseBeginDate(Timestamp raiseBeginDate) {
		this.raiseBeginDate = raiseBeginDate;
	}

	public Timestamp getRaiseEndDate() {
		return raiseEndDate;
	}

	public void setRaiseEndDate(Timestamp raiseEndDate) {
		this.raiseEndDate = raiseEndDate;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getBaseCommissionFront() {
		return baseCommissionFront;
	}

	public void setBaseCommissionFront(String baseCommissionFront) {
		this.baseCommissionFront = baseCommissionFront;
	}

	public String getBaseCommissionBack() {
		return baseCommissionBack;
	}

	public void setBaseCommissionBack(String baseCommissionBack) {
		this.baseCommissionBack = baseCommissionBack;
	}

	public String getCustodian() {
		return custodian;
	}

	public void setCustodian(String custodian) {
		this.custodian = custodian;
	}

	public BigDecimal getProCost() {
		return proCost;
	}

	public void setProCost(BigDecimal proCost) {
		this.proCost = proCost;
	}

	public BigDecimal getYraiseAccount() {
		return yraiseAccount;
	}

	public void setYraiseAccount(BigDecimal yraiseAccount) {
		this.yraiseAccount = yraiseAccount;
	}

	public BigDecimal getSraiseAccount() {
		return sraiseAccount;
	}

	public void setSraiseAccount(BigDecimal sraiseAccount) {
		this.sraiseAccount = sraiseAccount;
	}

	public Integer getProdTopType() {
		return prodTopType;
	}

	public void setProdTopType(Integer prodTopType) {
		this.prodTopType = prodTopType;
	}

	public String getTrustee() {
		return trustee;
	}

	public void setTrustee(String trustee) {
		this.trustee = trustee;
	}

	public String getOpenRaisePersonName() {
		return openRaisePersonName;
	}

	public void setOpenRaisePersonName(String openRaisePersonName) {
		this.openRaisePersonName = openRaisePersonName;
	}

	public String getOpenRaiseBankName() {
		return openRaiseBankName;
	}

	public void setOpenRaiseBankName(String openRaiseBankName) {
		this.openRaiseBankName = openRaiseBankName;
	}

	public Integer getBuyChannel() {
		return buyChannel;
	}

	public void setBuyChannel(Integer buyChannel) {
		this.buyChannel = buyChannel;
	}

	public BigDecimal getBespeakAccount() {
		return bespeakAccount;
	}

	public void setBespeakAccount(BigDecimal bespeakAccount) {
		this.bespeakAccount = bespeakAccount;
	}

	public Integer getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(Integer issuerId) {
		this.issuerId = issuerId;
	}

	public String getSalePoint() {
		return salePoint;
	}

	public void setSalePoint(String salePoint) {
		this.salePoint = salePoint;
	}

	public Integer getSaleOut() {
		return saleOut;
	}

	public void setSaleOut(Integer saleOut) {
		this.saleOut = saleOut;
	}

	public void setProTermStr(String proTermStr) {
		this.proTermStr = proTermStr;
	}

	public Timestamp getProDividendDate() {
		return proDividendDate;
	}

	public void setProDividendDate(Timestamp proDividendDate) {
		this.proDividendDate = proDividendDate;
	}

	public Integer getAutStartDiv() {
		return autStartDiv;
	}

	public void setAutStartDiv(Integer autStartDiv) {
		this.autStartDiv = autStartDiv;
	}

	public Integer getStartDiv() {
		return startDiv;
	}

	public void setStartDiv(Integer startDiv) {
		this.startDiv = startDiv;
	}

	public Timestamp getProDividStartDate() {
		return proDividStartDate;
	}

	public void setProDividStartDate(Timestamp proDividStartDate) {
		this.proDividStartDate = proDividStartDate;
	}

	public Integer getDivStartType() {
		return divStartType;
	}

	public void setDivStartType(Integer divStartType) {
		this.divStartType = divStartType;
	}



	public Long getContractId() {
		return contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	public Long getParId() {
		return parId;
	}

	public void setParId(Long parId) {
		this.parId = parId;
	}

	public String getLabel01() {
		return label01;
	}

	public void setLabel01(String label01) {
		this.label01 = label01;
	}

	public String getLabel02() {
		return label02;
	}

	public void setLabel02(String label02) {
		this.label02 = label02;
	}

	public String getLabel03() {
		return label03;
	}

	public void setLabel03(String label03) {
		this.label03 = label03;
	}

	public Integer getPayBackStatus() {
		return payBackStatus;
	}

	public void setPayBackStatus(Integer payBackStatus) {
		this.payBackStatus = payBackStatus;
	}

	public Timestamp getStartSaleTime() {
		return startSaleTime;
	}

	public void setStartSaleTime(Timestamp startSaleTime) {
		this.startSaleTime = startSaleTime;
	}

	public Timestamp getEndSaleTime() {
		return endSaleTime;
	}

	public void setEndSaleTime(Timestamp endSaleTime) {
		this.endSaleTime = endSaleTime;
	}

	public Integer getIsParent() {
		return isParent;
	}

	public void setIsParent(Integer isParent) {
		this.isParent = isParent;
	}

	public Integer getIsTest() {
		return isTest;
	}

	public void setIsTest(Integer isTest) {
		this.isTest = isTest;
	}

	public Timestamp getSaleOutTime() {
		return saleOutTime;
	}

	public void setSaleOutTime(Timestamp saleOutTime) {
		this.saleOutTime = saleOutTime;
	}

	public BigDecimal getLimitAmount() {
		return limitAmount;
	}

	public void setLimitAmount(BigDecimal limitAmount) {
		this.limitAmount = limitAmount;
	}

	public int getIsPayedFee() {
		return isPayedFee;
	}

	public void setIsPayedFee(int isPayedFee) {
		this.isPayedFee = isPayedFee;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTopSaleStr() {
		return topSaleStr;
	}

	public void setTopSaleStr(String topSaleStr) {
		this.topSaleStr = topSaleStr;
	}


	public String getLabel01Color01() {
		return label01Color01;
	}

	public void setLabel01Color01(String label01Color01) {
		this.label01Color01 = label01Color01;
	}

	public String getLabel01Color02() {
		return label01Color02;
	}

	public void setLabel01Color02(String label01Color02) {
		this.label01Color02 = label01Color02;
	}

	public String getLabel01Color03() {
		return label01Color03;
	}

	public void setLabel01Color03(String label01Color03) {
		this.label01Color03 = label01Color03;
	}

	public String getLabel02Color01() {
		return label02Color01;
	}

	public void setLabel02Color01(String label02Color01) {
		this.label02Color01 = label02Color01;
	}

	public String getLabel02Color02() {
		return label02Color02;
	}

	public void setLabel02Color02(String label02Color02) {
		this.label02Color02 = label02Color02;
	}

	public String getLabel02Color03() {
		return label02Color03;
	}

	public void setLabel02Color03(String label02Color03) {
		this.label02Color03 = label02Color03;
	}

	public String getLabel03Color01() {
		return label03Color01;
	}

	public void setLabel03Color01(String label03Color01) {
		this.label03Color01 = label03Color01;
	}

	public String getLabel03Color02() {
		return label03Color02;
	}

	public void setLabel03Color02(String label03Color02) {
		this.label03Color02 = label03Color02;
	}

	public String getLabel03Color03() {
		return label03Color03;
	}

	public void setLabel03Color03(String label03Color03) {
		this.label03Color03 = label03Color03;
	}

	public String getLabel04() {
		return label04;
	}

	public void setLabel04(String label04) {
		this.label04 = label04;
	}

	public String getLabel04Color01() {
		return label04Color01;
	}

	public void setLabel04Color01(String label04Color01) {
		this.label04Color01 = label04Color01;
	}

	public String getLabel04Color02() {
		return label04Color02;
	}

	public void setLabel04Color02(String label04Color02) {
		this.label04Color02 = label04Color02;
	}

	public String getLabel04Color03() {
		return label04Color03;
	}

	public void setLabel04Color03(String label04Color03) {
		this.label04Color03 = label04Color03;
	}


	public Integer getBuyType() {
		return buyType;
	}

	public void setBuyType(Integer buyType) {
		this.buyType = buyType;
	}

	public Integer getWaitSale() {
		return waitSale;
	}

	public void setWaitSale(Integer waitSale) {
		this.waitSale = waitSale;
	}

	public Timestamp getChgBuyTypeTime() {
		return chgBuyTypeTime;
	}

	public void setChgBuyTypeTime(Timestamp chgBuyTypeTime) {
		this.chgBuyTypeTime = chgBuyTypeTime;
	}

	public Integer getDirectional() {
		return directional;
	}

	public void setDirectional(Integer directional) {
		this.directional = directional;
	}

	public Integer getCreditorType() {
		return creditorType;
	}

	public void setCreditorType(Integer creditorType) {
		this.creditorType = creditorType;
	}

	public BigDecimal getRepayPoundage() {
		return repayPoundage;
	}

	public void setRepayPoundage(BigDecimal repayPoundage) {
		this.repayPoundage = repayPoundage;
	}


	@Override
	public int compareTo(ProductInfo o) {
		if (this == null ||  o == null) {
			return 0;
		}

		//01 对比sequence
		int compareResult = compareSequence(this, o);
		//System.out.println("compareSequence " + this.getName() + " : " + o.getName() + " : " +  compareResult );
		if(compareResult != 0){
			return compareResult;
		}

		//02 对比是否满标
		compareResult = compareSaleOut(this, o);
		//System.out.println("compareSaleOut " + this.getName() + " : " + o.getName() + " : " +  compareResult );
		if(compareResult != 0){
			return compareResult;
		}


		//03 对比支付渠道
		compareResult = compareBuyType(this, o);
		//System.out.println("compareBuyChannel " + this.getName() + " : " + o.getName() + " : " + compareResult );
		if(compareResult != 0){
			return compareResult;
		}


		//04 对比父标
		compareResult = compareParId(this, o);

		//System.out.println("compareParId " + this.getName() + " : " + o.getName() + " : " +  compareResult );

		if(compareResult != 0){
			return compareResult;
		}

		//05 对比产品收益
		compareResult = compareProdRate(this, o);

		//System.out.println("compareProdRate " + this.getName() + " : " + o.getName() + " : " +  compareResult );

		return compareResult ;


	}


	public int compareSequence(ProductInfo x, ProductInfo y){

		//System.out.println("compareSequence : " + " x.name = " + x.getName() + " sequence = " +  x.getSequence() + y.getName() + " sequence = " + y.getSequence() );
		if(x.getSequence() == null && y.getSequence() == null){
			return  0;
		}else if(x.getSequence() != null && y.getSequence() == null){
			return -1;
		}else if(x.getSequence() == null && y.getSequence() != null){
			return 1;
		}else{
			return x.getSequence().compareTo(y.getSequence());
		}
	}


	public int compareBuyType(ProductInfo x, ProductInfo y){

		//System.out.println("compareBuyChannel " + x.getBuyType() + "  " + y.getBuyType() );
		if(x.getBuyType() == y.getBuyType() ){
			return 0;
		}else if(x.getBuyType() == 3){
			return -1;
		}else if(y.getBuyType() == 3){
			return 1;
		}else{
			return x.getBuyType().compareTo(y.getBuyType());
		}
	}

	public int compareParId(ProductInfo x, ProductInfo y){
		if(x.getParId() == null && y.getParId() == null){
			return  0;
		}else if(x.getParId() != null && y.getParId() == null){
			return -1;
		}else if(x.getParId() == null && y.getParId() != null){
			return 1;
		}else{
			return x.getParId().compareTo(y.getParId());
		}

	}

	public int compareSaleOut(ProductInfo x, ProductInfo y){
		int result = 0;
		if(x.getSaleOut() == y.getSaleOut() ){
			result =  0;
		}else if (x.getSaleOut() == 0 && y.getSaleOut() == 1){
			result =  -1;
		}else if (x.getSaleOut() == 1 && y.getSaleOut() == 0){
			result =  1;
		}
		return result;
	}


	public int compareProdRate(ProductInfo x, ProductInfo y){



		return   -1;

	}

	@Override
	public String toString() {
		return " ProductInfo{" +

				" sequence " + sequence +
				" id=" + id +
				", name='" + name + '\'' +
				", buyChannel=" + buyChannel +
				", buyType=" + buyType +
				", saleOut=" + saleOut +
				"   sequence " + name +
				'}';
	}
}
