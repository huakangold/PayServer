package pay.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "t_province_info")
public class Province implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4768461577384242898L;

	@Id
	@GeneratedValue (generator =  "paymentableGenerator" )    
    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" )   
	public Integer id;

	public Integer proCode;
	
	
	public String proName;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getProCode() {
		return proCode;
	}


	public void setProCode(Integer proCode) {
		this.proCode = proCode;
	}


	public String getProName() {
		return proName;
	}


	public void setProName(String proName) {
		this.proName = proName;
	}
}
