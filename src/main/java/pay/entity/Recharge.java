//package pay.entity;
//
//import java.io.Serializable;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.GenericGenerator;
//
////用户充值记录
//@Entity
//@Table(name = "t_recharge_info")
//public class Recharge implements Serializable {
//
//	/**
//   * 
//   */
//	private static final long serialVersionUID = 7142926961257113998L;
//
//	@Id
//	@GeneratedValue (generator =  "paymentableGenerator" )    
//    @GenericGenerator (name =  "paymentableGenerator" , strategy =  "increment" )   
//	private Long id;
//
//	/**
//	 * 响应码
//	 */
//	private String resp_code;
//
//	/**
//	 * 响应消息
//	 */
//	private String resp_desc;
//	
//	/**
//	 * 商户代码
//	 */
//	private String mchnt_cd;
//	
//	/**
//	 * 请求流水号
//	 */
//	private String mchnt_txn_ssn;
//	
//	/**
//	 * 交易用户
//	 */
//	private String login_id;
//	
//	/**
//	 * 金额
//	 */
//	private String amount;
//	
//	/**
//	 * 用户ID
//	 */
//	private Long userId;
//	
//	
//	private Long cts;
//	
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getResp_code() {
//		return resp_code;
//	}
//
//	public void setResp_code(String resp_code) {
//		this.resp_code = resp_code;
//	}
//
//	public String getResp_desc() {
//		return resp_desc;
//	}
//
//	public void setResp_desc(String resp_desc) {
//		this.resp_desc = resp_desc;
//	}
//
//	public String getMchnt_cd() {
//		return mchnt_cd;
//	}
//
//	public void setMchnt_cd(String mchnt_cd) {
//		this.mchnt_cd = mchnt_cd;
//	}
//
//	public String getMchnt_txn_ssn() {
//		return mchnt_txn_ssn;
//	}
//
//	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
//		this.mchnt_txn_ssn = mchnt_txn_ssn;
//	}
//
//	public String getLogin_id() {
//		return login_id;
//	}
//
//	public void setLogin_id(String login_id) {
//		this.login_id = login_id;
//	}
//
//
//
//	public Long getUserId() {
//		return userId;
//	}
//
//	public void setUserId(Long userId) {
//		this.userId = userId;
//	}
//
//	public Long getCts() {
//		return cts;
//	}
//
//	public void setCts(Long cts) {
//		this.cts = cts;
//	}
//
//	public String getAmount() {
//		return amount;
//	}
//
//	public void setAmount(String amount) {
//		this.amount = amount;
//	}
//	
//	
//	
//	
//}
//
