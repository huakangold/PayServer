package pay.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SysUser")
public class SysUser implements java.io.Serializable {

	private static final long serialVersionUID = -1584980532386228461L;

	@Id
	@Column(name = "Id", length = 20)
	private Long id;

	/**
	 * 投资者的盈米账户ID
	 */
	@Column(name = "accountId")
	private String accountId;

	/** 用户名(登录账号)，非空 */
	@Column(name = "Name", length = 15)
	private String name;

	/** 登录密码, 非空 */
	@Column(name = "Pwd", length = 100)
	private String pwd;

	/** 用户名(真实姓名) */
	@Column(name = "RealName", length = 100)
	private String realName;

	/** 生日 */
	@Column(name = "Birthday")
	private Long birthday;

	/** 身份证 */
	@Column(name = "Identity", length = 100)
	private String identity;

	/** 年龄 */
	@Column(name = "Age", length = 10)
	private Integer age;

	/**
	 * 性别 男:0,  女:1
	 */
	@Column(name = "Gender", length = 1)
	private Integer gender;

	/** 身体状况 */
	@Column(name = "HealthStatus", length = 100)
	private String healthStatus;

	/** 组织架构id */
	private Integer orgId;

	/** 在职类型 0在职，1离职 */
	@Column(name = "OnType", length = 10)
	private Integer onType;

	/** 入职时间 */
	@Column(name = "EntryTime")
	private Long entryTime;

	/** 离职时间 */
	@Column(name = "LeaveTime")
	private Long leaveTime;

	/** 在职时间 */
	@Column(name = "OnJobTime")
	private String onJobTime;

	/** 是否为内部员工 : 1 华康财富 ， 2 普通用户 */
	private Integer isInside;

	/** 手机类型 : 1 Android 2 IOS */
	private Integer mobileType;

	/**
	 * 银行卡号
	 */
	private String paymentNo;


	/**
	 * 支付方式代码
	 */
	private String paymentMethodId;

	/** 唯一设备号 */
	private String mobileId;

	/** 注册时间　 */
	private Long registCts =  System.currentTimeMillis();

	/** 推荐码 */
	private String recommendCode;

	/** 推荐人ID **/
	private Long recommendId;

	/** KYC 问卷调查分数 */
	private Integer kycNum;

	/** 创建人 **/
	private Long creator;

	/** 是否绑卡
	 *	0: 否， 1：是
	 */
	private Integer tieCard;

	/** 用户级别 **/
	private  Integer level;

	private String flag01;

	private String flag02;

	private int hasVirtualAccount = 0;

	/**是否销户,0:未销户;1:已销户 */
	private Integer closeAccount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Long getBirthday() {
		return birthday;
	}

	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getHealthStatus() {
		return healthStatus;
	}

	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public Integer getOnType() {
		return onType;
	}

	public void setOnType(Integer onType) {
		this.onType = onType;
	}

	public Long getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(Long entryTime) {
		this.entryTime = entryTime;
	}

	public Long getLeaveTime() {
		return leaveTime;
	}

	public void setLeaveTime(Long leaveTime) {
		this.leaveTime = leaveTime;
	}

	public String getOnJobTime() {
		return onJobTime;
	}

	public void setOnJobTime(String onJobTime) {
		this.onJobTime = onJobTime;
	}

	public Integer getIsInside() {
		return isInside;
	}

	public void setIsInside(Integer isInside) {
		this.isInside = isInside;
	}

	public Integer getMobileType() {
		return mobileType;
	}

	public void setMobileType(Integer mobileType) {
		this.mobileType = mobileType;
	}

	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public String getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getMobileId() {
		return mobileId;
	}

	public void setMobileId(String mobileId) {
		this.mobileId = mobileId;
	}

	public Long getRegistCts() {
		return registCts;
	}

	public void setRegistCts(Long registCts) {
		this.registCts = registCts;
	}

	public String getRecommendCode() {
		return recommendCode;
	}

	public void setRecommendCode(String recommendCode) {
		this.recommendCode = recommendCode;
	}

	public Long getRecommendId() {
		return recommendId;
	}

	public void setRecommendId(Long recommendId) {
		this.recommendId = recommendId;
	}

	public Integer getKycNum() {
		return kycNum;
	}

	public void setKycNum(Integer kycNum) {
		this.kycNum = kycNum;
	}

	public Long getCreator() {
		return creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Integer getTieCard() {
		return tieCard;
	}

	public void setTieCard(Integer tieCard) {
		this.tieCard = tieCard;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getFlag01() {
		return flag01;
	}

	public void setFlag01(String flag01) {
		this.flag01 = flag01;
	}

	public String getFlag02() {
		return flag02;
	}

	public void setFlag02(String flag02) {
		this.flag02 = flag02;
	}

	public int getHasVirtualAccount() {
		return hasVirtualAccount;
	}

	public void setHasVirtualAccount(int hasVirtualAccount) {
		this.hasVirtualAccount = hasVirtualAccount;
	}

	public Integer getCloseAccount() {
		return closeAccount;
	}

	public void setCloseAccount(Integer closeAccount) {
		this.closeAccount = closeAccount;
	}
}
