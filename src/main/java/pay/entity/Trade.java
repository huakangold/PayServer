package pay.entity;

import pay.enm.WithdrawStatusEnum;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

/**
 * 交易流水
 * 
 * @author jeff
 *
 */

@Entity
@Table(name = "t_trade_info")
public class Trade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6715840786766485069L;

	@Id
	public Long id;

	/**
	 * 用户id
	 */
	public Long userId;

	/**
	 * 交易类型 /** 充值 1;提现 2;预授权 3;购买 4;回款 5
	 */
	public Integer type;

	/**
	 * 出入账, 入账为'+'， 出账为'-'
	 */
	public String flowType;

	/**
	 * 流水号
	 */
	public String mchnt_txn_ssn;
	/**
	 * 预授权合同号
	 */
	private String contract_no;
	/**
	 * 交易用户
	 */
	private String login_id;

	/**
	 * 响应码
	 */
	private String resp_code;

	/**
	 * 响应消息
	 */
	private String resp_desc;

	/**
	 * 金额
	 */
	public BigDecimal amt;

	/**
	 * 订单id
	 */
	public Long orderId;

	/**
	 * 订单id
	 */
	public Long productId;

	/**
	 * 描述
	 */
	public String tradeDesc;

	/**
	 * 手机端是否显示
	 */
	public Integer showStatus = 1;

	/**
	 * 交易时间
	 */
	public Long cts;

	/**
	 * 提现状态
	 */
	@Enumerated(EnumType.STRING)
	private WithdrawStatusEnum withdrawStatus;


	public Integer getShowStatus() {
		return showStatus;
	}

	public void setShowStatus(Integer showStatus) {
		this.showStatus = showStatus;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public Long getCts() {
		return cts;
	}

	public void setCts(Long cts) {
		this.cts = cts;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFlowType() {
		return flowType;
	}

	public void setFlowType(String flowType) {
		this.flowType = flowType;
	}

	public String getTradeDesc() {
		return tradeDesc;
	}

	public void setTradeDesc(String tradeDesc) {
		this.tradeDesc = tradeDesc;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getResp_code() {
		return resp_code;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public String getResp_desc() {
		return resp_desc;
	}

	public void setResp_desc(String resp_desc) {
		this.resp_desc = resp_desc;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getContract_no() {
		return contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	public WithdrawStatusEnum getWithdrawStatus() {
		return withdrawStatus;
	}

	public void setWithdrawStatus(WithdrawStatusEnum withdrawStatus) {
		this.withdrawStatus = withdrawStatus;
	}

	@Override
	public String toString() {
		return "Trade{" +
				"id=" + id +
				", userId=" + userId +
				", type=" + type +
				", flowType='" + flowType + '\'' +
				", mchnt_txn_ssn='" + mchnt_txn_ssn + '\'' +
				", contract_no='" + contract_no + '\'' +
				", login_id='" + login_id + '\'' +
				", resp_code='" + resp_code + '\'' +
				", resp_desc='" + resp_desc + '\'' +
				", amt=" + amt +
				", orderId=" + orderId +
				", productId=" + productId +
				", tradeDesc='" + tradeDesc + '\'' +
				", showStatus=" + showStatus +
				", cts=" + cts +
				", withdrawStatus=" + withdrawStatus +
				'}';
	}
}
