package pay.entity.req;

import java.util.List;

/**
 * Created by Sheldon Chen on 2016/12/19.
 */
public class CouponWalletBatchAddReq {

    private List<String> userIdList;

    private String cardId;

    private Long cardNum;

    public List<String> getUserIdList() {
        return userIdList;
    }

    public void setUserIdList(List<String> userIdList) {
        this.userIdList = userIdList;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Long getCardNum() {
        return cardNum;
    }

    public void setCardNum(Long cardNum) {
        this.cardNum = cardNum;
    }
}
