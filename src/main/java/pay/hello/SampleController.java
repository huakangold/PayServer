package pay.hello;

import java.net.MalformedURLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pay.base.ResponseBase;
import pay.entity.Bank;

import com.caucho.hessian.client.HessianProxyFactory;

@Controller
@EnableAutoConfiguration
public class SampleController {
 
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Value("${producer.host}")
	private String host;
  
    
	@RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello World!";
    }
	
	@RequestMapping("/sayHello")
	 public void accountService() throws MalformedURLException {
		 String url = "http://" + host + "/helloService";
		 
		 HessianProxyFactory factory=new HessianProxyFactory();
		
		 IHelloService services = (IHelloService)factory.create(IHelloService.class , url);
		 services.sayHello("abc");
	    }
	
	@ResponseBody
	@RequestMapping(value = "/testPost", method = RequestMethod.POST)
	public ResponseBase<Bank> test(Bank b){
		ResponseBase<Bank> rb = new ResponseBase<>();
		Bank bk = new Bank();
		logger.info("params:{}" , b.getBankCode());
		bk.setBankCode(b.getBankCode());
		bk.setBankName(b.getBankName());
		rb.setMsg("success");
		
		rb.setResult(bk);
		
		return rb;
		
		
	}
   /* public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleController.class, args);
    }*/
}
