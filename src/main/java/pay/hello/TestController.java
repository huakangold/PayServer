package pay.hello;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import pay.common.HqlFilter;
import pay.entity.Payment;
import pay.entity.SysUser;
import pay.portal.web.message.PaymentMsg;
import pay.service.IErrorLogJPA;
import pay.service.IFYResultCode;
import pay.service.IPayment;
import pay.service.ISysUserJPA;
import pay.service.ITrade;
import pay.utils.RedisCilent;

@RestController
// responsebody和controller结合。
@RequestMapping("/test")
public class TestController {

	// @Autowired
	// private IUser userService;

	@Autowired
	private IFYResultCode fyResultCode;
	@Autowired
	private ITrade tradeService;
	@Autowired
	private ISysUserJPA sysUserJPA;
	@Autowired
	private IPayment paymentService;
	@Autowired
	private IErrorLogJPA errorLogJPA;
	// @Autowired
	// private IBaseRepository baseRepository;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	private void setScale(BigDecimal fixedAssetsSuccessAmts, BigDecimal allFundsMoney, BigDecimal virtualLeftMoney){
		BigDecimal allAssets=fixedAssetsSuccessAmts.add(allFundsMoney).add(virtualLeftMoney);
		int virtualScale=virtualLeftMoney.multiply(BigDecimal.valueOf(100)).divide(allAssets,0,BigDecimal.ROUND_HALF_UP).intValue();		
		int fixedScale=fixedAssetsSuccessAmts.multiply(BigDecimal.valueOf(100)).divide(allAssets,0,BigDecimal.ROUND_HALF_UP).intValue();		
		int fundsScale=100-virtualScale-fixedScale;
		System.out.println(virtualScale+","+fixedScale+","+fundsScale);
		
	} 
	
	@RequestMapping(method = RequestMethod.GET, value = "/checkcard")
	public void addRedis() {
		
		setScale(BigDecimal.valueOf(500.76), BigDecimal.valueOf(0), BigDecimal.valueOf(100));
		
//		Payment payment=new Payment();
//		payment.setAccountName("胡婷婷2");
//		payment.setUserId(77290832527171L);
//		payment.setPaymentNo("674587435789345793");
//		paymentService.add(payment);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getRedis")
	public void testMap() {
		logger.info(RedisCilent.getString("pay1"));
		logger.info(RedisCilent.getString("pay2"));
		logger.info(RedisCilent.getString("pay3"));
		logger.info(RedisCilent.getString("pay4"));
	}

	// @RequestMapping(method = RequestMethod.GET, value = "/getList")
	// public List<User> getList() {
	//
	// // return userService.getList();
	// }

	@SuppressWarnings({ "unused", "rawtypes" })
	@RequestMapping("testJpa")
	public void testJpa() {
		String url = "http://localhost:8081/orderView/getOrderViewListByHqlfilter?sign=viewSign";
		RestTemplate template = new RestTemplate();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HqlFilter hqlFilter = new HqlFilter();
			hqlFilter.addEQFilter("userId", 77290832527171L);
			hqlFilter.addSort("productId");
			hqlFilter.addOrder("desc");
			// hqlFilter.addSql(" where userId=77290832527171");
			// hqlFilter.addEQFilter("userId", 77290832527171L);
			logger.info(hqlFilter.getWhereHql());
			HttpEntity<Object> requestEntity = new HttpEntity<Object>(
					hqlFilter, headers);
			ResponseEntity<List> resultList = template.exchange(url,
					HttpMethod.POST, requestEntity, List.class, hqlFilter);
			// logger.info("result=", result.getBody());
			//
			// if (StringHelper.isNotEmpty(result.getBody())) {
			// List<OrderView> viewList = JsonUtils.toBean(result.getBody(),
			// List.class, OrderView.class);
			// for (OrderView view : viewList) {
			// logger.info("理财师手机号：" + view.getFinPhone());
			// }
			//
			// }

			System.out.println("OK");
		} catch (Exception e) {
			logger.info("更新订单异常！");
			logger.error("更新订单异常！", e);
		}
	}

	@RequestMapping("getPaymentById")
	public void getPaymentById() {
		// Payment payment = paymentJpa.findOne(76336334854914L);
		// System.out.println(payment);

	}

	@RequestMapping("getUserById")
	public void getUserById() {
		SysUser user = sysUserJPA.findOne(28608272425582L);
		System.out.println(user);

	}

	// @RequestMapping("testPu")
	// public User testUpdate() {
	//
	// User user = usreJpa.findByName("胡婷婷");
	// user.setPwd("testabcdefg11111");
	// User pu = usreJpa.save(user);
	//
	// return pu;
	// }
	//
	// @RequestMapping("testDel")
	// public Boolean testDel() {
	// usreJpa.delete((long) 1);
	// return true;
	// }

	@RequestMapping("testSave")
	public SysUser testSave() {
		logger.info("test findByName{}", "test Save logger info...");
		SysUser user = new SysUser();
		user.setId((long) 1);
		user.setName("13316099280");
		user.setName("单佐");
		user.setPwd("fdfdsa");
		sysUserJPA.save(user);
		return user;
	}
}
