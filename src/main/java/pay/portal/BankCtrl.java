package pay.portal;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.entity.Bank;
import pay.service.IBank;

/**
 * @author lemon 用户实现类
 */
@Controller
@RequestMapping(value = BaseCtrl.App)
public class BankCtrl extends BaseCtrl {

    private Logger logger = LoggerFactory.getLogger(BankCtrl.class);

    @Autowired
    private IBank bankService;



    /**
     * 通过银行名称获取bankCode
     *
     * @param bankName
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getBankCode", method = RequestMethod.GET)
    public ResponseBase<String> getBankCode(@RequestParam String bankName) {
        ResponseBase<String> resp = new ResponseBase<>();
        String bankCode=bankService.getBankCode(bankName);
        resp.setResult(bankCode);
        resp.setResultCode(ResultCode.SUCC.getCode());
        return resp;
    }


    /**
     * 获取银行卡列表
     *
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getBankList", method = RequestMethod.GET)
    public ResponseBase<List<Bank>> getProList() {
        ResponseBase<List<Bank>> resp = new ResponseBase<>();

        List<Bank> respList;

        try {
            respList = bankService.findByShowStatus(1);
            if (respList != null && respList.size() > 0) {
                resp.setSize(respList.size());
                resp.setResult(respList);
                resp.setResultCode(ResultCode.SUCC.getCode());
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            resp.setMsg("操作异常，请重试");
            resp.setResultCode(ResultCode.FAILED.getCode());
        }

        return resp;
    }
}
