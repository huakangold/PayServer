package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pay.service.ChangeCardService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ChangeCardScheduler {
    protected Logger logger = LoggerFactory
            .getLogger(this.getClass().getName());

    @Autowired
    private ChangeCardService changeCardService;


    @Scheduled(cron = "0 0 1 * * ?")
    public void testTasks() {
        logger.info("查询换卡定时任务开始……");
        ExecutorService executor = Executors.newFixedThreadPool(1);
        Runnable task = () -> {
            changeCardService.syncChangeCardResult();
            logger.info("查询换卡定时任务结束……");
        };
        executor.execute(task);
        executor.shutdown();
    }


}
