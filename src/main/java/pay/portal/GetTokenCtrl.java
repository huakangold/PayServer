package pay.portal;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.portal.web.message.RequestMsg;
import pay.utils.CreateUniqueTokensUtil;
import pay.utils.SecurityUtils;

@Controller
@RequestMapping(value = BaseCtrl.App)
public class GetTokenCtrl {
	protected Logger logger = LoggerFactory
			.getLogger(this.getClass().getName());
    @Value("${server.url}")
    private String serverUrl;

    // @RequestMapping(value ="/getTokenRandom", method=RequestMethod.OPTIONS)
    // public void aActionOption(HttpServletResponse response ) throws
    // IOException{
    // response.setHeader("Access-Control-Allow-Headers",
    // "accept, content-type");
    // response.setHeader("Access-Control-Allow-Method", "POST");
    // response.setHeader("Access-Control-Allow-Origin", "http://"+serverUrl);
    // }

    @ResponseBody
    @RequestMapping(value = "/getSign", produces = "application/json;charset=UTF8", method = RequestMethod.GET)
    public ResponseBase<String> getSign(@RequestParam String str) {
        ResponseBase<String> base = new ResponseBase<>();
        SecurityUtils.initPrivateKey();
        SecurityUtils.initPublicKey();
        String signature = SecurityUtils.sign(str);
        base.setMsg("获取signature成功");
        logger.info("前台获取签名入参str={},signature={}",str,signature);
        base.setResult(signature);
        base.setResultCode(ResultCode.SUCC.getCode());
        return base;
    }

    @ResponseBody
    @RequestMapping(value = "/getPostSign", produces = "application/json;charset=UTF8", method = RequestMethod.POST)
    public ResponseBase<String> getPostSign(@RequestBody RequestMsg req) {
        ResponseBase<String> base = new ResponseBase<>();
        SecurityUtils.initPrivateKey();
        SecurityUtils.initPublicKey();
        String signature = SecurityUtils.sign(req.getToken());
        base.setMsg("获取signature成功");
        base.setResult(signature);
        base.setResultCode(ResultCode.SUCC.getCode());
        return base;
    }

    @ResponseBody
    @RequestMapping(value = "/getTokenRandom", produces = "application/json;charset=UTF8", method = RequestMethod.GET)
    public ResponseBase<String> getTokenRandom(@RequestParam String userId) {
        ResponseBase<String> base = new ResponseBase<>();
        String token = CreateUniqueTokensUtil.getRandomToken(userId);
        base.setMsg("获取token成功");
        base.setResult(token);
        base.setResultCode(ResultCode.SUCC.getCode());
        return base;
    }
}
