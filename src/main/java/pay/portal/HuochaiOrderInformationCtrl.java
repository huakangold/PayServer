package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.portal.web.message.HuoChaiOrderInfoReq;
import pay.service.huochai.IHuochaiOrderInformation;

import java.util.List;

/**
 * Created by Sheldon Chen on 2016/11/24.
 */
@Controller
@EnableAutoConfiguration
@RequestMapping(value = BaseCtrl.CommonRootPath)
public class HuochaiOrderInformationCtrl extends BaseCtrl {

    private Logger logger = LoggerFactory.getLogger(HuochaiOrderInformationCtrl.class);

    @Autowired
    private IHuochaiOrderInformation huochaiOrderInformationService;

    @ResponseBody
    @RequestMapping(value = "/huochai/order/list", method = RequestMethod.POST)
    public ResponseBase postHuochaiOrderList(@RequestBody List<HuoChaiOrderInfoReq> req) {
        ResponseBase resp = new ResponseBase();

        if (req == null || req.size() == 0) {
            resp.setResultCode(ResultCode.PARAM_ERROR.getCode());
            resp.setMsg("param error");
        } else {
            String filename = huochaiOrderInformationService.writeOrderInformationToFile(req);
            resp.setResultCode(ResultCode.SUCC.getCode());
            resp.setMsg("filename: " + filename);
        }

        return resp;
    }
}
