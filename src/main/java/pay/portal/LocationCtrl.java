package pay.portal;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.internal.util.StringHelper;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.entity.City;
import pay.entity.Province;
import pay.service.ICity;
import pay.service.IProvince;

/**
 * @author lemon 用户实现类
 */
@Controller
@RequestMapping(value = BaseCtrl.MgrRootPath + "/locationCtrl")
public class LocationCtrl extends BaseCtrl {

	private Logger logger = LoggerFactory.getLogger(LocationCtrl.class);

	@Autowired
	private IProvince proService;

	@Autowired
	private ICity cityService;

	/**
	 * 获取全部的省份列表
	 * 
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getProList", method = RequestMethod.GET)
	public ResponseBase<List<Province>> getProList() {
		ResponseBase<List<Province>> resp = new ResponseBase<>();

		List<Province> respList;

		try {
			respList = proService.findAll();
			resp.setSize(respList.size());
			resp.setResult(respList);
			resp.setResultCode(ResultCode.SUCC.getCode());

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			resp.setMsg("操作异常，请重试");
			resp.setResultCode(ResultCode.FAILED.getCode());
		}

		return resp;
	}

	/**
	 * 获取全部的省份列表
	 * 
	 * @param
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getCityListByProCode", method = RequestMethod.POST)
	public ResponseBase<List<City>> getCityListByProCode(
			@RequestBody String proCode) {
		ResponseBase<List<City>> resp = new ResponseBase<>();
		JSONObject object = new JSONObject(proCode);
		proCode = object.getString("proCode");
		List<City> respList = new ArrayList<>();
		logger.info("proCode = " + proCode);
		try {
			if (StringHelper.isNotEmpty(proCode)) {
				respList = cityService.findByProCode(Integer.parseInt(proCode));
				resp.setSize(respList.size());
				resp.setResult(respList);
				resp.setResultCode(ResultCode.SUCC.getCode());
			} else {
				resp.setSize(respList.size());
				resp.setResult(respList);
				resp.setMsg(ResultCode.PARAM_ERROR.getMsg());
				resp.setResultCode(ResultCode.PARAM_ERROR.getCode());
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			resp.setMsg("操作异常，请重试");
			resp.setResultCode(ResultCode.FAILED.getCode());
		}

		return resp;
	}

}
