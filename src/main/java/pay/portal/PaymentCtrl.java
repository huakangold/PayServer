package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.HqlFilter;
import pay.common.ResultCode;
import pay.entity.Payment;
import pay.entity.SysUser;
import pay.service.IBank;
import pay.service.IPayment;
import pay.service.sys.ISysUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lemon 用户实现类
 */
@Controller
@RequestMapping(value = BaseCtrl.App)
public class PaymentCtrl extends BaseCtrl {

    private Logger logger = LoggerFactory.getLogger(PaymentCtrl.class);

    @Autowired
    private IBank bankService;

    @Autowired
    private ISysUser sysUserService;

    @Autowired
    private IPayment paymentService;


    /**
     * 获取银行卡列表
     *
     * @param req
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPaymentByUser", method = RequestMethod.POST)
    public ResponseBase<Payment> getPaymentByUser(@RequestBody SysUser req) {
        ResponseBase<Payment> resp = new ResponseBase<>();

        if (req == null) {
//            logger.info("获取用户银行卡列表 userId ={}", req.getId());
            resp.setResult(null);
            resp.setResultCode(ResultCode.PARAM_ERROR.getMsg());
            logger.info("请求信息参数错误。");
            return resp;
        }

        SysUser user = sysUserService.findById(req.getId());

        if (user == null) {
            resp.setResult(null);
            resp.setMsg("该用户不存在。userId ={}" + req.getId());
            resp.setResultCode(ResultCode.FAILED.getCode());
            return resp;
        }

        HqlFilter hqlFilter = new HqlFilter();

        hqlFilter.addSql(" where  supportCompany like '%1%' and cardStatus = 1 and userId =" + user.getId());

        List<Payment> payList = paymentService.findByFilter(hqlFilter);
        logger.info("select filter = " + hqlFilter.getWhereAndOrderHql());
        if (payList != null && payList.size() > 0) {
            resp.setResult(payList.get(0));
            resp.setResultCode(ResultCode.SUCC.getCode());
        } else {
            resp.setMsg("银行卡不存在");
            resp.setResultCode(ResultCode.FAILED.getCode());
        }
        return resp;
    }


    /**
     * 获取支付信息
     * @param paymentId
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPaymnetInfo", method = RequestMethod.GET)
    public ResponseBase<Map<String, String>> getPaymnetInfo(@RequestParam  Long paymentId, @RequestParam  Long userId) {
        ResponseBase<Map<String, String>> resp = new ResponseBase<>();

        logger.info("获取支付信息。paymentId = {}, userId={}", paymentId, userId);
        if ( paymentId == null || userId == null ) {
            resp.setResultCode(ResultCode.PARAM_ERROR.getMsg());
            resp.setMsg("参数错误，必传参数为空");
            logger.info("请求信息参数错误。");
            return resp;
        }

        Payment pay = paymentService.findById(paymentId);
        SysUser user = sysUserService.findById(userId);

        if (pay == null || user == null) {
            logger.info("该支付方式或用户不存在。paymentId = {}, userId={}", paymentId, userId);
            resp.setMsg("该支付方式或用户不存在" + paymentId);
            resp.setResultCode(ResultCode.FAILED.getCode());
            return resp;
        }

        Map<String, String> resultMap = new HashMap<>();
        resultMap.put("companyInfo", "华康海宇财富管理有限公司");
        resultMap.put("phone", user.getName());
        resultMap.put("realName", pay.getEncryptAccountName());
        resultMap.put("idNo", pay.getEncryptIdentityNo());
        resp.setResult(resultMap);
        resp.setResultCode(ResultCode.SUCC.getCode());
        return resp;
    }

}
