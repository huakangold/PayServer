package pay.portal;

import org.mockito.internal.configuration.injection.filter.FinalMockCandidateFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.utils.MD5Util;
import pay.utils.PicUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 图片相关的接口
 */
@Controller
@RequestMapping(value = BaseCtrl.App)
public class PicCtrl extends BaseCtrl {

    private Logger logger = LoggerFactory.getLogger(PicCtrl.class);

    /**
     * 获取一个图片唯一的MD5值
     * @param file
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getPicMd5", method = RequestMethod.POST)
    public ResponseBase<Map<String, String>> changeCard(@RequestParam(value="picId") String picId, @RequestParam(value="file")MultipartFile file) {
        ResponseBase<Map<String, String>> responseBase = new ResponseBase<>();
        if (!file.isEmpty()) {

            if( picId.length() < 10){
                responseBase.setResultCode(ResultCode.FAILED.getCode());
                responseBase.setMsg("picId 过短，上传图片失败");
                return responseBase;
            }


            String originalFilename = file.getOriginalFilename();

            logger.info("originalFilename : {}",originalFilename);

            if( !(originalFilename.indexOf("jpg") >=0)){
                responseBase.setResultCode(ResultCode.FAILED.getCode());
                responseBase.setMsg("请上传jpg格式的图片");
                return responseBase;
            }

            if(file.getSize() > 307200000){
                responseBase.setResultCode(ResultCode.FAILED.getCode());
                responseBase.setMsg("图片尺寸大于3m");
                return responseBase;
            }


            try {
                if(!PicUtil.checkFileExist(picId)){
                    PicUtil.createTempFile(file, picId);
                }

                String fileName = PicUtil.getTempFileName(picId);

                String  filemd5str = MD5Util.getFileMD5String(new File(fileName));
                logger.info("filemd5str ={}", filemd5str);

                Map<String, String > resultMap = new HashMap<>();
                resultMap.put("picId", picId);
                resultMap.put("picMd5", filemd5str);
                responseBase.setResult(resultMap);
                responseBase.setResultCode(ResultCode.SUCC.getCode());
                responseBase.setMsg(ResultCode.SUCC.getMsg());
                return responseBase;



            } catch (Exception e) {
                e.printStackTrace();
                logger.error("上传失败," + e.getMessage());

                responseBase.setMsg("上传图片错误！");
                responseBase.setResultCode(ResultCode.FAILED.getCode());
                responseBase.setMsg(ResultCode.FAILED.getMsg());
                return responseBase;
            }
        } else {
            logger.error("上传失败, 图片为空");
            responseBase.setMsg("上传图片失败，图片为空！");
            responseBase.setResultCode(ResultCode.FAILED.getCode());
            responseBase.setMsg(ResultCode.FAILED.getMsg());
            return responseBase;
        }
    }
}
