package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.portal.web.message.PreAuthMsg;
import pay.portal.web.message.ResultMsg;
import pay.service.IOrderHelper;
import pay.service.IPreAuthCancel;

/**
 * 预授权接口,用于冻结客户金账户中的金额
 *
 * @author jeff
 */

@Controller
@EnableAutoConfiguration
@RequestMapping(value = BaseCtrl.App)
public class PreAuthCancelCtrl extends BaseCtrl {

    protected Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private IPreAuthCancel perAuthCancelService;

    @Autowired
    private IOrderHelper orderHelperService;


    @ResponseBody
    @RequestMapping(value = "/preAuthCancelCtrl", produces = "application/json;charset=UTF8", method = RequestMethod.POST)
    public ResponseBase<ResultMsg> preAuthCancelCtrl(@RequestBody PreAuthMsg req) {
        logger.info("进入富友取消预授权接口");

        ResponseBase<ResultMsg> resp = new ResponseBase<ResultMsg>();

        //订单id
        Long orderId = req.getOrderId();

        logger.info("进入富友取消预授权接口 , orderId ={}, 请求信息为={}", orderId, req.toString());
        ResultMsg resultMsg = perAuthCancelService.preAuthCancel(orderId);
        logger.info("取消预授权结果是 , orderId ={}, result ={}", orderId, resultMsg.getResult());

        resp.setMsg(resultMsg.getRespMsg());

        if (resultMsg.getResult()) {
            resp.setResultCode(ResultCode.SUCC.getCode());
            resp.setResult(resultMsg);
            logger.info("PreAuthCtrl 取消预授成功 即将退出 **********");

        } else {
            resp.setResultCode(ResultCode.FAILED.getCode());
            resp.setResult(resultMsg);

        }

        return resp;
    }


}
