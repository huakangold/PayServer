package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ResultCode;
import pay.portal.web.message.PreAuthMsg;
import pay.portal.web.message.PreAuthResultMsg;
import pay.portal.web.message.ResultMsg;
import pay.service.IOrderHelper;
import pay.service.IPreAuth;
import pay.service.IPreAuthCancel;
import pay.service.OrderMonitorService;

/**
 * 预授权接口,用于冻结客户金账户中的金额
 *
 * @author jeff
 */

@Controller
@EnableAutoConfiguration
@RequestMapping(value = BaseCtrl.App)
public class PreAuthCtrl extends BaseCtrl {
	
	protected Logger logger = LoggerFactory
			.getLogger(this.getClass().getName());
	
	@Autowired
	private IPreAuth perAuthService;

	@Autowired
	private IPreAuthCancel perAuthCancelService;

	@Autowired
	private IOrderHelper orderHelperService;

    @Autowired
    private OrderMonitorService orderMonitor;


    @ResponseBody
	@RequestMapping(value = "/preAuthCtrl", produces = "application/json;charset=UTF8", method = RequestMethod.POST)
	public ResponseBase<PreAuthResultMsg> PreAuthCtrl(@RequestBody PreAuthMsg req) {
		logger.info("进入富友预授权接口");

		ResponseBase<PreAuthResultMsg> resp = new ResponseBase<PreAuthResultMsg>();

		try {
			// 订单id
			Long orderId = req.getOrderId();

			logger.info("PreAuthCtrl 进入预授权划拨接口 , orderId ={}, 请求信息为={}", orderId, req.toString());

			//监控订单状态
			orderMonitor.preAuthStarted(orderId);

			PreAuthResultMsg resultMsg = perAuthService.PreAuth(req);

			logger.info("PreAuthCtrl 预授权结果是 , orderId ={}, result ={}", orderId, resultMsg.getResult());

			resp.setMsg(resultMsg.getRespMsg());

			//预授权失败，直接退出
			if (resultMsg.getResult() == false) {
				resp.setResultCode(ResultCode.FAILED.getCode());
				resp.setMsg("预授权失败");
				resp.setResult(resultMsg);
				//logger.info("PreAuthCtrl 预授失败 即将退出 ********** cancelResult={}", cancelResult);
				return resp;
			}

			//预授权成功，更新相关记录
			logger.info("PreAuthCtrl 预授成功 即将更新相关记录");

			Boolean updRecordResult = perAuthService.updPreautResult(resultMsg.getMchnt_txn_ssn(), resultMsg.getContract_no(), true);

			logger.info("PreAuthCtrl 预授成功 保存订单的结果是={}", updRecordResult);

			//预授成功、保存订单、添加产品路由、更改产品募集额度等操作全部成功
			if (updRecordResult) {
				//监控订单状态
				orderMonitor.preAuthFinish(orderId);

				resp.setResultCode(ResultCode.SUCC.getCode());
				resp.setMsg("预授权成功");
				resp.setResult(resultMsg);
				logger.info("PreAuthCtrl 预授成功、保存订单、添加产品路由、更改产品募集额度等操作全部成功 即将退出 **********");

				//监控订单状态
				orderMonitor.success(orderId);
				return resp;
			}

			//预授权成功，保存订单等操作失败，则进行取消预授权
			ResultMsg cancelAuthResult = perAuthCancelService.preAuthCancel(resultMsg.getOrderId());

			logger.info("PreAuthCtrl 取消预授权 ResultMsg ={}", cancelAuthResult);

			//如果取消预授权失败，则再次调用
			if (cancelAuthResult.getResult() == false) {
				logger.error("PreAuthCtrl 取消预授权失败 即将再次尝试取消预授权 **********");
				cancelAuthResult = perAuthCancelService.preAuthCancel(resultMsg.getOrderId());
			}

			if (cancelAuthResult.getResult() == false) {
				logger.error("PreAuthCtrl 取消预授权失败 即将退出 **********");
			}

			//监控订单状态
			orderMonitor.preAuthFailed(orderId,cancelAuthResult.getRespMsg());

			resp.setResultCode(ResultCode.FAILED.getCode());
			resp.setResult(null);
			resp.setMsg("预授权失败");
			logger.info("PreAuthCtrl 预授权失败 即将退出 ");
		}catch(Exception e){
			logger.error("PreAuthCtrl 预授权异常 ", e);
			resp.setResultCode(ResultCode.FAILED.getCode());
			resp.setResult(null);
			resp.setMsg("预授权失败");
			logger.info("PreAuthCtrl 预授权失败 即将退出 ");
		}
		return resp;
	}

	 

}
