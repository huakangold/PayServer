package pay.portal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.common.ConstantFYJZH;
import pay.common.ResultCode;
import pay.context.FYContext;
import pay.entity.SysUser;
import pay.portal.web.message.CZTXMsg;
import pay.portal.web.message.CZTXRecord;
import pay.portal.web.message.CZTXRes;
import pay.service.IErrorLogJPA;
import pay.service.IFYResultCode;
import pay.service.sys.ISysUser;
import pay.utils.DateUtil;
import pay.utils.StringHelper;

/**
 * 充值提现查询接口
 * 
 * @author jeff
 */

@Controller
@EnableAutoConfiguration
@RequestMapping(value = BaseCtrl.App)
public class QuerycztxCtrl extends BaseCtrl {

	/**
	 * 商户代码，从配置文件中读取
	 */

	@Value("${fy.mchntCd}")
	private String mchnt_cd;

	@Value("${server.scheme}")
	private String serverScheme;

	@Value("${fy.goldAccount.pathBaseUrl}")
	private String pathBaseUrl; // 测试环境为/jzh 生产无。
	// /**
	// * 商户密钥，从配置文件中读取
	// */
	// @Value("${fy.Secret}")
	// private String secret;
	// @Value("${pc.secret}")
	// private String pcSecret;
	@Value("${server.url}")
	private String serverUrl;

	/**
	 * 金账户baseURl，从配置文件中读取
	 */
	@Value("${fy.goldAccount.baseUrl}")
	private String jzhBaseUrl;

	@Autowired
	private IFYResultCode fyResultCodeService;

	@Autowired
	private ISysUser sysUserService;

	@Autowired
	private IErrorLogJPA errorLogJPA;

	@Autowired
	private FYContext fyContext;

	@ResponseBody
	@RequestMapping(value = "/querycztxCtrl", produces = "application/json;charset=UTF8", method = RequestMethod.POST)
	public ResponseBase<List<CZTXRecord>> querycztxCtrl(@RequestBody CZTXMsg req) {
		logger.info("进入8082 富友预授权接口");
		ResponseBase<List<CZTXRecord>> resp = new ResponseBase<List<CZTXRecord>>();
		// 用户
		String cust_no = req.getCust_no();
		logger.info("进入富友查询充值提现接口关键入参  cust_no={} ", cust_no);

		if (StringHelper.isEmpty(cust_no)) {
			logger.info("入参有空参数");
			resp.setResultCode(ResultCode.PARAM_LACK.getCode());
			return resp;
		}

		SysUser user = sysUserService.getByPhoneNum(req.getCust_no());

		if (user == null) {
			logger.info("用户不存在, cust_no={}", req.getCust_no());
			resp.setResultCode(ResultCode.PARAM_LACK.getCode());
			return resp;
		}

		// 调用富友接口，获取充值记录
		List<CZTXRecord> czRecordList = getTranRecords(
				ConstantFYJZH.BUSI_TY_CZ, cust_no);

		// 调用富友接口，获取提现
		List<CZTXRecord> txRecordList = getTranRecords(
				ConstantFYJZH.BUSI_TY_TX, cust_no);

		List<CZTXRecord> allRecordsList = getAllList(czRecordList, txRecordList);

		if (allRecordsList != null) {
			resp.setSize(allRecordsList.size());
			resp.setResult(allRecordsList);
			resp.setResultCode(ResultCode.SUCC.getCode());
		} else {
			resp.setResult(null);
			resp.setResultCode(ResultCode.FAILED.getCode());
		}

		return resp;
	}

	private List<CZTXRecord> getTranRecords(String busi_tp, String cust_no) {

		// 起始时间
		String start_time = "";

		// 截止时间
		String end_time = "";

		Calendar nowTime = Calendar.getInstance();

		Calendar startTime01 = Calendar.getInstance();

		startTime01.set(Calendar.DATE, startTime01.get(Calendar.DATE) - 30);

		start_time = DateUtil.getStartTimeOfDay(startTime01);

		end_time = DateUtil.getEndTimeOfDay(nowTime);

		// 查询近期的交易记录
		List<CZTXRecord> recordsListLatest = fyContext.getResult(busi_tp, cust_no,
				start_time, end_time);

		Calendar calendarEnd = Calendar.getInstance();

		calendarEnd.set(Calendar.DATE, calendarEnd.get(Calendar.DATE) - 31);

		start_time = DateUtil.START_TIME;

		end_time = DateUtil.getEndTimeOfDay(calendarEnd);

		// 获得以前的交易记录
		List<CZTXRecord> recordsListOld = fyContext.getResult(busi_tp, cust_no,
				start_time, end_time);

		// 合并交易记录
		List<CZTXRecord> allRecordsList = getAllList(recordsListLatest,
				recordsListOld);

		return allRecordsList;
	}



	private List<CZTXRecord> getAllList(List<CZTXRecord> czRecordList,
			List<CZTXRecord> txRecordList) {
		CZTXRes result = new CZTXRes();

		List<CZTXRecord> allList = new ArrayList<CZTXRecord>();
		allList.addAll(czRecordList);

		allList.addAll(txRecordList);

		Collections.sort(allList, new SortByCts());

		return allList;
	}

	class SortByCts implements Comparator {
		public int compare(Object o1, Object o2) {
			CZTXRecord r1 = (CZTXRecord) o1;
			CZTXRecord r2 = (CZTXRecord) o2;
			return -1 * r1.getCts().compareTo(r2.getCts());
		}
	}
}
