package pay.portal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pay.base.BaseCtrl;
import pay.base.ResponseBase;
import pay.service.ChangeCardService;

/**
 * Created by Antinomy on 17/8/9.
 */

@Controller
@RequestMapping(value = BaseCtrl.App+"/sync")
public class SyncController extends BaseCtrl {

    protected Logger logger = LoggerFactory
            .getLogger(this.getClass().getName());

    @Autowired
    private ChangeCardService changeCardService;

    @ResponseBody
    @RequestMapping(value = "/changeCard", method = RequestMethod.GET)
    public ResponseBase<String>  syncChangeCard() {
        ResponseBase<String> result = new ResponseBase<>();
        changeCardService.syncChangeCardResult();
        result.setMsg("finish");
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/disBindCard", method = RequestMethod.POST)
    public ResponseBase<String>  disBindCard(String mobile,String cardNum) {
        ResponseBase<String> result = new ResponseBase<>();
        result.setMsg(changeCardService.disBindCard(mobile,cardNum));
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/syncBindCard", method = RequestMethod.POST)
    public ResponseBase<String> syncBindCard(String mobile, String cardNum) {
        ResponseBase<String> result = new ResponseBase<>();
        result.setMsg(changeCardService.syncBindCard(mobile, cardNum));
        return result;
    }

    /**
     * 一次性同步完所有换卡数据
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/syncBindCardAll", method = RequestMethod.GET)
    public ResponseBase<String> syncBindCardAll(){
        ResponseBase<String> result = new ResponseBase<>();
        changeCardService.syncChangeCardResult();
        result.setMsg("同步成功");
        return  result;
    }
}
