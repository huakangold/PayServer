package pay.portal.web.message;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BalanceMsg {
	/**
	 * 账面总余额
	 */
	String ct_balance;

	/**
	 * 可用金额
	 */
	String ca_balance;

	/**
	 * 冻结金额
	 */
	String cf_balance;

	/**
	 * 未转结余额
	 */
	String cu_balance;

	/**
	 * 冻结笔数
	 */
	Long cf_account;

	public String getCt_balance() {
		return ct_balance;
	}

	public void setCt_balance(String ct_balance) {
		this.ct_balance = ct_balance;
	}

	public String getCa_balance() {
		return ca_balance;
	}

	public void setCa_balance(String ca_balance) {
		this.ca_balance = ca_balance;
	}

	public String getCf_balance() {
		return cf_balance;
	}

	public void setCf_balance(String cf_balance) {
		this.cf_balance = cf_balance;
	}

	public String getCu_balance() {
		return cu_balance;
	}

	public void setCu_balance(String cu_balance) {
		this.cu_balance = cu_balance;
	}

	public Long getCf_account() {
		return cf_account;
	}

	public void setCf_account(Long cf_account) {
		this.cf_account = cf_account;
	}

}
