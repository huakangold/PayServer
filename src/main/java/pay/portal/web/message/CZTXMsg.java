package pay.portal.web.message;


/**
 * 充值提现记录
 * @author jeff
 *
 */
 
public class CZTXMsg {
	/**
	 * 用户手机号 
	 */
	public String cust_no;
	
	
	/**
	 * 查询的年份
	 */
	public Integer year;
	
	
	/**
	 * 查询的月份
	 */
	public Integer month;
	
	/**
	 * 交易类型
	 */
	public Integer  type;
	
	/**
	 * 请求类型
	 */
	
	public Integer reqType;
	
	
	public Integer getReqType() {
		return reqType;
	}

	public void setReqType(Integer reqType) {
		this.reqType = reqType;
	}

	public Long userId;

	public String getCust_no() {
		return cust_no;
	}

	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	 
	 
	 
	
}
