package pay.portal.web.message;

public class CZTXRecord {

	/* 类型 */
	private String typeCode = "";

	private String typeStr = "";
	
	private String typeFlag = "";

	/* 充值提现日期 */
	private String ctsStr = "";

	/* 充值提现流水 */
	private String mchnt_ssn;

	/* 充值提现金额 */
	private String amt;

	private Long cts;

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

	public String getCtsStr() {
		return ctsStr;
	}

	public void setCtsStr(String ctsStr) {
		this.ctsStr = ctsStr;
	}

	public String getMchnt_ssn() {
		return mchnt_ssn;
	}

	public void setMchnt_ssn(String mchnt_ssn) {
		this.mchnt_ssn = mchnt_ssn;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public Long getCts() {
		return cts;
	}

	public void setCts(Long cts) {
		this.cts = cts;
	}

	public String getTypeFlag() {
		return typeFlag;
	}

	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}

	@Override
	public String toString() {
		return "CZTXRecord{" +
				"typeCode='" + typeCode + '\'' +
				", typeStr='" + typeStr + '\'' +
				", typeFlag='" + typeFlag + '\'' +
				", ctsStr='" + ctsStr + '\'' +
				", mchnt_ssn='" + mchnt_ssn + '\'' +
				", amt='" + amt + '\'' +
				", cts=" + cts +
				'}';
	}
}
