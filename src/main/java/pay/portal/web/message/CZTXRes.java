package pay.portal.web.message;

import java.util.ArrayList;
import java.util.List;


public class CZTXRes {
	
	public String resp_code  ="";
    
	public String busi_tp  = "";
    
    public String total_number  = "";
    
    public List<CZTXRecord>   recordList = new ArrayList<CZTXRecord>();

	public String getResp_code() {
		return resp_code;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public String getBusi_tp() {
		return busi_tp;
	}

	public void setBusi_tp(String busi_tp) {
		this.busi_tp = busi_tp;
	}

	public String getTotal_number() {
		return total_number;
	}

	public void setTotal_number(String total_number) {
		this.total_number = total_number;
	}

	public List<CZTXRecord> getRecordList() {
		return recordList;
	}

	public void setRecordList(List<CZTXRecord> recordList) {
		this.recordList = recordList;
	}

}
