package pay.portal.web.message;

import java.io.Serializable;

/**
 * 交易流水
 * 
 * @author jeff
 *
 */

public class ChangeCardReq implements Serializable {
	/**
	 * 用户id
	 */
	public String userId;

	/**
	 * 交易类型
	 */
	public String cityId;

	public String bankCd;
	
	public String cardNo;
	
	public String picId02;
	
	public String picId01;

	public Long oldPaymentId;

	public String mchnt_txn_ssn;

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getBankCd() {
		return bankCd;
	}

	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getPicId01() {
		return picId01;
	}

	public void setPicId01(String picId01) {
		this.picId01 = picId01;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPicId02() {
		return picId02;
	}

	public void setPicId02(String picId02) {
		this.picId02 = picId02;
	}

	public Long getOldPaymentId() {
		return oldPaymentId;
	}

	public void setOldPaymentId(Long oldPaymentId) {
		this.oldPaymentId = oldPaymentId;
	}
}
