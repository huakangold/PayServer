package pay.portal.web.message;

import java.io.Serializable;

public class CommonReq implements Serializable {

	private static final long serialVersionUID = -7119937891085550376L;

	/**
	 * 回调url
	 */
	String back_notify_url = "";

	/**
	 * 商户代码
	 */
	String mchnt_cd = "";

	/**
	 * 用户id
	 */
	String login_id = "";

	/**
	 * 充值金额
	 */
	String amt = "";

	/**
	 * 签名
	 */
	String signature = "";

	/**
	 * 流水号
	 */
	String mchnt_txn_ssn = "";

	/**
	 * 备注
	 */
	String rem = "";

	/**
	 * 身份证号码/证件
	 */
	String bank_nm = "";

	/**
	 * 银行卡账号
	 */
	String capAcntNo = "";

	/**
	 * 姓名
	 */
	String capAcntNm = "";

	/**
	 * 身份证号码/证件
	 */
	String certif_id = "";

	/**
	 * 开户行地区代码
	 */
	String city_id = "";

	/**
	 * 客户姓名
	 */
	String cust_nm = "";

	/**
	 * 邮箱地址
	 */
	String email = "";

	/**
	 * 手机号码
	 */
	String mobile_no = "";

	/**
	 * 开户行行别
	 */
	String parent_bank_id = "";

	/**
	 * 登录密码，不填默认为手机号后6位
	 */
	String lpassword = "";

	/**
	 * 提现密码:提现密码，不填默认为手机号后6位
	 */
	String password = "";

	/**
	 * 证件类型:0 身份证， 7其它
	 */
	String certif_tp = "";

	/**
	 * 商户返回地址
	 */
	String page_notify_url = "";

	/**
	 * 用户在商户系统的标志
	 */
	String user_id_from = "";

	/**
	 * 版本号
	 */
	String ver = "";

	String userId = "";

	/**
	 * 委托交易日
	 */
	String mchnt_txn_dt = "";

	/**
	 * 返回结果码
	 * 
	 * @return
	 */
	String resp_code = "";

	String token = "";

	String sign = "";

	public String getResp_code() {
		return resp_code;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public String getBack_notify_url() {
		return back_notify_url;
	}

	public void setBack_notify_url(String back_notify_url) {
		this.back_notify_url = back_notify_url;
	}

	public String getMchnt_cd() {
		return mchnt_cd;
	}

	public void setMchnt_cd(String mchnt_cd) {
		this.mchnt_cd = mchnt_cd;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getBank_nm() {
		return bank_nm;
	}

	public void setBank_nm(String bank_nm) {
		this.bank_nm = bank_nm;
	}

	public String getCapAcntNo() {
		return capAcntNo;
	}

	public void setCapAcntNo(String capAcntNo) {
		this.capAcntNo = capAcntNo;
	}

	public String getCapAcntNm() {
		return capAcntNm;
	}

	public void setCapAcntNm(String capAcntNm) {
		this.capAcntNm = capAcntNm;
	}

	public String getCertif_id() {
		return certif_id;
	}

	public void setCertif_id(String certif_id) {
		this.certif_id = certif_id;
	}

	public String getCity_id() {
		return city_id;
	}

	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}

	public String getCust_nm() {
		return cust_nm;
	}

	public void setCust_nm(String cust_nm) {
		this.cust_nm = cust_nm;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getParent_bank_id() {
		return parent_bank_id;
	}

	public void setParent_bank_id(String parent_bank_id) {
		this.parent_bank_id = parent_bank_id;
	}

	public String getLpassword() {
		return lpassword;
	}

	public void setLpassword(String lpassword) {
		this.lpassword = lpassword;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCertif_tp() {
		return certif_tp;
	}

	public void setCertif_tp(String certif_tp) {
		this.certif_tp = certif_tp;
	}

	public String getPage_notify_url() {
		return page_notify_url;
	}

	public void setPage_notify_url(String page_notify_url) {
		this.page_notify_url = page_notify_url;
	}

	public String getUser_id_from() {
		return user_id_from;
	}

	public void setUser_id_from(String user_id_from) {
		this.user_id_from = user_id_from;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMchnt_txn_dt() {
		return mchnt_txn_dt;
	}

	public void setMchnt_txn_dt(String mchnt_txn_dt) {
		this.mchnt_txn_dt = mchnt_txn_dt;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
