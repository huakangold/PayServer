package pay.portal.web.message;

import java.io.Serializable;

public class CommonRes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8600405613297289539L;
	
	private String cust_Name;//客户姓名
	
	private String certif_No;//身份证
	
	private String bank_No;//银行卡号
	
	private String bank_Code;//银行代码

	public String getCust_Name() {
		return cust_Name;
	}

	public void setCust_Name(String cust_Name) {
		this.cust_Name = cust_Name;
	}

	public String getCertif_No() {
		return certif_No;
	}

	public void setCertif_No(String certif_No) {
		this.certif_No = certif_No;
	}

	public String getBank_No() {
		return bank_No;
	}

	public void setBank_No(String bank_No) {
		this.bank_No = bank_No;
	}

	public String getBank_Code() {
		return bank_Code;
	}

	public void setBank_Code(String bank_Code) {
		this.bank_Code = bank_Code;
	}
	
	
}
