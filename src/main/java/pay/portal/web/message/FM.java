package pay.portal.web.message;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FM {

	/**
	 * 响应代码 0000表示成功
	 */
	private String rcd;
	/**
	 * 中文描述
	 */
	private String rdesc;
	/**
	 * 卡类型 01-借记卡，02-信用卡，03-准贷记卡，04-富友卡，05-非法卡号
	 */
	private String ctp;

	/**
	 * 银行名称
	 */
	private String cnm;
	/**
	 * 银行机构号
	 */
	private String inscd;
	/**
	 * MD5摘要数据
	 */
	private String sign;

	public String getRcd() {
		return rcd;
	}

	public void setRcd(String rcd) {
		this.rcd = rcd;
	}

	public String getRdesc() {
		return rdesc;
	}

	public void setRdesc(String rdesc) {
		this.rdesc = rdesc;
	}

	public String getCtp() {
		return ctp;
	}

	public void setCtp(String ctp) {
		this.ctp = ctp;
	}

	public String getCnm() {
		return cnm;
	}

	public void setCnm(String cnm) {
		this.cnm = cnm;
	}

	public String getInscd() {
		return inscd;
	}

	public void setInscd(String inscd) {
		this.inscd = inscd;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
