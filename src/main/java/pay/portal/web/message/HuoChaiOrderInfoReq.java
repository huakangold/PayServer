package pay.portal.web.message;

/**
 * Created by Sheldon Chen on 2016/11/24.
 */
public class HuoChaiOrderInfoReq {

    private String amount;
    private String userName;
    private String identity;
    private String paymentNo;

    private String payStatus;
    private String phoneNum;
    private Long orderTradeDate;
    private Long dividendDate;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Long getOrderTradeDate() {
        return orderTradeDate;
    }

    public void setOrderTradeDate(Long orderTradeDate) {
        this.orderTradeDate = orderTradeDate;
    }

    public Long getDividendDate() {
        return dividendDate;
    }

    public void setDividendDate(Long dividendDate) {
        this.dividendDate = dividendDate;
    }
}
