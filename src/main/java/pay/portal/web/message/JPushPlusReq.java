package pay.portal.web.message;

/**
 * Created by Sheldon Chen on 2017/2/13.
 */
public class JPushPlusReq {
    private String aliases;
    private String msg;
    private String menu;
    private String title;
    private String uuid;
    private String env;
    private String msgTypeConstants;
    private Integer channel;

    public Integer getChannel() {
        return channel;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getAliases() {
        return aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getMsgTypeConstants() {
        return msgTypeConstants;
    }

    public void setMsgTypeConstants(String msgTypeConstants) {
        this.msgTypeConstants = msgTypeConstants;
    }
}
