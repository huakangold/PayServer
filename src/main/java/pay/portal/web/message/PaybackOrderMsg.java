package pay.portal.web.message;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 订单回款划拨
 * @author jeff
 *
 */
public class PaybackOrderMsg implements Serializable {	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 2319102037927027694L;

	private Long orderId;
	
	private String outAccount;
	
	private String inAccount;
	
	private BigDecimal amt;
	
	private int status;
	
	private Timestamp cts;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	 

	public String getOutAccount() {
		return outAccount;
	}

	public void setOutAccount(String outAccount) {
		this.outAccount = outAccount;
	}

	public String getInAccount() {
		return inAccount;
	}

	public void setInAccount(String inAccount) {
		this.inAccount = inAccount;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Timestamp getCts() {
		return cts;
	}

	public void setCts(Timestamp cts) {
		this.cts = cts;
	}

	@Override
	public String toString() {
		return "TransferOrderMsg [orderId=" + orderId + ", outAccount=" + outAccount + ", inAccount="
				+ inAccount + ", amt=" + amt + ", status=" + status + ", cts="
				+ cts + "]";
	}
	
	

}
