package pay.portal.web.message;

import java.io.Serializable;

import javax.persistence.Id;

public class PaymentMsg implements Serializable {
	private static final long serialVersionUID = 5554548190723045374L;

	@Id
	private Long id;

	/**
	 * 用户id
	 */
	private Long userId;

	/**
	 * 账户姓名
	 */
	private String accountName;

	/**
	 * 身份证号，字母大写
	 */
	private String identityNo;

	/**
	 * 银行码
	 */
	private Integer bankId;

	/**
	 * 填写该银行卡号
	 */
	private String paymentNo;

	/**
	 * 办理银行卡时的开户电话号码
	 */
	private String phone;

	private Long cts;

	private Long lastUseTime;

	/**
	 * 支付方式, 盈米专用
	 */
	private String paymentMethodId;

	/**
	 * 支持的合作方
	 */
	private String supportCompany;

	/**
	 * 0:已解绑 1：绑卡 10：换卡中
	 */
	private Integer cardStatus;

	/**
	 * 银行名称 银行卡所在的银行名称
	 * 
	 */
	private String bankName;

	/**
	 * 银行机构号 根据机构号前6位判断为同一家银行 '0801020000' - 中国工商银行 '0801030000' - 中国农业银行
	 * '0801040000' –中国银行 '0801050000' - 中国建设银行
	 */
	private String insCard;

	/**
	 * 卡类型 01-借记卡，02-信用卡，03-准贷记卡，04-富友卡，05-非法卡号
	 */
	private String cardType;
	/**
	 * 流水号
	 */
	private String mchnt_txn_ssn;

	/**
	 * 其他信息
	 */
	private String extraInfo;
	/**
	 * 用户注册手机号码
	 */
	private String userRegistPhone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getIdentityNo() {
		return identityNo;
	}

	public void setIdentityNo(String identityNo) {
		this.identityNo = identityNo;
	}

	public Integer getBankId() {
		return bankId;
	}

	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}

	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getCts() {
		return cts;
	}

	public void setCts(Long cts) {
		this.cts = cts;
	}

	public Long getLastUseTime() {
		return lastUseTime;
	}

	public void setLastUseTime(Long lastUseTime) {
		this.lastUseTime = lastUseTime;
	}

	public String getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(String paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getSupportCompany() {
		return supportCompany;
	}

	public void setSupportCompany(String supportCompany) {
		this.supportCompany = supportCompany;
	}

	public Integer getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(Integer cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInsCard() {
		return insCard;
	}

	public void setInsCard(String insCard) {
		this.insCard = insCard;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}

	public String getUserRegistPhone() {
		return userRegistPhone;
	}

	public void setUserRegistPhone(String userRegistPhone) {
		this.userRegistPhone = userRegistPhone;
	}

}
