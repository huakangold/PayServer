package pay.portal.web.message;

import java.math.BigDecimal;

public class PreAuthMsg {
	
	
	/**
	 * 用户ID
	 */
	Long userId;
	
	/**
	 * 流水号
	 */
	String mchnt_txn_ssn;
	
	/**
	 * 出账账户
	 */
	String out_cust_no;
	
	/**
	 * 入账账户
	 */
	String in_cust_no;
	
	/**
	 * 预授权金额
	 */
	BigDecimal amtBigDecimal;
	
	/**
	 * 预授权合同号
	 */
	String contract_no;
	
	/**
	 * 产品id
	 * 
	 */
	public Long productId;
	
	
	/**
	 * 
	 * 订单id
	 */
	public Long orderId;
	
	/**
	 * resultCode
	 * @return
	 */
	public String resultCode;
	
	/**
	 *
	 */
	public String token;
	
	/**
	 * 签名
	 */
	public String sign;
	
	/**
	 * key
	 */
	public String apiKey;
	
	

	 

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getOut_cust_no() {
		return out_cust_no;
	}

	public void setOut_cust_no(String out_cust_no) {
		this.out_cust_no = out_cust_no;
	}

	public String getIn_cust_no() {
		return in_cust_no;
	}

	public void setIn_cust_no(String in_cust_no) {
		this.in_cust_no = in_cust_no;
	}

	public BigDecimal getAmtBigDecimal() {
		return amtBigDecimal;
	}

	public void setAmtBigDecimal(BigDecimal amtBigDecimal) {
		this.amtBigDecimal = amtBigDecimal;
	}

	public String getContract_no() {
		return contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	@Override
	public String toString() {
		return "PreAuthMsg [userId=" + userId + ", mchnt_txn_ssn="
				+ mchnt_txn_ssn + ", out_cust_no=" + out_cust_no
				+ ", in_cust_no=" + in_cust_no + ", amtBigDecimal="
				+ amtBigDecimal + ", contract_no=" + contract_no
				+ ", productId=" + productId + ", orderId=" + orderId
				+ ", resultCode=" + resultCode + ", token=" + token + ", sign="
				+ sign + ", apiKey=" + apiKey + "]";
	}
	
	
}
