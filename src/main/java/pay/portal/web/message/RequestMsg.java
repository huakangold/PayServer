package pay.portal.web.message;

import java.io.Serializable;

public class RequestMsg implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8600405613297289539L;
	private String token;
	private Integer curPage;
	private Integer pageSize;
	
	public Integer getCurPage() {
		return curPage;
	}
	public void setCurPage(Integer curPage) {
		this.curPage = curPage;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
