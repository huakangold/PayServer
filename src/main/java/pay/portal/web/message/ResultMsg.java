package pay.portal.web.message;

import java.math.BigDecimal;

public class ResultMsg {
	
	/**
	 * 流水号
	 */
	String mchnt_txn_ssn;
	
	/**
	 * 预授权金额
	 */
	BigDecimal amtBigDecimal;
	
	/**
	 * 预授权合同号
	 */
	String contract_no;
	
	Boolean result;
	
	
	String respMsg;
	

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}


	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}


	public BigDecimal getAmtBigDecimal() {
		return amtBigDecimal;
	}


	public void setAmtBigDecimal(BigDecimal amtBigDecimal) {
		this.amtBigDecimal = amtBigDecimal;
	}


	public String getContract_no() {
		return contract_no;
	}


	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}


	public String getRespMsg() {
		return respMsg;
	}


	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}


	public Boolean getResult() {
		return result;
	}


	public void setResult(Boolean result) {
		this.result = result;
	}


	@Override
	public String toString() {
		return "ResultMsg{" +
				"mchnt_txn_ssn='" + mchnt_txn_ssn + '\'' +
				", amtBigDecimal=" + amtBigDecimal +
				", contract_no='" + contract_no + '\'' +
				", result=" + result +
				", respMsg='" + respMsg + '\'' +
				'}';
	}
}
