package pay.portal.web.message;

public class SessionConstants {

	// 登陆超时间
	public static final int LOGIN_SESSION_TIMEOUT = 8 * 60 * 60;// 60*60 6个小时
	public static final String Token = "Token";
	
	public static final String MenuMgr="sysMgr:sysUser,sysRole,sysRole,sysResource;"
	+"employeeMgr:department,duties,employee,attendance;"
	+"productMgr:building,area,banBasic,houseType,banProduct,payWay,payParameter;"
	+"customerMgr:customer;";
}
