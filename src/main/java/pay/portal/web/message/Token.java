package pay.portal.web.message;

import java.sql.Timestamp;
import java.util.List;

public class Token {

	private Long id;  //用户id
	private String name; // 用户名
	private String realName;  //用户真实姓名
	private String pwd; // 密码
	private Timestamp birthday;
	private String identity;
	private Integer age;
	private String healthStatus;
	private Long deptId;
	private Long dutiesId;
	private Integer onType;
	private Timestamp entryTime;
	private Timestamp leaveTime;
	private String onJobTime;
	
	private String roleIds;
	private String roleNames;
	private String accountId;
	
	private Integer orgId;
	
	private String depName;
	
	private List<String> sysUri;
	private List<String> typeAndFlag;
	private List<String> funcBtns;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public Timestamp getBirthday() {
		return birthday;
	}
	public void setBirthday(Timestamp birthday) {
		this.birthday = birthday;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getHealthStatus() {
		return healthStatus;
	}
	public void setHealthStatus(String healthStatus) {
		this.healthStatus = healthStatus;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getDutiesId() {
		return dutiesId;
	}
	public void setDutiesId(Long dutiesId) {
		this.dutiesId = dutiesId;
	}
	public Integer getOnType() {
		return onType;
	}
	public void setOnType(Integer onType) {
		this.onType = onType;
	}
	public Timestamp getEntryTime() {
		return entryTime;
	}
	public void setEntryTime(Timestamp entryTime) {
		this.entryTime = entryTime;
	}
	public Timestamp getLeaveTime() {
		return leaveTime;
	}
	public void setLeaveTime(Timestamp leaveTime) {
		this.leaveTime = leaveTime;
	}
	public Integer getOrgId() {
		return orgId;
	}
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	public String getDepName() {
		return depName;
	}
	public void setDepName(String depName) {
		this.depName = depName;
	}
	public String getOnJobTime() {
		return onJobTime;
	}
	public void setOnJobTime(String onJobTime) {
		this.onJobTime = onJobTime;
	}
	public String getRoleIds() {
		return roleIds;
	}
	public void setRoleIds(String roleIds) {
		this.roleIds = roleIds;
	}
	public String getRoleNames() {
		return roleNames;
	}
	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}
	public List<String> getSysUri() {
		return sysUri;
	}
	public void setSysUri(List<String> sysUri) {
		this.sysUri = sysUri;
	}
	public List<String> getTypeAndFlag() {
		return typeAndFlag;
	}
	public void setTypeAndFlag(List<String> typeAndFlag) {
		this.typeAndFlag = typeAndFlag;
	}
	public List<String> getFuncBtns() {
		return funcBtns;
	}
	public void setFuncBtns(List<String> funcBtns) {
		this.funcBtns = funcBtns;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
}
