package pay.portal.web.message;

import java.io.Serializable;

/**
 * 交易流水
 * 
 * @author jeff
 *
 */

public class TradeFYReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2546883177782165081L;

	/**
	 * 商户代码
	 */
	public String mchnt_cd;
	public String mobile_no;
	public String remark;
	/**
	 * 流水号
	 */
	public String mchnt_txn_ssn;

	/**
	 * 交易日期
	 */
	public String mchnt_txn_dt;

	/**
	 * 充值金额
	 */
	public String amt;

	/**
	 * 签名数据
	 * 
	 * @return
	 */
	public String signature;
	public String resp_code;
	public String suc_amt;

	public String getMchnt_cd() {
		return mchnt_cd;
	}

	public void setMchnt_cd(String mchnt_cd) {
		this.mchnt_cd = mchnt_cd;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getMchnt_txn_dt() {
		return mchnt_txn_dt;
	}

	public void setMchnt_txn_dt(String mchnt_txn_dt) {
		this.mchnt_txn_dt = mchnt_txn_dt;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getResp_code() {
		return resp_code;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public String getSuc_amt() {
		return suc_amt;
	}

	public void setSuc_amt(String suc_amt) {
		this.suc_amt = suc_amt;
	}

	public String getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
