package pay.portal.web.message;

import java.io.Serializable;

/**
 * 交易流水
 * 
 * @author jeff
 *
 */

public class TradeReq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3567501253553423599L;
	

	/**
	 * 用户id
	 */
	public Long userId;

	/**
	 * 交易类型
	 */
	public Integer type;

	public Integer month;
	
	public Integer year;
	
	public Integer curPage; 
	
	public Integer pageSize; 
	
	public Integer tradeType;

	
	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	 

	public Integer getCurPage() {
		return curPage;
	}

	public void setCurPage(Integer curPage) {
		this.curPage = curPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
	 
	
}
