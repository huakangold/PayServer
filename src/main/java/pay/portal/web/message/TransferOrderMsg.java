package pay.portal.web.message;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class TransferOrderMsg implements Serializable {

	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 4858810596905134559L;
	 
	private Long orderId;
	
	private String contractNo;
	
	private String outAccount;
	
	private String inAccount;
	
	private BigDecimal amt;
	
	private int status;
	
	private Timestamp cts;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getOutAccount() {
		return outAccount;
	}

	public void setOutAccount(String outAccount) {
		this.outAccount = outAccount;
	}

	public String getInAccount() {
		return inAccount;
	}

	public void setInAccount(String inAccount) {
		this.inAccount = inAccount;
	}

	public BigDecimal getAmt() {
		return amt;
	}

	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Timestamp getCts() {
		return cts;
	}

	public void setCts(Timestamp cts) {
		this.cts = cts;
	}

	@Override
	public String toString() {
		return "TransferOrderMsg [orderId=" + orderId + ", contractNo="
				+ contractNo + ", outAccount=" + outAccount + ", inAccount="
				+ inAccount + ", amt=" + amt + ", status=" + status + ", cts="
				+ cts + "]";
	}
	
	

}
