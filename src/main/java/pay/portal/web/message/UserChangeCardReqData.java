package pay.portal.web.message;

import java.io.File;
import java.io.Serializable;

/**
 * Created by lipei on 2017/10/9.
 */
public class UserChangeCardReqData implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String mchnt_cd = "";		//商户代码
    private String mchnt_txn_ssn = "";	//流水号
    private String login_id = "";		//个人用户
    private String city_id = "";		//开户行地区代码
    private String bank_cd = "";		//开户行行别
    private String card_no = "";		//帐号
    private File file1 = new File("");	//图片1
    private File file2 = new File("");	//图片2


    private String pathUrl = "";
    public String createSignValue(){
        String src = bank_cd +"|"+ card_no +"|"+ city_id +"|" + login_id + "|" + mchnt_cd +"|"+mchnt_txn_ssn;
        System.out.println("签名明文>>>>"+src);
        return src;
    }

    public String getMchnt_cd() {
        return mchnt_cd;
    }
    public void setMchnt_cd(String mchnt_cd) {
        this.mchnt_cd = mchnt_cd;
    }
    public String getMchnt_txn_ssn() {
        return mchnt_txn_ssn;
    }
    public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
        this.mchnt_txn_ssn = mchnt_txn_ssn;
    }
    public String getLogin_id() {
        return login_id;
    }
    public void setLogin_id(String login_id) {
        this.login_id = login_id;
    }
    public String getCity_id() {
        return city_id;
    }
    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }
    public String getBank_cd() {
        return bank_cd;
    }
    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }
    public String getCard_no() {
        return card_no;
    }
    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }
    public File getFile1() {
        return file1;
    }
    public void setFile1(File file1) {
        this.file1 = file1;
    }
    public File getFile2() {
        return file2;
    }
    public void setFile2(File file2) {
        this.file2 = file2;
    }

    public String getPathUrl() {
        return pathUrl;
    }

    public void setPathUrl(String pathUrl) {
        this.pathUrl = pathUrl;
    }


    @Override
    public String toString() {
        return "UserChangeCardReqData{" +
                "bank_cd='" + bank_cd + '\'' +
                ", mchnt_cd='" + mchnt_cd + '\'' +
                ", mchnt_txn_ssn='" + mchnt_txn_ssn + '\'' +
                ", login_id='" + login_id + '\'' +
                ", city_id='" + city_id + '\'' +
                ", card_no='" + card_no + '\'' +
                ", file1=" + file1 +
                ", file2=" + file2 +
                ", pathUrl='" + pathUrl + '\'' +
                '}';
    }
}