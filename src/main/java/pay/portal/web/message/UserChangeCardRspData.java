package pay.portal.web.message;

/**
 * Created by lipei on 2017/10/9.
 */
public class UserChangeCardRspData {

    private String resp_code="";
    private String resp_desc="";
    private String mchnt_cd="";
    private String mchnt_txn_ssn="";
    public String getResp_code() {
        return resp_code;
    }
    public void setResp_code(String resp_code) {
        this.resp_code = resp_code;
    }
    public String getResp_desc() {
        return resp_desc;
    }
    public void setResp_desc(String resp_desc) {
        this.resp_desc = resp_desc;
    }
    public String getMchnt_cd() {
        return mchnt_cd;
    }
    public void setMchnt_cd(String mchnt_cd) {
        this.mchnt_cd = mchnt_cd;
    }
    public String getMchnt_txn_ssn() {
        return mchnt_txn_ssn;
    }
    public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
        this.mchnt_txn_ssn = mchnt_txn_ssn;
    }
    @Override
    public String toString() {
        return "UserChangeCardRspData [resp_code=" + resp_code + ", resp_desc="
                + resp_desc + ", mchnt_cd=" + mchnt_cd + ", mchnt_txn_ssn="
                + mchnt_txn_ssn + "]";
    }





}
