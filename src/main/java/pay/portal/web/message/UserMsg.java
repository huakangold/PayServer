package pay.portal.web.message;

public class UserMsg {

	/**
	 * 用户ID
	 */
	Long userId;

	/**
	 * 流水号
	 */
	String mchnt_txn_ssn;

	/**
	 *
	 */
	public String token;

	/**
	 * 签名
	 */
	public String sign;

	/**
	 * key
	 */
	public String apiKey;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMchnt_txn_ssn() {
		return mchnt_txn_ssn;
	}

	public void setMchnt_txn_ssn(String mchnt_txn_ssn) {
		this.mchnt_txn_ssn = mchnt_txn_ssn;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	
}
