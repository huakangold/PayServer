package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;

import pay.entity.ApiSecret;

public interface IApiSecret extends JpaRepository<ApiSecret, Integer> {
	
	public  ApiSecret findByApikey(String  apikey);
}
