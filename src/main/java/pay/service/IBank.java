package pay.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pay.entity.Bank;

public interface IBank extends JpaRepository<Bank, Integer>{

	/** 根据id获实体 */
	public Bank findById(Integer id);
	
	/**
	 * 根据bankId 获取银行全名
	 * @return
	 */
	public String findNameById(Integer id);

	
	/**
	 * 根据是否显示获取银行列表
	 * @param showStatus
	 * @return
	 */
	public List<Bank> findByShowStatus(Integer showStatus);
	
	@Query(" select bankCode from Bank b where b.bankName=?1")
	public String getBankCode(String bankName);

	/**
	 * 根据bankCode 获取bankId
	 * @param bankCode
	 * @return
	 */
	@Query(" select id from Bank b where b.bankCode=?1")
	public Integer getBankIdByCode(String bankCode);
}
