package pay.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pay.entity.City;

public interface ICity extends JpaRepository<City, Integer> {
	
	public List<City> findByProCode(Integer proCode);
}