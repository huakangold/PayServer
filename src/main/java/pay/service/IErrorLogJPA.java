package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;

import pay.entity.ErrorLog;

public interface IErrorLogJPA extends JpaRepository<ErrorLog, Long> {
	@SuppressWarnings("unchecked")
	public ErrorLog save(ErrorLog p);
}
