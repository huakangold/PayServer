package pay.service;

import pay.entity.FYResultCode;

import java.util.List;

public interface IFYResultCode {

	/**
	 *
	 * @param code
	 * @return
	 */
	public String getCodeMsg(String code);
}