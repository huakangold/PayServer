package pay.service;

import java.util.List;

import pay.common.HqlFilter;
import pay.entity.HistoryPayment;


public interface IHistoryPayment {
	public HistoryPayment findById(long id);

	/**
	 * 保存
	 * 
	 * @param b
	 * @return
	 */
	public void add(HistoryPayment historyPayment);
	//public void add(int company,String paymentType,String oldBankNo,String NewBankNo,Long userId);
	/**
	 * 更新
	 * 
	 * @param b
	 */
	public void update(HistoryPayment u);

	public List<HistoryPayment> findByFilter(HqlFilter hqlFilter, int pageNum,
			int pageSize);

	public List<HistoryPayment> findByFilter(HqlFilter hqlFilter);
}
