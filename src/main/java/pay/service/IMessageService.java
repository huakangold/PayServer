package pay.service;

/**
 * Created by Sheldon Chen on 2017/3/16.
 */
public interface IMessageService {

    public String sendMsg(String aliases, String msg, String title, String menu, String msgTypeConstants);
}
