package pay.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import pay.entity.OrderInfo;

public interface IOrderHelper    {	
	 
	/**
	 * 获取订单列表
	 */
	public Boolean updateOrder(OrderInfo orerInfo);

	/**
	 * 获取订单列表
	 */
	public Boolean saveOrder(OrderInfo orerInfo);


	/**
	 * 更新订单
	 * @param
	 * @return
	 */
	public void updOrderInRedis(String keyStr, OrderInfo order);

	/**
	 * 获取24小时内起标的顶订单
	 * 
	 * @return
	 */
	public List<OrderInfo> getProdNewStartOrders();

	public void addIntoRedis(String keyStr, OrderInfo order);

	public OrderInfo getFromRedis(String key);

	public Boolean deleteFromRedis(String keyStr);

	public Boolean addToRouteInfo(OrderInfo order);

	/**
	 * 获取用户的冻结订单笔数
	 * 
	 * @param userId
	 * @return
	 */
	public Long getDJOrderAccount(Long userId);

	public boolean addToRouteOrders(Long orderId, Long userId, int company);

	public Boolean updateProductInfo(Long productId, BigDecimal amount);

	public void deleteFromRedis(String constantTradeType, String ssn);

	public void updateOrderStatus(Long orderId, Integer payStatus,
			Integer confirmStatus, String contract_no, String constantTradeType);
	
	/**
	 * 从redis中获取需要划拨的订单
	 */
	public List<String> getTranseferOrderListFromRedis();
	
	/**
	 * 将已划拨的订单从redis中删除
	 * @param orderId
	 * @param
	 * @param
	 * @param
	 */
	public void delTransferOrderFromRedis(Long orderId, String orderTransferMsg);
	
	/**
	 * 从redis中获取需要回款的订单
	 */
	public List<String> getPaybackOrderListFromRedis();
	
	/**
	 * 将已回款划拨的订单从redis中删除
	 * @param orderId
	 * @param
	 * @param
	 * @param
	 */
	public void delPaybackOrderFromRedis(Long orderId, String orderPaybackMsg);
	
	/**
	 * 获取待回款订单
	 * @param
	 * @param
	 * @param
	 * @param
	 */
	public List<Map<String, Object>> getPayBackOrders (String productId);


	public Boolean refundOrder(Long orderId);
}
