package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import pay.entity.OrderInfo;

public interface IOrderJpa extends JpaRepository<OrderInfo, Long> {

	public OrderInfo findById(Long  id);
    //修改Storm的state的状态
	@Transactional
    @Modifying
    @Query(value = "update t_order_info t set t.confirmStatus=?2, t.orderConfirmDate = ?3 where t.id=?1 ", nativeQuery = true)
    public void updateOrderConfirmStatus(Long id, int status, Long timeLong);

    //修改Storm的state的状态
	@Transactional
    @Modifying
    @Query(value = "update t_order_info t set t.payStatus=?2, t.confirmStatus = ?3 where t.id=?1 ", nativeQuery = true)
    public void updateOrderToPayback(Long id, int payStatus, int confirmStatus);


    @Query(value = "select count(1) from t_order_info where payStatus=2 and confirmStatus = 3 and userId =?1", nativeQuery = true)
    public Long getDJOrderAccount(Long userId);

    @Query(value = "select count(1) from orderView where (payStatus in (1,2,3) or  confirmStatus in (1,2,3)) and paymentNo=?1", nativeQuery = true)
    public Long getOrderInProgressAccount(String paymentNo);
}
