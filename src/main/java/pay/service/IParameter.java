package pay.service;


import pay.common.HqlFilter;
import pay.entity.Parameter;

import java.util.List;

public interface IParameter {
	
	
	
	public List<Parameter> findByFilter(HqlFilter hqlFilter);
	
	
	public List<Parameter> findByFilter(HqlFilter hqlFilter, Integer curPage, Integer pageSize);
	
	
	public Parameter findById(long id);

	/**
	 * 保存
	 * 
	 * @param
	 * @return
	 */
	public void add(Parameter u);
	
	 

	/**
	 * 删除
	 * 
	 * @param
	 * @return
	 */
	public void delete(Parameter u);

	/**
	 * 更新
	 * 
	 * @param
	 */
	public void update(Parameter u);

	/**
	 * 根据名称获取参数
	 * @param name
	 * @return
	 */
	public Parameter getByName(String name);
	
	
	 
	/**
	 * 判断是否存在
	 * @param name
	 * @return
	 */
	public Boolean existByName(String name);
	
	/**
	 * 根据标记符获取参数列表
	 * @param flag
	 * @return
	 */
	public List<Parameter> getByFlag(String flag);
}
