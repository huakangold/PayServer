package pay.service;

import java.math.BigDecimal;


/**
 * 回款划拨
 * @author jeff
 *
 */
public interface IPayBack{
	
	/**
	 * 获取流水号
	 */
	public String getSSN(Long orderId);
	
	
	/**
	 * 进行回款
	 * @param mchnt_txn_ssn
	 * @param paybackOrderStr
	 * @return
	 */
	public  Boolean payBack(String mchnt_txn_ssn);
	
	
	/**
	 * 根据回款结果更新相关信息
	 * @param mchnt_txn_ssn
	 * @param result
	 * @return
	 */
	public  Boolean updPaybackResult(String mchnt_txn_ssn, Boolean result);
	
	/**
	 * 将预授权记录保存在redis中
	 */
	public void addPaybackRecordIntoRedis(String mchnt_txn_ssn, String paybackOrderStr);
	
	
	/**
	 * 将预授权记录从redis中取出
	 */
	public String getPaybackRecordFromRedis(String mchnt_txn_ssn);
	
	
	/**
	 * 将预授权记录从redis中删除
	 */
	public void delPaybackRecordFromRedis(String mchnt_txn_ssn);
	
	/**
	 * 添加到记录表中
	 * 
	 * @param orderId
	 * @param userId
	 * @param amount
	 * @param mchnt_txn_ssn
	 * @param tradeDesc
	 */
	public void addIntoTradeTable(Long orderId, Long userId, String login_id, BigDecimal amount,
			String mchnt_txn_ssn, String tradeDesc, String resp_code, String resp_desc);
	
	
	
	/**
	 * 获取回款划拨的结果
	 * @param orderId
	 * @param mchnt_txn_ssn
	 * @param in_cust_no
	 * @param contract_no
	 * @param amt
	 * @return
	 */
	public Boolean getPayBackResult(Long orderId, String mchnt_txn_ssn,
			String in_cust_no, String out_cust_no, BigDecimal amt);
	
	
	/**
	 * 将回款记录写入日志文件中
	 * @param record
	 */
	public void  writeFile(String responseCode, Long orderId,
			String in_cust_no, String out_cust_no, BigDecimal amt, Long userId);
	
	public void  writeProductFile(String responseCode, Long orderId,
			String in_cust_no, String out_cust_no, BigDecimal amt, Long userId,Long productId);
	
	/**
	 * 调用富友接口进行回款划拨
	 * @param mchnt_txn_ssn
	 * @param out_cust_no
	 * @param in_cust_no
	 * @param amt
	 * @return
	 */
	public String sendPostToFY(String mchnt_txn_ssn, String out_cust_no, String in_cust_no,  BigDecimal amt);
}
