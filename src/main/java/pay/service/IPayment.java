package pay.service;

import java.util.List;

import pay.common.HqlFilter;
import pay.entity.Payment;
import pay.entity.SysUser;

public interface IPayment {
	public Payment findById(long id);

	/**
	 * 保存
	 * 
	 * @param b
	 * @return
	 */
	public Long add(Payment u);

	/**
	 * 保存并提供company 信息
	 * 
	 * @param b
	 * @return
	 */
	public Long add(Payment u, Integer companyId);

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	public void delete(Payment u);

	/**
	 * 更新
	 * 
	 * @param b
	 */
	public void update(Payment u);

	public Payment getByUserId(Long userId);

	public List<Payment> findByFilter(HqlFilter hqlFilter, int pageNum,
			int pageSize);

	public List<Payment> findByFilter(HqlFilter hqlFilter);

	public Long countByFilter(HqlFilter hqlFilter);

	public Boolean existById(Long id);

	/**
	 * 判断两种支付方式是否相同
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public Boolean isSame(Payment a, Payment b);

	/**
	 * 通过银行卡号获取支付方式
	 * 
	 * @param paymentNo
	 * @return
	 */
	public Payment getByPaymentNo(String paymentNo);

	public void setAddCardCompany(Integer company, Payment payment);

	public void changeSupportCardStatus(Integer company, Payment payment,
			Integer cardStatus);

	public String getCardCompany(Integer company, Payment payment);

	public Boolean checkSamePayment(Payment pay01, Payment pay02);

	public Boolean checkFullMsg(Payment pay);

	public Payment addOrUpdatePayment(String capAcntNo, String cust_nm,
			String certif_id, String mobile_no, SysUser user);

	public Payment addNewCard(String cust_nm, String certif_id, String capAcntNo,// 添加新卡
			String mobile_no, SysUser user);

	public Payment updateNewPaymentExist(List<Payment> currentPaymentList,// 更新已存在的而在富友添加的卡。
			String certif_id, String cust_nm);

	public Long updateExistPayment(Payment willChangePayment);

	public Long updateOldPayment(Payment oldPayment);
}
