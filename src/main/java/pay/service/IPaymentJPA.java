package pay.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import pay.entity.Payment;

public interface IPaymentJPA extends JpaRepository<Payment, Long>{
	
	@Cacheable(value = "Id"  , key = "#Id", unless="#result==null")
	public Payment findById(Long  Id);
	
	@Query("select supportCompany from Payment u where u.id=?2")	
	public String findByCompCard(Integer company,Long id);
	
	public List<Payment> findByPaymentNo(String  paymentNo);
	
	public List<Payment> findByPhoneAndCardStatus(String  phone,int cardstatus);
	
	public List<Payment> findBySupportCompany(String  supportCompany);
	
	@SuppressWarnings("unchecked")
	public Payment save(Payment p);
	
	
	@CacheEvict(value = "id" , key = "#id")
	public void delete(Long id);

	@Query  ("select u from Payment u where u.userId=?1")
	public List<Payment> findPaymentList(Long userId);
	
	@Transactional
	@Modifying 
	@Query("update Payment u set u.supportCompany = :supportCompany where u.id = :id") 
	public void updatePaymentByCompany(@Param("id")  Long id,@Param("supportCompany") String supportCompany);
	
	@Transactional
	@Modifying 
	@Query("update Payment u set u.cardStatus = :cardStatus,u.mchnt_txn_ssn=:mchnt_txn_ssn where u.id = :id") 
	public void updatePaymentByCardStatus(@Param("id")  Long id,@Param("cardStatus") int cardStatus,@Param("mchnt_txn_ssn") String mchnt_txn_ssn);
	
//	@Transactional
//	@Modifying 
//	@Query("update Payment u set u.userId=:userId,u.accountName=:accountName,u.identityNo=:identityNo,u.paymentNo=:paymentNo,u.phone=:phone,u.cts=:cts,u.lastUseTime=:lastUseTime,u.paymentMethodId=:paymentMethodId,u.cardStatus = :cardStatus,u.mchnt_txn_ssn=:mchnt_txn_ssn where u.id = :id") 	
//	public void updatePayment(Payment payment);
	
}
