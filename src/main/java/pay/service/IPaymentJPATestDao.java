package pay.service;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import pay.entity.Payment;
import pay.serviceImpl.dao.base.IBaseRepository;

public interface IPaymentJPATestDao extends IBaseRepository<Payment, Long>,
		JpaSpecificationExecutor<Payment> {
	// SysUser findByRealName(String realName);
}