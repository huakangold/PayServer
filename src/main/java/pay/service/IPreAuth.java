package pay.service;

import java.math.BigDecimal;
import java.util.Map;

import pay.portal.web.message.PreAuthMsg;
import pay.portal.web.message.PreAuthResultMsg;


 

public interface IPreAuth{
	
	/**
	 * 开启预授权操作
	 * @param req
	 * @return
	 */
	public PreAuthResultMsg PreAuth(PreAuthMsg req);


	/**
	 * 写入日志文件中
	 * @param
	 */
	public Boolean updPreautResult(String mchnt_txn_ssn, String contract_no, Boolean result);

	/**
	 *
	 * @param mchnt_txn_ssn
	 * @param contract_no
	 * @param result
	 */
	public void updPreautResultTask(String mchnt_txn_ssn, String contract_no, Boolean result);


	/**
	 * 检查token 
	 * @param token
	 * @param userId
	 * @return
	 */
	public Boolean checkToken(String token, Long userId);
	
	/**
	 * 根据参数封装map
	 * @param orderId
	 * @param userId
	 * @param token
	 * @param sign
	 * @param apiKey
	 * @return
	 */
	public Map<String, String> getRequestMap(Long orderId, Long userId,
			String token, String sign, String apiKey);
	
	/**
	 * 将参数传给富友，获取预授权结果
	 * @param mchnt_txn_ssn
	 * @param out_cust_no
	 * @param in_cust_no
	 * @param amt
	 * @return
	 */
	public String sendPostToFY(String mchnt_txn_ssn, String out_cust_no,
			String in_cust_no, String amt);
	
	/**
	 * 根据预授权，更新订单的相关信息
	 * @param orderId
	 * @param payStatus
	 * @param confirmStatus
	 * @param contract_no
	 */
	public void updateOrderStatus(Long orderId, Integer payStatus,
			Integer confirmStatus, String contract_no);
	
	/**
	 * 添加到预授权表中
	 * @param orderId
	 * @param userId
	 * @param login_id
	 * @param amount
	 * @param mchnt_txn_ssn
	 * @param contract_no
	 * @param tradeDesc
	 * @param resp_code
	 * @param resp_desc
	 */
	public void addIntoTradeTable(Long orderId, Long userId, String login_id,
			BigDecimal amount, String mchnt_txn_ssn, String contract_no,
			String resp_code, String resp_desc);
	
	/**
	 * 写入日志文件中
	 * @param record
	 */
	public void writeFile(String record);
	
}