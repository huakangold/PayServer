package pay.service;

import pay.portal.web.message.PreAuthMsg;
import pay.portal.web.message.PreAuthResultMsg;
import pay.portal.web.message.ResultMsg;

import java.math.BigDecimal;
import java.util.Map;


public interface IPreAuthCancel {
	
	/**
	 * 预授权取消
	 * @param req
	 * @return
	 */
	public ResultMsg preAuthCancel(PreAuthMsg req);

	/**
	 * 预授权取消
	 * @param
	 * @return
	 */
	public ResultMsg preAuthCancel(Long orderId);

	/**
	 * 预授权取消
	 * @param
	 */
	public ResultMsg preAuthCancel(Long orderId, Long userId, String out_cust_no, String in_cust_no, String contract_no);


	
}