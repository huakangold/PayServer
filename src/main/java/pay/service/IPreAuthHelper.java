//package pay.service;
//
//import java.math.BigDecimal;
//
//import com.hk.commons.entity.order.OrderInfo;
//
//public interface IPreAuthHelper {
//
//	public Boolean deleteFromRedis(String ssn);
//
//	public Boolean addIntoRedis(PreAuth preAuth);
//
//	public PreAuth getFromRedis(String ssn);
//
//	/**
//	 * 更加订单号，获取预授权订单信息
//	 * 
//	 * @param orderId
//	 * @return
//	 */
//	public OrderInfo getOrderInfo(Long orderId);
//
//	/**
//	 * 将预授权记录添加到trade 表中,以供查询使用
//	 * 
//	 * @param pa
//	 */
//	public void addIntoTradeTable(PreAuth pa);
//
//	/**
//	 * 更改订单状态，同步获取预授权结果时使用
//	 * 
//	 * @param orderId
//	 * @param payStatus
//	 * @param confirmStatus
//	 * @param contract_no
//	 */
//	public void updateOrderStatus(Long orderId, Integer payStatus,
//			Integer confirmStatus, String contract_no);
//
//	public boolean updateProductInfo(Long productId, BigDecimal amount);
//
//	/**
//	 * 根据预授权结果更改订单状态, 异步回调接口使用
//	 * 
//	 * @param mchnt_txn_ssn
//	 * @param contractNo
//	 * @param result
//	 * @return
//	 */
//	public Boolean updatePreAuthResult(String mchnt_txn_ssn, String contractNo,
//			Boolean result);
//
//	public boolean addToRouteOrders(Long orderId, Long userId, int company);
// }