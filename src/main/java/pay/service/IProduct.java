package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;
import pay.entity.ProductInfo;

public interface IProduct extends JpaRepository<ProductInfo, Long> {
	
	public ProductInfo findById(Long  id);


	public String getNameById(long Id);
	
//	@Transactional
//	@Modifying
//	@Query("update ProductInfo u set u.payBackStatus = :payBackStatus where u.id = :id")
//	public void updateProduct(@Param("id") Long id,
//			@Param("payBackStatus") int payBackStatus);

}