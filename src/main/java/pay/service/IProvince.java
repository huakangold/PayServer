package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;

import pay.entity.Province;

public interface IProvince extends JpaRepository<Province, Integer>{

}
