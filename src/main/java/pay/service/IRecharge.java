//package pay.service;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import pay.entity.Recharge;
//
// 
//public interface IRecharge extends JpaRepository<Recharge, Integer>{
//	/**
//	 * 
//	 * @Cacheable: 插入缓存
//	 * value: 缓存名称
//	 * key: 缓存键，一般包含被缓存对象的主键，支持Spring EL表达式
//	 * unless: 只有当查询结果不为空时，才放入缓存
//	 * @CacheEvict: 失效缓存
//	 * @param name
//	 * @return
//	 */
//	/*
//	//@Cacheable(value = "name"  , key = "100", unless="#result==null")
//	public ChongZhi findByName(String name);
//	
//	//@Cacheable(value = "id"  , key = "100", unless="#result==null")
//	public ChongZhi findById(Long id);
//	
//	//@SuppressWarnings("unchecked")
//	//@CachePut(value = "name" , key = "100")
//	public ChongZhi save(ChongZhi cz);
//	*/
// }