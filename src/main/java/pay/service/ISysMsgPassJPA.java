package pay.service;


import org.springframework.data.jpa.repository.JpaRepository;
import pay.entity.SendMsgPass;


public interface ISysMsgPassJPA extends JpaRepository<SendMsgPass, Long>{
	public SendMsgPass findById(long id);

}
