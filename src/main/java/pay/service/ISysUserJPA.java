package pay.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import pay.entity.SysUser;

public interface ISysUserJPA extends JpaRepository<SysUser, Long> {
	/**
	 * 
	 * @Cacheable: 插入缓存 value: 缓存名称 key: 缓存键，一般包含被缓存对象的主键，支持Spring EL表达式 unless:
	 *             只有当查询结果不为空时，才放入缓存
	 * @CacheEvict: 失效缓存
	 * @param name
	 * @return
	 */
	// @Cacheable(value = "name" , key = "100", unless="#result==null")
	public SysUser findByName(String name);

	// @Cacheable(value = "id" , key = "100", unless="#result==null")
	public SysUser findById(Long id);

	@SuppressWarnings("unchecked")
	// @CachePut(value = "name" , key = "100")
	public SysUser save(SysUser user);

	@Query("from SysUser u where u.name=:name")
	public SysUser findUser(@Param("name") String name);

	/*
	 * userService.upUserByPayment(userId,identity, accountName);
	 */

	@Transactional
	// 更新操作需要添加此注解，否则报错
	@Modifying
	@Query("update SysUser u set u.identity = :identity,u.realName=:realName where u.id = :id")
	public void updateSysUser(@Param("id") Long id,
			@Param("identity") String identity,
			@Param("realName") String realName);

	/*
	 * update SysUser u set u.identity = :identity, b=b.b, c=b.c from t a,t b
	 * where (a.条件1) and (b.条件2)
	 */

	// @CacheEvict(value = "name" , key = "100")
	public void delete(Long id);
}
