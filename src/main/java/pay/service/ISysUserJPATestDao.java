package pay.service;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import pay.entity.SysUser;
import pay.serviceImpl.dao.base.IBaseRepository;

public interface ISysUserJPATestDao extends IBaseRepository<SysUser, Long>,
		JpaSpecificationExecutor<SysUser> {
	SysUser findByRealName(String realName);
}