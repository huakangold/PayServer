package pay.service;

import java.util.List;

import pay.common.HqlFilter;
import pay.entity.Trade;

public interface ITrade {

	/**
	 * 保存
	 * 
	 * @param trade
	 */
	public Boolean save(Trade trade);

	/** 根据id获实体 */
	public Trade findById(Long id);

	/**
	 * 根据type 获取交易类型
	 * 
	 * @return
	 */
	public Trade findByType(Integer type);
	
	
	
	/**
	 * 判断交易记录是否存在 
	 * @return
	 */
	public Boolean isExist(Trade  trade);

	/**
	 * 获取全部的交易信息
	 * 
	 * @return
	 */
	public List<Trade> getAll(Long userId, Long startTime, Long endTime,
			Integer tradeType, Integer curpage, Integer pageSize);

	/**
	 * 获取总条目数
	 * 
	 * @param userId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Integer getTotalNum(Long userId, Long startTime, Long endTime);

	/**
	 * 获取冻结的交易记录数
	 * 
	 * @return
	 */
	public Integer getTotalDJCount(Long userId);

	/**
	 * 获取总条目数
	 *
	 * @return
	 */
	public Trade getFromRedis(String mmchnt_txn_ssn);

	public void writeTradeFile(Trade trade, String logFilePath);

	/**
	 * 预授权取消，隐藏相应记录
	 * @param contract_no
	 */
	public void hiddenRecordByContractNo(String contract_no);


	/**
	 * 将错误信息发送给IT和运营
	 * @param msg
     */
	public void sendErrorMsg(String msg, String emailMsgTitle, String emailMsgContent);

	/**
	 * 根据trade 获取产品名称
	 * @param trade
	 * @return
     */
	public String getProductNameByTrade(Trade trade);

	/**
	 * 根据hql查询交易记录
	 * @param hql
	 * @return
	 */
	List<Trade> findByFilter(HqlFilter hql);

	/**
	 * 更新
	 * @param trade
	 */
	void update(Trade trade);

}
