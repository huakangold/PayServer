package pay.service;

import java.math.BigDecimal;

/*
 * 预授权划拨
 */

public interface ITransfer{
	
	/**
	 * 获取流水号
	 */
	public String getSSN(Long orderId);
	
	/**
	 * 将预授权记录保存在redis中
	 */
	public void addTransferRecordIntoRedis(String mchnt_txn_ssn, String transferMsgStr);
	
	
	/**
	 * 将预授权记录从redis中取出
	 */
	public String getTransferRecordIntoRedis(String mchnt_txn_ssn);
	
	
	/**
	 * 将预授权记录从redis中删除
	 */
	public void delTransferRecordIntoRedis(String mchnt_txn_ssn);
	
	/**
	 * 获取相关信息
	 * 
	 * @param order
	 * @param orderService
	 */
	public Boolean transfer(String mchnt_txn_ssn);
	
	
	/**
	 * 根据流水号更新划拨的结果
	 * @param mchnt_txn_ssn
	 * @param result
	 * @return
	 */
	public Boolean updTransferResult(String mchnt_txn_ssn, Boolean result);
	
	
	/**
	 * 添加到记录表中
	 * 
	 * @param orderId
	 * @param userId
	 * @param amount
	 * @param mchnt_txn_ssn
	 * @param tradeDesc
	 */
	public void addIntoTradeTable(Long orderId, Long userId, String login_id, BigDecimal amount,
			String mchnt_txn_ssn, String tradeDesc, String resp_code, String resp_desc);
	
	
	
	/**
	 * 获取划拨的结果
	 * @param orderId
	 * @param mchnt_txn_ssn
	 * @param out_cust_no
	 * @param in_cust_no
	 * @param contract_no
	 * @param amt
	 * @return
	 */
	public Boolean getTransferResult(Long orderId, String mchnt_txn_ssn, String out_cust_no,
			String in_cust_no, String contract_no, BigDecimal amt);
	
	
	/**
	 * 将划拨记录写入日志文件中
	 * @param record
	 */
	public void writeFile(String record);
	
	
	/**
	 * 调用富友接口进行划拨
	 * @param mchnt_txn_ssn
	 * @param contract_no
	 * @param out_cust_no
	 * @param in_cust_no
	 * @param amt
	 * @return
	 */
	public String sendPostToFY(String mchnt_txn_ssn, String contract_no,
			String out_cust_no, String in_cust_no, BigDecimal amt);
}
