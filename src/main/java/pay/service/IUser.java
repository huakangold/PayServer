package pay.service;

import java.util.List;
import java.util.Map;

import pay.entity.User;

public interface IUser {
	public List<User> getList();
	
	public Map<String,String> getMoreTable();
}