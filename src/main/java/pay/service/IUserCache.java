package pay.service;

import pay.common.AllAssetsDto;

import java.util.Map;

public interface IUserCache {


	/**
	 * 移除redis 中用户投资资产的缓存
	 * @param userId
	 * @return
     */
	public Boolean removeUserAssetCache(Long userId);

	/**
	 * 获取redis 中用户投资资产的缓存
	 * @param userId
	 * @return
     */
	public AllAssetsDto getUserAssetCache(Long userId);

	/**
	 * 设置redis 中用户投资资产的缓存
	 * @param userId
	 * @return
	 */
	public Boolean setUserAssetCache(Long userId,  AllAssetsDto allAssetsDto);
}