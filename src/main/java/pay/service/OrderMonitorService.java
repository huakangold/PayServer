package pay.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pay.common.ConstantsOrder;
import pay.entity.OrderInfo;
import pay.utils.RedisUtils;

import java.util.HashMap;

/**
 * Created by Antinomy on 17/2/21.
 */
@Service
public class OrderMonitorService {
    private Logger logger = LoggerFactory.getLogger(OrderMonitorService.class);

    @Value("${producer.host}")
    private String host;

    private RestTemplate template = new RestTemplate();


    public void preAuthStarted(Long orderId) {
        OrderInfo order = RedisUtils.getOrderInfo(orderId);

        if(order ==null) {
            logger.info("订单{}不存在,不能监控",orderId);
            return;
        }

        String url = "http://" + host + "/mgr/orderMonitor/preAuthStarted/"+orderId;
        remoteCall(orderId, url,null);
    }

    public void preAuthFinish(Long orderId) {
        OrderInfo order = RedisUtils.getOrderInfo(orderId);

        if(order ==null) {
            logger.info("订单{}不存在,不能监控",orderId);
            return;
        }

        String url = "http://" + host + "/mgr/orderMonitor/preAuthFinish/"+orderId;
        remoteCall(orderId, url,null);
    }

    public void preAuthFailed(Long orderId,String msg) {
        OrderInfo order = RedisUtils.getOrderInfo(orderId);

        if(order ==null) {
            logger.info("订单{}不存在,不能监控",orderId);
            return;
        }

        String url = "http://" + host + "/mgr/orderMonitor/preAuthFailed/"+orderId;
        remoteCall(orderId, url,msg);
    }

    public void success(Long orderId) {
        OrderInfo order = RedisUtils.getOrderInfo(orderId);

        if(order ==null) {
            logger.info("订单{}不存在,不能监控",orderId);
            return;
        }

        String url = "http://" + host + "/mgr/orderMonitor/success/"+orderId;
        remoteCall(orderId, url,null);
    }

    private void remoteCall(Long orderId, String url,String msg) {
        try {
            HttpEntity<Object> requestEntity;

            if(StringUtils.isBlank(msg))
                requestEntity = getHttpEntity();
            else
                requestEntity = getHttpEntity(msg);

            logger.info("remote call {}",url);

            ResponseEntity<String> result = template.postForEntity(url, requestEntity, String.class);

            logger.info("订单{}预授权监控开始",result);

        } catch (Exception e) {
            logger.error("订单{}预授权监控异常！{}",orderId, e);
        }
    }

    private HttpEntity<Object> getHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(headers);
    }

    private HttpEntity getHttpEntity(String msg) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HashMap resp = new HashMap();
        resp.put("result",msg);

        HttpEntity result = new HttpEntity(resp, headers);

        return result;
    }

}
