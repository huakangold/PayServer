package pay.service.huochai;

import org.springframework.stereotype.Component;
import pay.portal.web.message.HuoChaiOrderInfoReq;

import java.util.List;

/**
 * Created by Sheldon Chen on 2016/11/24.
 */
public interface IHuochaiOrderInformation {

    public String writeOrderInformationToFile(List<HuoChaiOrderInfoReq> reqs);
}
