package pay.service.sys;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import pay.common.HqlFilter;
import pay.common.SqlFilter;
import pay.entity.SysUser;

public interface ISysUser {
	
	/** 根据id获实体 */
	public SysUser findById(Long id);
	
	public SysUser getByMobileId(String mobileId);
		
	/** 分页获取 */
	public List<SysUser> find(int pageNum, int pageSize);
	
	/** 保存  */
	public Long add(SysUser o);	
	
	public SysUser delete(Long id);
	/**
	 * 获取全部的用户
	 * @return
	 */
	public List<SysUser> getAll();
	
	/**
	 * 根据推荐码获取用户
	 * @param orgID
	 * @return
	 */
	public List<SysUser>  getUserByRecommendCode(String recommendCode);
	
	/**
	 * 根据真实姓名获取用户
	 * @param orgID
	 * @return
	 */
	public SysUser  getUserByRealName(String realName);
	
	public List<SysUser>  getUserListByRealName(String realName);
	
	public List<SysUser> findByFilter(HqlFilter hqlFilter);
	
	public List<SysUser> findByFilter(HqlFilter hqlFilter,int pageNum, int pageSize);
	
	public Long countByFilter(HqlFilter hqlFilter);
	
	public void update(SysUser sysUser);
	
	public boolean existById(Long id);
	
	public boolean existByName(String name);
	
	public boolean existByMobileId(String mobileId);
	/**
	 * 账号查询用户
	 * 
	 * @param account 系统用户帐户
	 * @return
	 */
	public SysUser getByAccount(String account);
		
	public List<SysUser> findBySqlFilter(SqlFilter sqlFilter);

	public List<SysUser> findBySqlFilter(SqlFilter sqlFilter, int page, int rows) ;

	public BigInteger countBySqlFilter(SqlFilter sqlFilter) ;

	/**
	 * 根据accountId 获取用户
	 * @param accountId
	 * @return
	 */
	public SysUser getByAccountId(String accountId);
	
	/**
	 * 根据phoneNum 获取用户
	 * @param phoneNum
	 * @return
	 */
	public SysUser getByPhoneNum(String phoneNum);

	/**
	 * 根据phoneNum 获取用户
	 * @param phoneNum
	 * @param closeAccount 销户状态（0：末销户，1：已销户）
	 * @return
	 */
	SysUser getByPhoneNum(String phoneNum,Integer closeAccount);
	
	/**
	 * 通过id 获取用户名
	 * @param id
	 * @return
	 */
	public String getNameById(Long id);
	
	/**
	 * 设置用户的绑卡信息为已支付
	 */
	public void upUserByPayment(Long userId, String accountId, String identity, String accountName,int hasVirtualAccount);
	
	/**
	 * 检查用户id 长度
	 * @param id
	 * @return
	 */
	public Long checkId(Long id);
	
	 
	/**
	 * 根据用户id ,获取理财师id, 理财师不存在时则返回null
	 * @param userId
	 * @return
	 */
	public Long getFinUserIdByUserId(Long userId);
	
	/**
	 * 根据用户id ,获取推荐人数目
	 * @param userId
	 * @return
	 */
	public Integer getRelUserNumById(Long userId);
	
	/**
	 * 查询用户余额
	 * @param userId
	 * @return
	 */
	public BigDecimal getUserLeftMoney(SysUser user);
}

