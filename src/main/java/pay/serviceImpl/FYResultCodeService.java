package pay.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import pay.entity.FYResultCode;
import pay.service.IFYResultCode;

import java.util.List;

@Component
public class FYResultCodeService implements IFYResultCode {

	protected Logger logger = LoggerFactory
			.getLogger(this.getClass().getName());

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public String getCodeMsg(String code) {
		String sqlNum = "SELECT count(*)  FROM t_fy_resultCode a WHERE a.code = '" + code + "'";

		logger.info("num sql ={}", sqlNum);
		Integer  num = jdbcTemplate.queryForObject(sqlNum, Integer.class);

		if(num != 1) {
			return null;
		}

		String sqlMsg = "SELECT codeMsg  FROM t_fy_resultCode a WHERE a.code = '" + code + "'";

		String codeMsg = jdbcTemplate.queryForObject(sqlMsg, String.class);
		return codeMsg;
	}
}





