package pay.serviceImpl;


import org.springframework.stereotype.Service;

import pay.entity.HistoryPayment;
import pay.service.IHistoryPayment;
import pay.serviceImpl.dao.base.impl.BaseServiceImpl;
import pay.utils.DateUtil;
import pay.utils.LongId;
@Service
public class HistoryPaymentImpl extends BaseServiceImpl<HistoryPayment>  implements IHistoryPayment {

	@Override
	public HistoryPayment findById(long id) {
		return getById(id);
	}
	@Override
	public void add(HistoryPayment historyPayment) {
		Long cts=System.currentTimeMillis();
		String visibleCts=DateUtil.transferLongToDate("yyyy-MM-dd HH:mm:ss", cts);
		historyPayment.setCts(cts);
		historyPayment.setVisibleCts(visibleCts);
		historyPayment.setId(LongId.get());		
		super.save(historyPayment);		
		
	}

//	@Override
//	public void add(int company,String paymentType,String oldBankNo,String NewBankNo,Long userId) {
//		HistoryPayment historyPayment=new HistoryPayment();
//		historyPayment.setCompany(company);
//		Long cts=System.currentTimeMillis();
//		String visibleCts=DateUtil.transferLongToDate("yyyy-MM-dd HH:mm:ss", cts);
//		historyPayment.setCts(cts);
//		historyPayment.setVisibleCts(visibleCts);
//		historyPayment.setId(LongId.get());
//		historyPayment.setNewBankNo(NewBankNo);
//		historyPayment.setOldBankNo(oldBankNo);
//		historyPayment.setPaymentType(paymentType);
//		historyPayment.setUserId(userId);
//		super.save(historyPayment);		
//	}

}
