//package pay.serviceImpl;
//
//import java.math.BigDecimal;
//import java.net.MalformedURLException;
//import java.util.HashMap;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import pay.common.ConstantFYJZH;
//import pay.common.ConstantOrderStatus;
//import pay.common.ConstantsOrder;
//import pay.entity.PreAuth;
//import pay.entity.Trade;
//import pay.service.IOrderHelper;
//import pay.service.IPreAuth;
//import pay.service.IPreAuthHelper;
//import pay.service.IProduct;
//import pay.service.ITrade;
//import pay.utils.JsonUtils;
//import pay.utils.RedisCilent;
//
//import com.hk.commons.entity.order.OrderInfo;
//
//@Service
//public class PreAuthHelperImpl implements IPreAuthHelper {
//
//	@Autowired
//	private IProduct prdService;
//
//	@Autowired
//	private ITrade tradeService;
//
//	@Autowired
//	private IPreAuth preAuthService;
//
//	@Autowired
//	private IOrderHelper orderHelperService;
//
//	@Value("${producer.host}")
//	private String host;
//
//	private Logger logger = LoggerFactory.getLogger(PreAuthHelperImpl.class);
//
//	public Boolean deleteFromRedis(String ssn) {
//		String keyStr = ConstantFYJZH.TRADE_RECORD + ssn;
//
//		// 从缓存中获取预授权订单
//		if (RedisCilent.existsKey(keyStr)) {
//			RedisCilent.delKey(keyStr);
//		}
//
//		return true;
//
//	}
//
//	public Boolean addIntoRedis(PreAuth pa) {
//
//		String keyStr = pa.getMchnt_txn_ssn();
//
//		RedisCilent.setString(keyStr, JsonUtils.toJson(pa), 259200);
//
//		return true;
//
//	}
//
//	public PreAuth getFromRedis(String ssn) {
//
//		PreAuth preAuth = null;
//		String keyStr = ssn;
//
//		// 从缓存中获取预授权订单
//		if (RedisCilent.existsKey(keyStr)) {
//			String preAuthStr = RedisCilent.getString(keyStr);
//			preAuth = JsonUtils.toBean(preAuthStr, PreAuth.class);
//		}
//
//		return preAuth;
//	}
//
//	/**
//	 * 从redis 中获取订单信息，并进行修改
//	 * 
//	 * @param orderId
//	 * @param payStatus
//	 * @param confirmStatus
//	 * @throws MalformedURLException
//	 */
//	public void updateOrderStatus(Long orderId, Integer payStatus,
//			Integer confirmStatus, String contract_no) {
//
//		String orderKey = ConstantsOrder.ORDER_FYJZH_PRIX + orderId;
//
//		OrderInfo order = orderHelperService.getFromRedis(orderKey);
//		order.setContract_no(contract_no);
//		order.setPayStatus(payStatus);
//		order.setConfirmStatus(confirmStatus);
//
//		orderHelperService.saveOrder(order);
//
//	}
//
//	/**
//	 * 从redis 中获取订单信息，并进行修改
//	 * 
//	 * @param orderId
//	 * @throws MalformedURLException
//	 */
//	public OrderInfo getOrderInfo(Long orderId) {
//		OrderInfo order = null;
//
//		String orderKey = ConstantsOrder.ORDER_FYJZH_PRIX + orderId;
//
//		logger.info("orderKey = " + orderKey);
//		order = orderHelperService.getFromRedis(orderKey);
//		return order;
//	}
//
//	// 添加到trade table 中
//	public void addIntoTradeTable(PreAuth pa) {
//		BigDecimal amount = new BigDecimal(pa.getAmount());
//		Long userId = pa.getUserId();
//		Trade trade = new Trade();
//		trade.setOrderId(pa.getOrderId());
//		trade.setUserId(userId);
//		trade.setAmt(amount);
//		trade.setCts(System.currentTimeMillis());
//		trade.setMchnt_txn_ssn(pa.getMchnt_txn_ssn());
//		trade.setFlowType(ConstantFYJZH.BALANCE_CUT);
//		trade.setType(ConstantFYJZH.TRADE_TY_YSQ);
//		trade.setTradeDesc(pa.getPreAuthDesc());
//		tradeService.save(trade);
//
//	}
//
//	@Override
//	public boolean addToRouteOrders(Long orderId, Long userId, int company) {
//		OrderInfo orderInfo = new OrderInfo();
//		orderInfo.setId(orderId);
//		orderInfo.setUserId(userId);
//		orderInfo.setCompanyId(company);
//		return orderHelperService.addToRouteInfo(orderInfo);
//	}
//
//	/**
//	 * 通过回调接口获取预授权结果，并更改订单
//	 * 
//	 * @param mchnt_txn_ssn
//	 * @param contractNo
//	 * @param result
//	 * @return
//	 */
//	public Boolean updatePreAuthResult(String mchnt_txn_ssn, String contractNo,
//			Boolean result) {
//
//		PreAuth pa = getFromRedis(mchnt_txn_ssn);
//
//		if (pa != null) {
//			pa.setResp_code("0000");
//			pa.setContract_no(contractNo);
//
//			// 添加到交易记录表中
//			addIntoTradeTable(pa);
//
//			// 保存预授权记录
//			preAuthService.save(pa);
//
//			// 更改订单状态
//			updateOrderStatus(pa.getOrderId(),
//					ConstantOrderStatus.PAY_STATUS_TRUE,
//					ConstantOrderStatus.CONFIRM_STATUS_FALSE, contractNo);
//
//			// 从换从中删除相关信息
//			deleteFromRedis(mchnt_txn_ssn);
//
//		}
//
//		return true;
//	}
//
//	@Override
//	public boolean updateProductInfo(Long productId, BigDecimal amount) {
//		Boolean returnVal = false;
//		try {
//			Map<Object, Object> map = new HashMap<Object, Object>();
//			map.put("productId", productId);
//			map.put("amount", String.valueOf(amount));
//			System.out.println(amount.longValue());
//			String url = "http://" + host
//					+ "/hk/RMICtr/updateProductAmount?sign=inner";
//			HttpHeaders headers = new HttpHeaders();
//			headers.setContentType(MediaType.APPLICATION_JSON);
//			HttpEntity<Object> requestEntity = new HttpEntity<Object>(map,
//					headers);
//			ResponseEntity<Boolean> result = new RestTemplate().exchange(url,
//					HttpMethod.POST, requestEntity, Boolean.class, map);
//			if (!result.getBody()) {
//				logger.info("更新产品信息！productId={}", productId);
//			} else {
//				returnVal = true;
//			}
//		} catch (Exception e) {
//			logger.info("更新产品信息异常！");
//			logger.error("更新产品信息异常！", e);
//		}
//		return returnVal;
//	}
// }
