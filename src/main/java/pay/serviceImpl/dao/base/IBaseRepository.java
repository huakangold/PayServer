package pay.serviceImpl.dao.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import pay.common.HqlFilter;

@NoRepositoryBean
// Spring Data Jpa在启动时就不会去实例化IBaseRepository这个接口
public interface IBaseRepository<T, ID extends Serializable> extends
		JpaRepository<T, ID> {

	String sharedCustomMethod();

	/**
	 * 通过HqlFilter查询对象
	 * 
	 * @param hqlFilter
	 * @return
	 */
	public List<T> findByFilter(HqlFilter hqlFilter, String className);

	public List<T> getByHql(String hql, Map<String, Object> params);

}