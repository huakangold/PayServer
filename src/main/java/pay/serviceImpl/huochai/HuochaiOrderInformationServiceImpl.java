package pay.serviceImpl.huochai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pay.portal.web.message.HuoChaiOrderInfoReq;
import pay.service.huochai.IHuochaiOrderInformation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Sheldon Chen on 2016/11/24.
 */
@Component
public class HuochaiOrderInformationServiceImpl implements IHuochaiOrderInformation {

    private Logger logger = LoggerFactory.getLogger(HuochaiOrderInformationServiceImpl.class);

    @Value("${huochai.data.file}")
    private String fileSave;

    @Override
    public String writeOrderInformationToFile(final List<HuoChaiOrderInfoReq> req) {
        SimpleDateFormat sf = new SimpleDateFormat("YYYMMddHHmmss");
        String date = sf.format(System.currentTimeMillis());
        String path = "huochai_1124_nav_history_" + date;
        String fileName = path + ".txt";

        // 生成订单流文件
        Path filePath = Paths.get(fileSave + fileName);
        if (!Files.exists(Paths.get(fileSave + fileName))) {
            ScheduledExecutorService executor = Executors
                    .newScheduledThreadPool(1);

            Runnable writeFileTask = () -> {
                String fileStr = this.getStringFromReq(req);
                try {
                    Files.write(filePath, fileStr.getBytes("UTF-8"));
                } catch (IOException e) {
                    logger.error("file:{} error:{}", fileName, e);
                }
            };

            ScheduledFuture<?> future = executor.schedule(writeFileTask, 3,
                    TimeUnit.SECONDS);
        }
        return fileName;
    }

    private String getStringFromReq(List<HuoChaiOrderInfoReq> req) {
        StringBuilder sb = new StringBuilder();
        sb.append("amount|userName|identity|paymentNo|payStatus|phoneNum|orderTradeDate|dividendDate");
        sb.append("\r\n");
        for (HuoChaiOrderInfoReq orderInfoReq: req) {
            sb.append(orderInfoReq.getAmount());
            sb.append("|");
            sb.append(orderInfoReq.getUserName());
            sb.append("|");
            sb.append(orderInfoReq.getIdentity());
            sb.append("|");
            sb.append(orderInfoReq.getPaymentNo());
            sb.append("|");
            sb.append(orderInfoReq.getPayStatus());
            sb.append("|");
            sb.append(orderInfoReq.getPhoneNum());
            sb.append("|");
            sb.append(orderInfoReq.getOrderTradeDate().toString());
            sb.append("|");
            sb.append(orderInfoReq.getDividendDate().toString());
            sb.append("\r\n");
        }
        return sb.toString();
    }

}
