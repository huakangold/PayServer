package pay.task;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pay.entity.req.CouponWalletBatchAddReq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Sheldon Chen on 2016/12/15.
 */
@Component
public class NewYearTask {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${coupon.wallet.add}")
    private String url;

//    @Scheduled(cron = "0 0 0 1 1 *")
    public void test() {
        logger.info("registCts: " + System.currentTimeMillis());
        String selectUserSql = "select id from SysUser where registCts < ?";
        List<Map<String, Object>> resultList = this.jdbcTemplate.queryForList(selectUserSql,
                new Object[] { System.currentTimeMillis() });
        logger.info(String.format("result: %d", resultList.size()));
        CouponWalletBatchAddReq couponWalletBatchAddReq = new CouponWalletBatchAddReq();

        for (int i = 0; i < resultList.size(); i=i+100) {
            List<String> userIdList = new ArrayList<>();
            List<Map<String, Object>> ids = resultList.subList(i, i + 100);

            for (Map<String, Object> result: ids) {
                userIdList.add(String.valueOf(result.get("id")));
            }
            couponWalletBatchAddReq.setUserIdList(userIdList);
            couponWalletBatchAddReq.setCardNum(3L);
            couponWalletBatchAddReq.setCardId("394172104950796556");

            JSONObject jsonObject = new JSONObject(couponWalletBatchAddReq);
            logger.info(jsonObject.toString());

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(this.url);
                StringEntity s = new StringEntity(jsonObject.toString());
                s.setContentEncoding("UTF-8");
                s.setContentType("application/json");
                httpPost.setEntity(s);
                HttpResponse response = httpClient.execute(httpPost);
                int statusCode = response.getStatusLine().getStatusCode();
                HttpEntity httpEntity = response.getEntity();
                logger.info("http status code: " + statusCode);
                logger.info("http response entity: " + EntityUtils.toString(httpEntity));
            } catch (Exception e) {
                logger.error("exception", e);
            } finally {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    logger.error("Sleep fail", e);
                }
            }

        }



    }
}
