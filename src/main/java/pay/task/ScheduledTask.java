package pay.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

	/**
	 * @Scheduled标注中，我们使用了三种方式来实现了同一个功能：每隔5秒钟记录一次当前的时间：
	 * 
	 *                                                  fixedRate =
	 *                                                  5000表示每隔5000ms，Spring
	 *                                                  scheduling会调用一次该方法
	 *                                                  ，不论该方法的执行时间是多少
	 *                                                  fixedDelay =
	 *                                                  5000表示当方法执行完毕5000ms后
	 *                                                  ，Spring
	 *                                                  scheduling会再次调用该方法 cron
	 *                                                  = "*\/5 * * * * * *
	 *                                                  "提供了一种通用的定时任务表达式，这里表示每隔5秒执行一次，更加详细的信息可以参考cron表
	 *                                                  达 式 。
	 */

	
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	private Integer count0 = 1;
	private Integer count1 = 1;
	private Integer count2 = 1;

//	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() throws InterruptedException {
		logger.info(String.format("---第%s次执行，当前时间为：%s", count0++,
				dateFormat.format(new Date())));
	}

//	@Scheduled(fixedDelay = 5000)
	public void reportCurrentTimeAfterSleep() throws InterruptedException {
		logger.info(String.format("===第%s次执行，当前时间为：%s", count1++,
				dateFormat.format(new Date())));
	}

//	@Scheduled(cron = "*/5 * * * * *")
	public void reportCurrentTimeCron() throws InterruptedException {
		logger.info(String.format("+++第%s次执行，当前时间为：%s", count2++,
				dateFormat.format(new Date())));
	}
}
