package pay.utils;

/**
 * 金额转换
 */

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class BalanceUtils {

	/**
	 * 富友转换金额，将12345转换成123.45元
	 * 
	 * @param amount_In
	 * @return
	 */
	public static String getFYVal(String amount_In) {
		if (StringHelper.isEmpty(amount_In)) {
			amount_In = "0";
		}

		String result = "";
		Integer checkInt = Integer.valueOf(amount_In);
		float amount = (float) (checkInt / 100.00);
		DecimalFormat df = new DecimalFormat("0.00");
		result = df.format(amount).toString();

		return result;
	}

	/**
	 * 富友转换金额，将123.45元转换成12345分
	 * 
	 * @param amount_In
	 * @return
	 */
	public static String chgToFYVal(BigDecimal amount_In) {
		if (amount_In == null) {
			return "0";
		}

		amount_In = amount_In.multiply(new BigDecimal("100"));

		return amount_In.toBigInteger().toString();
	}
}
