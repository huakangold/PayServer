package pay.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lipei on 2017/10/7.
 */
public class Base64Util {

    private static final BASE64Encoder encoder = new BASE64Encoder();


    private static final BASE64Decoder decoder = new BASE64Decoder();

    /*
     * 得到指定图片的base64编码
     */
    public static String getImageBinary(String path, String suffix) throws IOException {
        File f = new File(path);
        BufferedImage bi = ImageIO.read(f);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, suffix, baos);
        byte[] bytes = baos.toByteArray();

        return encoder.encodeBuffer(bytes).trim();
    }

    public static void createImage(String base64Source, String path) throws IOException {
        byte[] bytes = decoder.decodeBuffer(base64Source);
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.flush();
        fos.close();
    }


    public static String getImageBinary02(String path, String suffix) throws IOException {
        File f = new File(path);
        BufferedImage bi = ImageIO.read(f);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, suffix, baos);
        byte[] bytes = baos.toByteArray();

        return  new String(bytes, Charset.defaultCharset());
    }

    public static void createImage02(String base64Source, String path) throws IOException {
        byte[] bytes = base64Source.getBytes();
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.flush();
        fos.close();
    }



    public static void main(String[] args) throws IOException {

        creareFile01();
        creareFile02();
    }

    public static void creareFile01()throws IOException {
        // 得到图片的base64编码
        String base64 = getImageBinary("./pics/6666.jpg", "jpg");
        System.out.println("base64Str =" + base64);
        System.out.println("base64Str length =" + base64.length());
        //去掉得到的base64编码的换行符号
        Pattern p = Pattern.compile("\\s*|\t|\r|\n");
        Matcher m = p.matcher(base64);
        String after = m.replaceAll("");
        //打印去掉换行符号base64编码
        System.out.println(after);

        createImage(base64, "./pics/6667.jpg");
    }

    public static void creareFile02()throws IOException {
        // 得到图片的base64编码
        String normalStr = getImageBinary02("./pics/6666.jpg", "jpg");
        System.out.println("normalStr =" + normalStr);
        System.out.println("normalStr length =" + normalStr.length());


        createImage02(normalStr, "./pics/6668.jpg");
    }
}
