package pay.utils;

public class CacheConstants {
	/**
	 * 拦截器校验用户token
	 * */
	public static final String VALIDATE_TOKEN = "VALIDATE_TOKEN_";

	/**
	 * 交易
	 * */
	public static final String TRADE = "TRADE_";

	/**
	 * 交易
	 * */
	public static final String CREATEACCOUNT = "CREATEACCOUNT_";
	/**
	 * 用户绑卡后推送理财师
	 * */
	public static final String FINALCIAL_PUSH = "FINALCIAL_PUSH";
	
	/**
	 * 登陆token
	 * */
	public static final String UserLog_Token = "UserLog_Token_";
}
