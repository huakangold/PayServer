package pay.utils;

import java.util.ResourceBundle;

public class ConfigReader {
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle("application-" + System.getProperty("Env"));// 不需要添加.properties

	public static String getConfig(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static int getInt(String key) {
		return Integer.parseInt(RESOURCE_BUNDLE.getString(key));
	}
}
