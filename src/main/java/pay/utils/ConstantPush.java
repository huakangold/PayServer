package pay.utils;

/**
 * Created by Sheldon Chen on 2017/3/16.
 */
public class ConstantPush {

    /**
     * 回款
     */
    public static String PAYBACK = "payback";

    /**
     * 充值
     */
    public static String RESERVATION = "reservation";

    /**
     * 提现
     */
    public static String WITHDRAW = "withdraw";
}
