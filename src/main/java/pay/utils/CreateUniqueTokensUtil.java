package pay.utils;

import java.util.UUID;

public class CreateUniqueTokensUtil {

	/**
	 * default size 8
	 * 
	 * @param userId
	 * @return
	 */
	synchronized public static String getRandomToken(String userId) {
		int size = 8;

		return getRandomToken(userId, size);
	}

	/**
	 * 
	 * @param userId
	 * @return 八位随机码
	 */
	synchronized public static String getRandomToken(String userId, int size) {// 8位
		String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h",
				"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
				"u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
				"6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
				"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
				"U", "V", "W", "X", "Y", "Z" };

		// StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "")
				.substring(0, size);

		RedisCilent.setString(CacheConstants.VALIDATE_TOKEN + uuid, userId,
				120);
		return uuid;
	}

	/**
	 * 生成正常位数的UUID
	 * 
	 * @param name
	 * @return
	 */
	synchronized public static String getToken(String userId) {// 正常位数
		String token = UUID.randomUUID().toString().replace("-", "");
		RedisCilent.setString(CacheConstants.VALIDATE_TOKEN + userId, token);
		return token;
	}

	/**
	 * 根据字符串生成固定UUID
	 * 
	 * @param name
	 * @return
	 */
	synchronized public static String getUUID(String name) {
		UUID uuid = UUID.nameUUIDFromBytes(name.getBytes());
		String str = uuid.toString();
		String uuidStr = str.replace("-", "");
		return uuidStr;
	}
}
