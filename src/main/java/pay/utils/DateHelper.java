package pay.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 * 
 * @author pansenxin
 * @date 2014-03-27
 */
public class DateHelper {

	public static final int YEAR = 1;
	public static final int MONTH = 2;
	public static final int DAY = 3;
	public static final int HOUR = 4;
	public static final int MINUTE = 5;
	public static final int SECOND = 6;
	public static final int MILLIS = 7;

	public static String getTimeStamp() {
		String temp = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		temp = sdf.format(new java.util.Date());
		return temp;
	}

	// 根据特定格式获取当前时间 参数format为显示时间格式 如：yyyy-MM-dd HH:mm:ss.SSS
	public static String getNowDateByFormat(String format) {
		return (new SimpleDateFormat(format)).format(new Date());
	}

	// 根据特定格式获取指定时间 参数format为显示时间格式 如：yyyy-MM-dd HH:mm:ss.SSS
	public static String getAssignDateByFormat(Date date, String format) {
		return (new SimpleDateFormat(format)).format(date);
	}
	
	// 根据特定格式获取指定时间 参数format为显示时间格式 如：yyyy-MM-dd HH:mm:ss.SSS
	public static String getAssignTimestampByFormat(Timestamp ts, String format) {
		return (new SimpleDateFormat(format)).format(ts);
	}

	// 根据指定编码获取相关时间 参数date为时间，code指定返回形式，如返回年，返回月....
	public static long getCorrelateTime(Date date, int code) {
		long rel = 0;
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		switch (code) {
		case 1:
			rel = (int) c.get(1);
			break;
		case 2:
			rel = (int) (c.get(2) + 1);
			break;
		case 3:
			rel = (int) (c.get(5));
			break;
		case 4:
			rel = (int) (c.get(11));
			break;
		case 5:
			rel = (int) (c.get(12));
			break;
		case 6:
			rel = (int) (c.get(13));
			break;
		case 7:
			rel = c.getTimeInMillis();
			break;
		default:
			break;
		}
		return rel;
	}

	// 在指定的时间加上相应时间 参数date为时间，code指定添加形式，如添加月，添加天....，time为添加数量
	public static Date addTime(Date date, int code, int time) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		long mills = cal.getTimeInMillis();
		switch (code) {
		case 2:
			cal.add(2, time);
			break;
		case 3:
			cal.setTimeInMillis(mills + time * 24L * 3600L * 1000L);
			break;
		case 4:
			cal.setTimeInMillis(mills + time * 3600L * 1000L);
			break;
		case 5:
			cal.setTimeInMillis(mills + time * 60L * 1000L);
			break;
		default:
			break;
		}
		return cal.getTime();
	}

	// 将字符串改为时间类型 参数str为需转变字符串
	public static Date stringToDate(String str) {
		if (str != null && str.length() > 0) {
			String format = "";
			switch (str.length()) {
			case 10:
				format = "yyyy-MM-dd";
				break;
			case 19:
				format = "yyyy-MM-dd HH:mm:ss";
				break;
			case 16:
				format = "yyyy-MM-dd HH:mm";
				break;
			case 4:
				format = "yyyy";
				break;
			case 7:
				format = "yyyy-MM";
				break;
			case 23:
				format = "yyyy-MM-dd HH:mm:ss.SSS";
				break;
			}
			DateFormat dateFormat = new SimpleDateFormat(format);
			try {
				return dateFormat.parse(str);
			} catch (ParseException e) {
				return null;
			}
		}
		return null;
	}

	// 判断时间是否为今天
	public static boolean isToday(String checkDate) {
		Date date = stringToDate(checkDate);
		getAssignDateByFormat(date, "yyyy-MM-dd");
		return getAssignDateByFormat(new Date(), "yyyy-MM-dd").equals(
				getAssignDateByFormat(date, "yyyy-MM-dd"));
	}

	// 将时间变为Timestamp
	public static Timestamp dateToTimestamp(Date date) {
		Timestamp tmResult = null;
		if (date != null)
			tmResult = new Timestamp(date.getTime());
		return tmResult;
	}

	// 获取时间差 参数beginDate,endDate时间格式为Date,code指定返回形式，如3返回天数，4返回小时数
	public static long between(Date beginDate, Date endDate, int code) {
		long diff = endDate.getTime() - beginDate.getTime();
		long rel = 0;
		long day = diff / (24 * 60 * 60 * 1000);
		long hour = (diff / (60 * 60 * 1000));
		long min = (diff / (60 * 1000));
		long s = (diff / 1000);
		switch (code) {
		case 3:
			rel = day;
			break;
		case 4:
			rel = hour;
			break;
		case 5:
			rel = min;
			break;
		case 6:
			rel = s;
			break;
		case 7:
			rel = diff;
			break;
		default:
			break;
		}
		return rel;
	}

	// 获取时间差 参数beginDate,endDate时间格式为String,code指定返回形式，如3返回天数，4返回小时数
	public static long between(String beginDate, String endDate, int code) {
		Date d1 = stringToDate(beginDate);
		Date d2 = stringToDate(endDate);
		long diff = d2.getTime() - d1.getTime();
		long rel = 0;
		long day = diff / (24 * 60 * 60 * 1000);
		long hour = (diff / (60 * 60 * 1000));
		long min = (diff / (60 * 1000));
		long s = (diff / 1000);
		switch (code) {
		case 3:
			rel = day;
			break;
		case 4:
			rel = hour;
			break;
		case 5:
			rel = min;
			break;
		case 6:
			rel = s;
			break;
		case 7:
			rel = diff;
			break;
		default:
			break;
		}
		return rel;
	}

	// 获取时间差 参数时间格式为String 返回结果为天小时分秒毫秒，如：1 20:58:57.950
	public static String between(String beginDate, String endDate) {
		String rel = "";
		Date d1 = stringToDate(beginDate);
		Date d2 = stringToDate(endDate);
		long diff = d2.getTime() - d1.getTime();
		long day = diff / (24 * 60 * 60 * 1000);
		long hour = (diff / (60 * 60 * 1000) - day * 24);
		long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		long s = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
		long ms = (diff - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000
				- min * 60 * 1000 - s * 1000);
		rel = day + " " + hour + ":" + min + ":" + s + "." + ms;
		return rel;
	}
}
