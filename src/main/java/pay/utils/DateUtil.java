package pay.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public final static String START_TIME = "2016-08-01 00:00:00";
	
	public static String getStartTimeOfDay(Calendar c){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 00:00:00"); 
		String first = format.format(c.getTime());
		return first;
	}
	
	public static String getEndTimeOfDay(Calendar c){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 23:59:59"); 
		String first = format.format(c.getTime());
		return first;
	}
	
	
	/**
	 * 当天的开始时间
	 * 
	 * @return
	 */
	public static Calendar getNowTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}
	
	/**
	 * 当天的开始时间
	 * 
	 * @return
	 */
	public static long startOfTodDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date date = calendar.getTime();
		return date.getTime();
	}

	/**
	 * 当天的结束时间
	 * 
	 * @return
	 */
	public static long endOfTodDay() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		Date date = calendar.getTime();
		return date.getTime();
	}

	/**
	 * 将 2001-01-01T12:01:01 类型转换为 Long
	 * 
	 * @param
	 *            '2001-01-01T12:01:01'
	 * @return Long
	 */
	public static long ToDateTimeFromUTCISO8601(String dateTimeString) {
		Long timeLong = null;
		String dateString = dateTimeString.substring(0, 10) + " "
				+ dateTimeString.substring(11, dateTimeString.length());

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {

			timeLong = format.parse(dateString).getTime();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return timeLong;
	}
	
	/**
	 * 将 2001-01-01T12:01:01 类型转换为 Long
	 * 
	 * @param
	 *            '2001-01-01T12:01:01'
	 * @return Long
	 */
	public static long ToDateTimeFrom01(String dateTimeString) {
		Long timeLong = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		try {

			timeLong = format.parse(dateTimeString).getTime();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return timeLong;
	}

	/**
	 * 将 2010-01-01 类型转换为 Long
	 * 
	 * @param dayString
	 *            '2001-01-01'
	 * @return Long
	 */
	public static long ToDateTimeFromDay(String dayString) {
		Long timeLong = null;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {

			timeLong = format.parse(dayString).getTime();
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return timeLong;
	}

	/**
	 * 将 2010-01-01 类型转换为 String
	 * 
	 * @param
	 *            '2001-01-01 01:01:01'
	 * @return Long
	 */
	public static String longToLongDate(long lo) {
		Date date = new Date(lo);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sd.format(date);
	}

	/**
	 * 将 1464056763499 类型转换为 String
	 * 
	 * @param
	 *            '2001-01-01'
	 * @return Long
	 */
	public static String longToShortDate(long lo) {
		Date date = new Date(lo);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		return sd.format(date);
	}
	
	/**
	 * 将 1464056763499 类型转换为 String
	 * 
	 * @param
	 *            '20010101'
	 * @return Long
	 */
	public static String longToShortDate02(long lo) {
		Date date = new Date(lo);
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
		return sd.format(date);
	}

	/**
	 * 
	 * 把毫秒转化成日期
	 * 
	 * @param dateFormat
	 *            (日期格式，例如：yyyyMMddHHmmss)
	 * 
	 * @param millSec
	 *            (毫秒数)
	 * 
	 * @return
	 */

	@SuppressWarnings("unused")
	public static String transferLongToDate(String dateFormat, Long millSec) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date = new Date(millSec);
		return sdf.format(date);
	}

	/**
	 * 
	 * 把毫秒转化成日期
	 * 
	 * @param
	 * 
	 * 
	 * 
	 * @return 2016-07-05 11:10:24
	 */
	public static Long transferStringToLong(String dayString) {
		Long timeLong = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		
		if(dayString.length() == 8){
		  format = new SimpleDateFormat("yyyyMMdd");
		}
		
		try {
			timeLong = format.parse(dayString).getTime();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return timeLong;
	}
	
	/**
	 * 获取一个月的第一天， 返回值为String
	 * @param year
	 * @param month
	 * @return
	 */
	public static String getFirstDayOfMonth(Integer year, Integer month){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 00:00:00"); 
		
		//获取当前月第一天：
		Calendar c = Calendar.getInstance();    
		
		//设置为1号,当前日期既为本月第一天 
		c.set(year, month - 1,1);
		c.set(Calendar.DAY_OF_MONTH, 1);
		String first = format.format(c.getTime());
		
		return first;
	}
	
	/**
	 * 获取一个月的第一天， 返回值为Long
	 * @param year
	 * @param month
	 * @return
	 */
	public static Long getFirstTimeOfMonth(Integer year, Integer month){
		//获取当前月第一天：
		Calendar c = Calendar.getInstance();

		//设置为1号,当前日期既为本月第一天
		c.set(year, month - 1,1);

		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		c.set(Calendar.DAY_OF_MONTH, 1);
	 
		
		return c.getTimeInMillis();
	}
	
	/**
	 * 获取一个月的最后一天的时间， 返回值为String
	 * @param year
	 * @param month
	 * @return
	 */
	public static String getLastDayOfMonth(Integer year, Integer month){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 23:59:59"); 
		
		//获取当前月最后一天
		Calendar c = Calendar.getInstance();    
		
		c.set(year, month - 1, 1);



		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DATE));
		
		String last = format.format(c.getTime());
		
		return last;
	}
	
	/**
	 * 获取一个月的最后一天的时间， 返回值为Long
	 * @param year
	 * @param month
	 * @return
	 */
	public static Long getLastTimeOfMonth(Integer year, Integer month){

		//获取当前月最后一天
		Calendar c = Calendar.getInstance();


		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);

		c.set(year, month - 1, 1);

		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DATE));
		
		return c.getTimeInMillis();
	}
	/**
	 * 获取一个月的第一天， 返回值为String
	 * @param
	 * @param
	 * @return
	 */
	public static String getNowDay(){
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd"); 
		
		//获取当前月第一天：
		Calendar c = Calendar.getInstance();    
		String first = format.format(c.getTime());
		return first;
	}
	
	

}
