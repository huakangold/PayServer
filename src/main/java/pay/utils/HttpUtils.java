package pay.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class HttpUtils {

	public static String invokeGet(String uri, int timeout)
			throws ClientProtocolException, IOException {
		return Request.Get(uri).connectTimeout(timeout).useExpectContinue()
				.execute().returnContent().asString();
	}

	public static String invokePost(String uri, Object o, int timeout)
			throws ClientProtocolException, IOException {
		String s_json = JsonUtils.toJson(o);
		return Request.Post(uri).connectTimeout(timeout).useExpectContinue()
				.bodyString(s_json, ContentType.APPLICATION_JSON).execute()
				.returnContent().asString();
	}

	public static String invokePostAddAuth(String uri, Object o, int timeout,
			String base64After) throws ClientProtocolException, IOException {
		String s_json = JsonUtils.toJson(o);
		return Request.Post(uri)
				.addHeader("Authorization", "Basic " + base64After)
				.connectTimeout(timeout).useExpectContinue()
				.bodyString(s_json, ContentType.APPLICATION_JSON).execute()
				.returnContent().asString();
	}

	public static Map<String, String> invokePostaddH(String uri, Object o,
			int timeout, String base64After, int flag)
			throws ClientProtocolException, IOException {
		Map<String, String> resultMap = new HashMap<String, String>();
		String s_json = JsonUtils.toJson(o);
		try {
			Response response = Request.Post(uri)
					.addHeader("Authorization", "Basic " + base64After)
					.connectTimeout(timeout).useExpectContinue()
					.bodyString(s_json, ContentType.APPLICATION_JSON).execute();

			HttpResponse httpResponse = response.returnResponse();
			String xmlContent = EntityUtils.toString(httpResponse.getEntity());// {"is_valid":false,"error":{"code":50011,"message":"expired code"}}
			// System.out.println(xmlContent);

			JSONObject resultObj = new JSONObject(xmlContent);

			if (flag == 1) {// 1是验证

				if (resultObj.getBoolean("is_valid")) {
					resultMap.put("code", "200");
					resultMap.put("message", "success");
				} else {
					JSONObject error = resultObj.getJSONObject("error");
					resultMap.put("code", "-1111");
					resultMap.put("message", error.get("message").toString());
				}
			} else {
				if (resultObj.has("msg_id")) {
					resultMap.put("code", "200");
					resultMap.put("msg_id", resultObj.get("msg_id").toString());
				} else {// {"error":{"code":50008,"message":"no sms code auth"}}
					JSONObject error = resultObj.getJSONObject("error");
					resultMap.put("code", "-1111");
					resultMap.put("message", error.get("message").toString());
				}
			}

		} catch (Exception e) {
			resultMap.put("code", "-1111");
			resultMap.put("message", "极光验证码发送或验证出现未知错误");
			e.printStackTrace();
		}
		return resultMap;
	}

	public static String invokePostForm(String uri, Map<String, String> params,
			int timeout) throws ClientProtocolException, IOException {
		List<NameValuePair> list = new ArrayList<NameValuePair>();

		Set<String> keySet = params.keySet();
		for (String key : keySet) {
			list.add(new BasicNameValuePair(key, params.get(key)));
		}
		return Request.Post(uri).connectTimeout(timeout).useExpectContinue()
				.bodyForm(list, Consts.UTF_8).execute().returnContent()
				.asString();
	}

}
