package pay.utils;

/**
 * 消息类型定义
 *
 * Created by Sheldon Chen on 2017/3/9.
 */
public interface MsgTypeConstants {
    String REGISTER = "1";      // 注册
    String BIND_CARD = "2";     // 绑卡
    String APPOINTMENT = "3";
    String BUY = "4";
    String PAYBACK = "5";
    String WITHDRAW = "6";
    String RECHARGE = "7";
    String ANNOUNCEMENT = "8";
    String ACTIVITY = "9";
}
