package pay.utils;


import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import pay.common.EmailRes;
import pay.common.ResultCode;

import java.util.Map;

public class MsgUtils {
	protected static Logger logger = LoggerFactory.getLogger("MsgUtils");

	public static void sendMsg(RestTemplate template,String content,String mobile){
		String tokenResult= template.getForObject("http://MSG-SERVICE/getToken", String.class);
		JSONObject tokenResultObject=new JSONObject(tokenResult);
		String msgUrl="http://MSG-SERVICE/sendMsg?content="+content+"&mobile="+mobile+"&token="+tokenResultObject.getString("result");
		logger.info("短信发送结果mobile={},msgResult={}",mobile,template.getForObject(msgUrl,String.class));	
	}


	public static void sendEmailList(RestTemplate template,String content, String msgTitle, String[] emailAddressArray){
		for(String emailAddress:emailAddressArray){
			sendEmail(template, content, msgTitle, emailAddress);
		}
	}


	public static void sendEmail(RestTemplate template, String content, String msgTitle, String emailAddress){

		EmailRes emailRes = new EmailRes();
		emailRes.setSubject(msgTitle);
		emailRes.setText(content);
		emailRes.setToName(emailAddress);

		Map<String, Object> mapObj  = template.postForObject("http://MSG-SERVICE/sendSimpleMail", emailRes, Map.class);

		if(mapObj.get("resultCode").equals(ResultCode.SUCC.getCode())){
			logger.info("发送错误邮件成功");
		}else{
			logger.error("发送邮件错误");
		}
	}
}
