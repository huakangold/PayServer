package pay.utils;

import pay.common.ConstantsOrder;
import pay.entity.OrderInfo;

/**
 * Created by Antinomy on 17/2/21.
 */
public class RedisUtils {

    public static OrderInfo getOrderInfo(Long orderId) {
        return getOrderInfo(ConstantsOrder.ORDER_FYJZH_PRIX + orderId);
    }

    public static OrderInfo getOrderInfo(String keyStr) {

        OrderInfo orderInfo = null;

        // 从缓存中获取预授权订单
        if (RedisCilent.existsKey(keyStr)) {
            String orderStr = RedisCilent.getString(keyStr);
            orderInfo = JsonUtils.toBean(orderStr, OrderInfo.class);
        }

        return orderInfo;
    }
}
