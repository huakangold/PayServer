/**
 * 
 */
package pay.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * @author psx
 * 
 */
public class ReqToModelHelper {

	// 将参数值赋值给model
	public static void copyReqValueToModel(Object req, Object model) {
		Field fields[] = req.getClass().getDeclaredFields();
		Field modelFields[] = model.getClass().getDeclaredFields();

		Field field = null;
		Field modelField = null;
		// 循环遍历
		for (int i = 0; i < fields.length; i++) {
			for (int j = 0; j < modelFields.length; j++) {
				field = fields[i];
				field.setAccessible(true);
				modelField = modelFields[j];
				modelField.setAccessible(true);

				String fieldName = field.getName();
				String methodName = fieldName.substring(0, 1).toUpperCase()
						+ fieldName.substring(1);
				String type = field.getGenericType().toString(); // 获取属性的类型
				String modelFieldType = modelField.getGenericType().toString();
				// 如果方法名相同，并且类型相同
				if (field.getName().equals(modelField.getName())
						&& field.getType().getName()
								.equals(modelField.getType().getName())) {

					try {

						if (type.equals("class java.lang.Integer")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Integer value = (Integer) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Integer.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.Long")) {

							Method m = req.getClass().getMethod(
									"get" + methodName);
							Long value = (Long) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Long.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.String")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							String value = (String) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, String.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.Boolean")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Boolean value = (Boolean) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Boolean.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.util.Date")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Date value = (Date) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Date.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.Float")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Float value = (Float) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Float.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.Short")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Short value = (Short) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Short.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.lang.Double")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Double value = (Double) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Double.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.math.BigDecimal")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							BigDecimal value = (BigDecimal) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, BigDecimal.class);
								m.invoke(model, value);
							}
						}
						if (type.equals("class java.sql.Timestamp")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);

							Timestamp value = (Timestamp) m.invoke(req);
							if (value != null) {
								m = model.getClass().getMethod(
										"set" + methodName, Timestamp.class);
								m.invoke(model, value);
							}
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

				// 如果方法名相同，并且数据的类型不同
				if (field.getName().equals(modelField.getName())
						&& (!field.getType().getName()
								.equals(modelField.getType().getName()))) {

					// 如果是将String 赋值给Timestamp

					try {
						if (field.getType().getName().endsWith(("String"))
								&& modelField.getType().getName()
										.endsWith("Timestamp")) {
							Method m;
							m = req.getClass().getMethod("get" + methodName);
							String value = (String) m.invoke(req);
							if (StringUtils.isNotBlank(value)) {
								Timestamp valueTime = DateHelper
										.dateToTimestamp(DateHelper
												.stringToDate(value));

								m = model.getClass().getMethod(
										"set" + methodName, Timestamp.class);
								m.invoke(model, valueTime);
							}
						} else if (field.getType().getName().endsWith(("Long"))
								&& modelField.getType().getName()
										.endsWith("Timestamp")) {
							Method m = req.getClass().getMethod(
									"get" + methodName);
							Long value = (Long) m.invoke(req);
							if (value != null) {
								Timestamp valueTime = new Timestamp(value);
								m = model.getClass().getMethod(
										"set" + methodName, Timestamp.class);
								m.invoke(model, valueTime);
							}
						}

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		}
	}

	private static boolean exist(Field field, Object model) {
		// TODO Auto-generated method stub

		Field modelFields[] = model.getClass().getDeclaredFields();
		for (Field a : modelFields) {
			if (field.getName().equals(a.getName())) {
				return true;
			}
		}
		return false;
	}

}
