package pay.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class SecurityUtils {
	public static PrivateKey privateKey;
	
	public static PublicKey publicKey;
	
	private static String privateKeyPath;
	
	private static String publicKeyPath;

	static {
		try {
			Security.addProvider(new BouncyCastleProvider());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
	}

	public static void initPrivateKey() {
		try {
			if (privateKey == null) {
				privateKeyPath = ConfigReader.getConfig("privateKeyPath");
				privateKey = getPrivateKey(privateKeyPath);
			}
		} catch (Exception e) {
			System.out.println("SecurityUtils初始化失败" + e.getMessage());
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
	}

	public static void initPublicKey() {
		try {
			if (publicKey == null) {
				publicKeyPath = ConfigReader.getConfig("publicKeyPath");
				publicKey = getPublicKey(publicKeyPath);
			}
		} catch (Exception e) {
			System.out.println("SecurityUtils初始化失败" + e.getMessage());
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
	}

	public static String sign(String inputStr) {
		String result = null;
		try {
			if (privateKey == null) {
				initPrivateKey();
			}

			Signature signature = Signature.getInstance("SHA1withRSA", "BC");
			signature.initSign(privateKey);
			signature.update(inputStr.getBytes("UTF-8"));
			byte[] tByte = signature.sign();
			result = Base64.encode(tByte);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
		return result;
	}

	public static boolean verifySign(String src, String signValue) {
		boolean bool = false;
		try {
			if (publicKey == null) {
				initPublicKey();
			}
			Signature signature = Signature.getInstance("SHA1withRSA", "BC");
			signature.initVerify(publicKey);
			signature.update(src.getBytes("UTF-8"));
			bool = signature.verify(Base64.decode(signValue));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
		return bool;
	}

	private static PrivateKey getPrivateKey(String filePath) {
		String base64edKey = readFile(filePath);

		PrivateKey privateKey = null;
		try {
			KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
					Base64.decode(base64edKey));
			privateKey = kf.generatePrivate(keySpec);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
		return privateKey;
	}

	private static PublicKey getPublicKey(String filePath) {
		String base64edKey = readFile(filePath);

		PublicKey publickey = null;
		try {
			KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
					Base64.decode(base64edKey));
			publickey = kf.generatePublic(keySpec);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("密钥初始化失败");
		}
		return publickey;
	}

	private static String readFile(String fileName) {
		try {
			File f = new File(fileName);
			FileInputStream in = new FileInputStream(f);
			int len = (int) f.length();

			byte[] data = new byte[len];
			int read = 0;
			while (read < len) {
				read += in.read(data, read, len - read);
			}
			in.close();
			return new String(data);
		} catch (IOException e) {
		}
		return null;
	}
}
