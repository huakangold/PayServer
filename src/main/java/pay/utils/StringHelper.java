package pay.utils;

public class StringHelper {

	public static final String EMPTY = "";

	public static boolean isEmpty(String str) {
		return (str == null) || (str.length() == 0);
	}

	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	public static boolean isBlank(String str) {
		int strLen;
		if ((str == null) || ((strLen = str.length()) == 0)
				|| str.equals("null"))
			return true;

		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}

	public static String trim(String str) {
		if (isEmpty(str)) {
			return str;
		}
		StringBuffer buf = new StringBuffer(str);
		int index = 0;
		while (buf.length() > index) {
			if (Character.isWhitespace(buf.charAt(index)))
				buf.deleteCharAt(index);
			else {
				index++;
			}
		}
		return buf.toString();
	}
	
	public static String getCodeById(String code,Long maxId){
		Long xh =1l;
		String orderId ="";
		if (maxId == null || maxId == 0) {
			orderId = code + "000" + String.valueOf(xh);
		} else {
			xh = maxId ;
			String xhStr = String.valueOf(xh);
			if (xh >= 10000) {
				return "-1";
			} else if (xh >= 1 && xh < 10) {
				xhStr = "000" + String.valueOf(xh);
			} else if (xh >= 10 && xh < 100) {
				xhStr = "00" + String.valueOf(xh);
			}else if(xh >= 100 && xh < 1000){
				xhStr = "0" + String.valueOf(xh);
			}				
			orderId = code + xhStr;
		}
		return orderId;
	}
	
	
}
