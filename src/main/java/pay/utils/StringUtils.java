package pay.utils;

import static strman.Strman.*; 

public class StringUtils {

	/**
	 * A Java 8 library for working with String. It is inspired by dleitee/strman.
	 */
	
	
	
	public static void main(String[] args) {
		StringUtils s = new StringUtils();
		//append result => "abc"
		System.out.println(append("a", "b" , "c"));
		//appendArray result => "foobar"
		System.out.println(appendArray("f", new String[]{"o", "o", "b", "a", "r"}));
		//at result => Optional("f")
		System.out.println(at("foobar", 0));
		//between result => ["abc","def"]
		System.out.println(between("[abc][def]", "[", "]")[0]);
		//chars  result => ["t", "i", "t", "l", "e"]
		System.out.println(chars("title"));
		//collapseWhitespace result => "foo bar"
		System.out.println(collapseWhitespace("foo    bar"));
		//contains result => true
		System.out.println(contains("foo bar","foo"));
		// result => true
		System.out.println(contains("foo bar","FOO", false));
		//containsAll // result => true   result => true
		System.out.println(containsAll("foo bar", new String[]{"foo", "bar"}));
		System.out.println(containsAll("foo bar", new String[]{"FOO", "bar"},false));
		//containsAny  result => true
		System.out.println(containsAny("bar foo", new String[]{"FOO", "BAR", "Test"}, true));
		//countSubstr result => 2  result => 3
		System.out.println(countSubstr("aaaAAAaaa", "aaa"));
		System.out.println(countSubstr("aaaAAAaaa", "aaa", false, false));
		//endsWith result => true result => true
		System.out.println(endsWith("foo bar", "bar"));
		System.out.println(endsWith("foo Bar", "BAR", false));
		//
	}
}
