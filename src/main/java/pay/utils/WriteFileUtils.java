package pay.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WriteFileUtils {
	protected static Logger logger = LoggerFactory.getLogger("WriteFileUtils");

	public static void writeFile(String fileName, String dateFormat,
			String path, String log, Long cts) {
		String pathName = fileName
				+ DateUtil.transferLongToDate(dateFormat, cts) + ".txt";
		Path filePath = Paths.get(path + pathName);

		if (!Files.exists(filePath)) {
			try {
				StringBuilder fileStr = new StringBuilder();
				fileStr.append(log);
				// fileStr.append("/n");
				Files.write(filePath, fileStr.toString().getBytes("UTF-8"));
			} catch (IOException e) {
				logger.error("首次写文件失败！", e);
			}
		} else {// 存在则追加
			try {
				Files.write(filePath, log.getBytes("UTF-8"),
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				logger.error("追加写文件失败！", e);
			}

		}

	}
}
