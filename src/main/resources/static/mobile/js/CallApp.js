/**
 * Created by aa on 2016/8/19.
 */
function CallApp(){

    function setupWebViewJavascriptBridge(callback) {
        if (window.WebViewJavascriptBridge) { return callback(WebViewJavascriptBridge); }
        if (window.WVJBCallbacks) { return window.WVJBCallbacks.push(callback); }
        window.WVJBCallbacks = [callback];
        var WVJBIframe = document.createElement('iframe');
        WVJBIframe.style.display = 'none';
        WVJBIframe.src = 'wvjbscheme://__BRIDGE_LOADED__';
        document.documentElement.appendChild(WVJBIframe);
        setTimeout(function() { document.documentElement.removeChild(WVJBIframe) }, 0)
    }

    //安卓成功接口
    this.callAndroidSuccessOperate=function(result){
        crossFire.notifyAndroid(result);
    };

    //ios成功接口
    this.callIOSSuccessOperate=function(result){
        setupWebViewJavascriptBridge(function(bridge) {
            bridge.callHandler('notifyIOS',result, function responseCallback(responseData) {
            })
        });
    };
}