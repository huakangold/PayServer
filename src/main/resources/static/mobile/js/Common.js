/**
 * Created by aa on 2016/5/13.
 */
var UrlInformation={
    protocol:window.location.protocol,//协议
    host:window.location.host,//主机部分
    port:window.location.port,//端口
    pathname:window.location.pathname,//文件地址
    search: window.location.search,//问号以及之后的地址
    fullhost:this.window.location.protocol+'//'+window.location.host,
    apipath:window.location.hostname=='localhost'||window.location.hostname=='59.107.26.181'||window.location.hostname=='10.1.13.147'||window.location.hostname=='10.1.13.146'?'https://pays.hkfsvip.com':'',//测试地址和正式地址切换
    formAction:window.location.hostname=='localhost'||window.location.hostname=='59.107.26.181'||window.location.hostname=='10.1.13.147'||window.location.hostname=='10.1.13.146'?'https://jzh-test.fuiou.com/jzh/app/':'https://jzh.fuiou.com/app/'//测试地址和正式金账户表单提交地址切换
};

var browser={
    versions:function(){
        var u = navigator.userAgent, app = navigator.appVersion;
        return {
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1,//火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
            iPhone: u.indexOf('iPhone') > -1 , //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
            weiXin:(function (){
                var ua = window.navigator.userAgent.toLowerCase();
                if(ua.match(/MicroMessenger/i) == 'micromessenger'){
                    return true;
                }else{
                    return false;
                }
            })()
        };
    }(),
    language:(navigator.browserLanguage || navigator.language).toLowerCase()
};

//通用jquery ajax操作
function commonAjax (url, datastr, callback) {
    $.ajax({
        url: url,
        type: "POST",
        contentType: 'application/json',
        data: datastr,
        success: callback,
        error: function (error) {
            alert(error);
        }
    });
}

//获取参数
function getParameters(){
    var parameters=UrlInformation.search.replace('?','');
    var parameterArray=parameters.split('&');
    var parameterResult=[];
    for(var i=0;i<parameterArray.length;i++){
        var result=parameterArray[i].split('=');
        parameterResult.push(result);
    }
    return parameterResult;
}

//获取参数，返回对象
function getParametersObj(){
    var parameters=UrlInformation.search.replace('?','');
    var parameterArray=parameters.split('&');
    var parameterResult={};
    for(var i=0;i<parameterArray.length;i++){
        var result=parameterArray[i].split('=');
        parameterResult[result[0]]=result[1];
    }
    return parameterResult;
}
