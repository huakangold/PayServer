/**
 * Created by aa on 2016/8/24.
 */
function DataOperation(){
    var parameters = getParametersObj();
    var postData=JSON.stringify({id:parameters.userId});
    this.checkBalance=function(callback){
        var data={userId:parameters.userId,apiKey:'bafc6f9a-b5cd-4246-968c-fce1e1a30122'};
        var encryptedOperation=new EncryptedOperation();
        var encryptedData=encryptedOperation.getEncryptedObject(data,'$1$27U1vBzc$XMO6PoparDqQo/329yrme0');

        $.ajax({
            url:UrlInformation.apipath+'/payServer/app/checkBalance',
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify(encryptedData),
            success: function(data){
                var result=JSON.parse(data);
                if(result.resultCode=='200'){
                    callback(result.result.ca_balance);
                }
                else{
                    layer.open({
                        content:'<div class="error">'+result.msg+'</div>',
                        time:2
                    });
                }
            },
            error: function (error) {
                layer.open({
                    content:'<div class="error">获取数据出错，请稍后再试！</div>',
                    time:2
                });
            }
        });
    };

    this.getPaymentByUser=function(callback){
        $.ajax({
            url:UrlInformation.apipath+'/payServer/app/getPaymentByUser',
            type: "POST",
            contentType: 'application/json',
            data: postData,
            success: function(data){
                var result=JSON.parse(data);
                if(result.resultCode=='200'){
                    callback(result.result);
                }
                else{
                    layer.open({
                        content:'<div class="error">'+result.msg+'</div>',
                        time:2
                    });
                }
            },
            error: function (error) {
                layer.open({
                    content:'<div class="error">获取数据出错，请稍后再试！</div>',
                    time:2
                });
            }
        });
    };
    //获取rsa加密串
    this.getSign=function(data,callback){
        var realUrl=UrlInformation.apipath+'/payServer/app/getSign?str='+data;
        $.ajax({
            url:realUrl,
            type: "GET",
            contentType: 'application/json',
            success: function(data){
                var result=JSON.parse(data);
                if(result.resultCode=='200'){
                    callback(result.result);
                }
                else{
                    layer.open({
                        content:'<div class="error">'+result.msg+'</div>',
                        time:2
                    });
                }
            },
            error: function (error) {
                layer.open({
                    content:'<div class="error">获取数据出错，请稍后再试！</div>',
                    time:2
                });
            }
        });
    };

    this.getPostSign=function(data,callback){
        $.ajax({
            url:UrlInformation.apipath+'/payServer/app/getPostSign',
            /*url:'http://10.1.13.154:8082/payServer/app/getPostSign',*/
            type: "POST",
            contentType: 'application/json',
            data: JSON.stringify({token:data}),
            success: function(data){
                var result=JSON.parse(data);
                if(result.resultCode=='200'){
                    callback(result.result);
                }
                else{
                    layer.open({
                        content:'<div class="error">'+result.msg+'</div>',
                        time:2
                    });
                }
            },
            error: function (error) {
                layer.open({
                    content:'<div class="error">获取数据出错，请稍后再试！</div>',
                    time:2
                });
            }
        });
    }


}