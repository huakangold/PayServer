/**
 * Created by aa on 2016/8/22.
 */
$(function(){
    var money=0;
    var dataOperation=new DataOperation();
    $('#withdraw').on('input',function(event){
        if($('#withdraw').val()!=''){
            $('#commit').removeAttr('disabled');
            //转化为以分为单位
            money=$('#withdraw').val()*100;
        }
        else{
            $('#commit').attr('disabled','disabled');
        }
    });

    $('#commit').on('click',function(){
        layer.open({
            type: 2
        });
        var parameters = getParametersObj();
        var formOperation=new FormOperation();
        var submitForm = formOperation.getNewSubmitForm();
        formOperation.createNewFormElement(submitForm, "mchnt_cd", decodeURIComponent(parameters.mchntCd));
        formOperation.createNewFormElement(submitForm, "mchnt_txn_ssn", decodeURIComponent(parameters.mchntTxnSsn));
        formOperation.createNewFormElement(submitForm, "login_id", decodeURIComponent(parameters.loginId));
        formOperation.createNewFormElement(submitForm, "amt", money);
        formOperation.createNewFormElement(submitForm, "page_notify_url", decodeURIComponent(parameters.pageNotifyUrl));
        formOperation.createNewFormElement(submitForm, "back_notify_url", "");
        dataOperation.getPostSign(money+'||'+decodeURIComponent(parameters.loginId)+'|'+decodeURIComponent(parameters.mchntCd)+'|'+decodeURIComponent(parameters.mchntTxnSsn)+'|'+decodeURIComponent(parameters.pageNotifyUrl),function(result){
            formOperation.createNewFormElement(submitForm, "signature", result);
            submitForm.action =  UrlInformation.formAction+"500003.action";
            submitForm.submit();
        });
      //  console.log(decodeURIComponent(parameters.mchntCd)+'|'+decodeURIComponent(parameters.mchntTxnSsn));
        /*console.log(money+'|'+decodeURIComponent(parameters.backNotifyUrl)+'|'+decodeURIComponent(parameters.loginId)+'|'+decodeURIComponent(parameters.mchntCd)+'|'+decodeURIComponent(parameters.mchntTxnSsn)+'|'+decodeURIComponent(parameters.pageNotifyUrl));*/
        //console.log(money+'||'+decodeURIComponent(parameters.loginId)+'|'+decodeURIComponent(parameters.mchntCd)+'|'+decodeURIComponent(parameters.mchntTxnSsn)+'|'+decodeURIComponent(parameters.pageNotifyUrl));

    });

    //初始化余额
    dataOperation.checkBalance(function(result){
        $('#balance').val(result);
    });
    //初始化银行卡信息
    dataOperation.getPaymentByUser(function(result){
        $('#card').val(result.bankName+" 尾号"+result.paymentNo.substr(result.paymentNo.length-4,4));
    });

});