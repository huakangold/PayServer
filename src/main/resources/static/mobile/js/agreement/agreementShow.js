/**
 * 产品修改js
 */
var agreementShow={
	getAgreement:function(){
		var dateUrl =UrlInformation.apipath+ '/common/agreementCtrl/list';
		var name = "华财通平台注册协议";
		var params = {
			'curPage' : 1,
			'pageSize' : 99999,
			'name' : name
		};
		
		$.ajax({
			type: "POST",
			async : false,
			url : dateUrl,
			contentType : 'application/json',
			data : JSON.stringify(params),
			dataType : 'json',
			timeout : 30000, // 30秒超时
			success : function(result, textStatus) {
				if (result.resultCode == 200) {
					$("body").html(result.result[0].agreementText);
				}
			},
			error : function(result) {
				//alert(result.status+'请求错误，请稍后再试！');
			}
		});
	},
	
	init:function(){
		agreementShow.getAgreement();
	}
}

$(document).ready(function() {
	agreementShow.init();
});