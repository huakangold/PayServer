/**
 * Created by aa on 2016/8/12.
 */
window.onload = function () {
//加载动画
    layer.open({
        type: 2
    });
    var parameters = getParametersObj();
    var formOperation=new FormOperation();
    var submitForm = formOperation.getNewSubmitForm();
    formOperation.createNewFormElement(submitForm, "mchnt_cd", decodeURIComponent(parameters.mchntCd));
    formOperation.createNewFormElement(submitForm, "mchnt_txn_ssn", decodeURIComponent(parameters.mchntTxnSsn));
    formOperation.createNewFormElement(submitForm, "login_id", decodeURIComponent(parameters.loginId));
    formOperation.createNewFormElement(submitForm, "page_notify_url", decodeURIComponent(parameters.pageNotifyUrl));
    formOperation.createNewFormElement(submitForm, "signature", decodeURIComponent(parameters.signature));

    submitForm.action = UrlInformation.formAction+"appChangeCard.action";

    submitForm.submit();

    //console.log(decodeURIComponent(parameters.loginId)+'|'+decodeURIComponent(parameters.mchntCd)+'|'+decodeURIComponent(parameters.mchntTxnSsn)+'|'+decodeURIComponent(parameters.pageNotifyUrl));

};