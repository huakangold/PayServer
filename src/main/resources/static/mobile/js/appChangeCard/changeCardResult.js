/**
 * Created by aa on 2016/8/19.
 */
window.onload=function(){
    var result=getParametersObj();
    layer.open({
        content:'<div class="error">'+decodeURIComponent(result.responseMsg)+'</div>',
        time:4,
        end:function(){
            var callApp=new CallApp();
            var resultData=JSON.stringify(result);
            if(browser.versions.android){
                callApp.callAndroidSuccessOperate(resultData)
            }
            else if(browser.versions.iPhone||browser.versions.iPad){
                callApp.callIOSSuccessOperate(resultData);
            }
        }
    });
};