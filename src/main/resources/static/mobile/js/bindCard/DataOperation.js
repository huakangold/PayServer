/**
 * Created by aa on 2016/8/9.
 */
function DataOperation() {

    //获取token
    function getToken(userId, callBack) {
        $.ajax({
            url: UrlInformation.apipath + '/payServer/app/getTokenRandom?userId=' + userId,
            /*url:'http://10.1.13.154:8082/payServer/app/getTokenRandom?userId='+userId,*/
            /* url:'https://120.76.206.9/payServer/app/getTokenRandom?userId='+userId,*/
            type: "GET",
            contentType: 'application/json',
            success: function (data) {
                var result = JSON.parse(data);
                if (result.resultCode == "200") {
                    callBack(result.result);
                }
                else {
                    layer.closeAll();
                    layer.open({
                        content: '<div class="error">获取数据出错，请稍后再试！</div>',
                        time: 2
                    });
                }
            },
            error: function (error) {
                layer.closeAll();
                layer.open({
                    content: '<div class="error">获取数据出错，请稍后再试！</div>',
                    time: 2
                });
            }
        })
    }

    //根据参数获取初始值
    this.getInitData = function (callBack) {
        //通过参数获取初始值
        var initData = getParametersObj();
        //通过bankName获取bankCode
        if (initData.bankName) {
            this.getBanks(function (result) {
                for (var bank in result) {
                    if (decodeURIComponent(initData.bankName) === result[bank].bankName) {
                        initData.bankCode = result[bank].bankCode;
                        callBack(initData);
                        break;
                    }
                }
            });
        }
        else{
            callBack(initData);
        }
    };

    //获取省份列表
    this.getProvinces = function (callBack) {
        var self = this;
        $.ajax({
            url: UrlInformation.apipath + '/payServer/mgr/locationCtrl/getProList',
            // url:'http://59.107.26.181:8082/test/testJpa',
            type: "GET",
            contentType: 'application/json',
            success: function (data) {
                var result = JSON.parse(data).result;
                // var result=data.result;
                self.provinceCache = result;
                callBack(result);
            },
            error: function (error) {

            }
        });
        /* $.get(UrlInformation.apipath+'/mgr/locationCtrl/getProList',function(data){
         var result=JSON.parse(data).result;
         self.provinceCache=result;
         callBack(result);
         });*/
    };

    //获取城市列表
    this.getCitys = function (proCode, callBack) {
        var parameters = JSON.stringify({proCode: proCode});
        $.ajax({
            url: UrlInformation.apipath + '/payServer/mgr/locationCtrl/getCityListByProCode',
            type: "POST",
            contentType: 'application/json',
            data: parameters,
            success: function (data) {
                var result = JSON.parse(data).result;
                // var result=data.result;
                callBack(result);
            },
            error: function (error) {
                layer.open({
                    content: '<div class="error">获取数据出错，请稍后再试！</div>',
                    time: 2
                });
            }
        });
    };

    //获取银行列表
    this.getBanks = function (callBack) {
        var self = this;
        $.ajax({
            url: UrlInformation.apipath + '/payServer/app/getBankList',
            type: "GET",
            contentType: 'application/json',
            success: function (data) {
                var result = JSON.parse(data).result;
                // var result=data.result;
                self.bankCache = result;
                if (callBack) {
                    callBack(result);
                }
            },
            error: function (error) {
                layer.open({
                    content: '<div class="error">获取数据出错，请稍后再试！</div>',
                    time: 2
                });
            }

        });
    };

    //根据编码获取银行名称
    this.getBankNameByCode = function (code, callBack) {
        function find(result) {
            for (var bank in result) {
                if (code === result[bank].bankCode) {
                    return result[bank].bankName;
                }
            }
        }

        if (this.bankCache) {
            callBack(find(this.bankCache));
        }
        else {
            this.getBanks(function (result) {
                callBack(find(result));
            })
        }
    };

    //给app客户端发送错误信息
    var sendErrorToApp = function () {
        var callApp = new CallApp();
        var resultData = 'error';
        if (browser.versions.android) {
            callApp.callAndroidSuccessOperate(resultData)
        }
        else if (browser.versions.iPhone || browser.versions.iPad) {
            callApp.callIOSSuccessOperate(resultData)
        }
    };

    //提交数据
    this.submitData = function (data, callBack) {
        //提交数据前获取token
        getToken(data.userId, function (token) {
            data.token = token;
            var encryptedOperation = new EncryptedOperation();
            var encryptedData = encryptedOperation.getEncryptedObject(data, '$1$27U1vBzc$XMO6PoparDqQo/329yrme0');
            var parameters = JSON.stringify(encryptedData);
            $.ajax({
                url: UrlInformation.apipath + '/payServer/app/kaihu',
                /*    url:'http://10.1.13.154:8082/payServer/app/kaihu',*/
                /* url:'https://120.76.206.9/payServer/app/kaihu',*/
                //url:'http://10.1.13.154:8081/payServer/app/kaihu',
                type: "POST",
                contentType: 'application/json',
                data: parameters,
                success: function (data) {
                    var result = JSON.parse(data);
                    if (result.resultCode == '200') {
                        callBack(result.result);
                    }
                    else {
                        layer.closeAll();
                        layer.open({
                            content: '<div class="error">' + result.msg + '</div>',
                            time: 2,
                            end: sendErrorToApp
                        });
                    }
                },
                error: function (error) {
                    layer.closeAll();
                    layer.open({
                        content: '<div class="error">提交数据出错，请稍后再试！</div>',
                        time: 2,
                        end: sendErrorToApp
                    });
                }
            });
        });
    }


}