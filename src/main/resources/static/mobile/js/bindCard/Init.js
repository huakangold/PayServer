/**
 * Created by aa on 2016/8/10.
 */
function Init(){
    //读取url中的初始化数据
    this.initDom=function(data){
        if(data.userName){
            $('#userName').val(decodeURIComponent(data.userName));
            $('#userName').attr('disabled','disabled');
        }
        if(data.ID){
            $('#ID').val(decodeURIComponent(data.ID));
            $('#ID').attr('disabled','disabled');
        }

        if(data.bankNumber){
            $('#bankNumber').val(decodeURIComponent(data.bankNumber));
            $('#bankNumber').attr('disabled','disabled');
        }

        if(data.bankName){
            $('#userBand .dropDownMsg').html(decodeURIComponent(data.bankName));
            $('#userBand').attr('disabled','disabled');
        }
        $('#userName').focus();
        $('#phoneNumber').html(data.phone);
    }
}