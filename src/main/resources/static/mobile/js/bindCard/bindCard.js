/**
 * Created by aa on 2016/8/8.
 */
$(function () {

    var dataOperattion=new DataOperation();
    var mainData;
    dataOperattion.getInitData(function(result){
        mainData=result;
        //初始化界面显示数据
        var initOperation=new Init();
        initOperation.initDom(mainData);

        //没有初始银行数据时绑定事件
        if(!mainData.bankName) {
            $('#userBand').on('click', function () {
                if (dataOperattion.bankCache) {
                    getBank(dataOperattion.bankCache);
                }
                else {
                    //加载动画
                    layer.open({
                        type: 2
                    });
                    dataOperattion.getBanks(function (result) {
                        layer.close(0);
                        getBank(result);
                    });
                }
            });
        }
    });
   /* dataOperattion.getBanks();*/
    $('#commit').on('click',function(){
        //验证不通过
        if(Object.keys(mainData).length<8){
            layer.open({
                content:'<div class="error">请按要求填写数据！</div>',
                time:2
            });
            return;
        }
        else{
            //加载动画
            layer.open({
                type: 2
            });
            var submitData={
                capAcntNo:mainData.bankNumber,
                certif_id:mainData.ID,
                city_id:mainData.area.cityCode,
                cust_nm:mainData.userName,
                mobile_no:mainData.phone,
                parent_bank_id:mainData.bankCode,
                userId:mainData.userId,
                password:CryptoJS.MD5(mainData.password).toString()
            };
            dataOperattion.submitData(submitData,function(result){
                layer.closeAll();
                layer.open({
                    content:'<div class="error">绑卡成功！</div>',
                    time:2,
                    end:function(){
                        var callApp=new CallApp();
                        var resultData=JSON.stringify(result);
                        if(browser.versions.android){
                            callApp.callAndroidSuccessOperate(resultData)
                        }
                        else if(browser.versions.iPhone||browser.versions.iPad){
                            callApp.callIOSSuccessOperate(resultData)
                        }
                    }
                });
            });
        }
    });

    //获取焦点时提示框消失
    $('input').on('focus',function(e){
        $(e.currentTarget).parent('.item').children('.toolTip').hide();
    });

    //离开时验证输入是否合法
    $('input').on('blur',function(e){
        //检测是否为空
       if($(e.currentTarget).val()==''){
           $(e.currentTarget).parent('.item').children('.toolTip').show();
           delete mainData[$(e.currentTarget).attr('id')];
       }
        else{
           //根据正则表达式检测是否符合要求
           if($(e.currentTarget).attr('userpattern')){
               var pattern=$(e.currentTarget).attr('userpattern');
               var patternTest=new RegExp(pattern,"i");
               if(patternTest.test($(e.currentTarget).val())){
                   $(e.currentTarget).parent('.item').children('.toolTip').hide();
                   mainData[$(e.currentTarget).attr('id')]=$(e.currentTarget).val();
               }
               else{
                   $(e.currentTarget).parent('.item').children('.toolTip').show();
                   delete mainData[$(e.currentTarget).attr('id')];
               }
           }
           else {
               $(e.currentTarget).parent('.item').children('.toolTip').hide();
               mainData[$(e.currentTarget).attr('id')]=$(e.currentTarget).val();
           }
       }
    });

    //接受银行数据之后的操作
    function getBank(result){
        var template = _.template($('#bankTemp').html());
        layer.open({
            content: template({data:result})
        });

        //选择银行后缓存银行
        $('.bank .bankItem').on('click', function (e) {
            $('#userBand .dropDownMsg').html($(e.currentTarget).attr('bankname'));
            mainData.bankCode = $(e.currentTarget).attr('bankCode');
            $('#userBand .dropDownMsg').attr('class', 'dropDownMsg value');
            layer.closeAll();
        });
    }



    //获取省份数据之后的操作
    function getProvinces(result){
        var template = _.template($('#provinceTemp').html());
        layer.open({
            content: template({data: result})
        });

        //选择省份后弹出城市选择框
        $('.area .proItem').on('click',function(e){
            var proCode=$(e.currentTarget).attr('proCode');
            var proName=$(e.currentTarget).attr('proName');
            dataOperattion.getCitys(proCode,function(result){
                layer.closeAll();
                var template = _.template($('#cityTemp').html());
                layer.open({
                    content: template({data: result})
                });

                $('.city .cityItem').on('click',function(e){
                    $('#area .dropDownMsg').html($(e.currentTarget).attr('cityName'));
                    $('#area .dropDownMsg').attr('class','dropDownMsg value');
                    mainData.area={proCode:proCode,cityCode:$(e.currentTarget).attr('cityCode')};
                    /*                       $('#area .dropDownMsg').attr('proCode',proCode);
                     $('#area .dropDownMsg').attr('cityCode',$(e.currentTarget).attr('cityCode'));*/
                    layer.closeAll();
                })

            });
        });
    }

    //弹出省份选择框
    $('#area').on('click', function () {
        if(dataOperattion.provinceCache){
            getProvinces(dataOperattion.provinceCache);
        }
        else {
            layer.open({
                type: 2
            });
            dataOperattion.getProvinces(function (result) {
                layer.close(0);
                getProvinces(result);
            });
        }
    });


});
