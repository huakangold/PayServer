/**
 * Created by aa on 2016/8/22.
 */
function FormOperation(){
    this.getNewSubmitForm =function(){
        var submitForm = document.createElement("FORM");
        submitForm.id="currentForm";
        document.body.appendChild(submitForm);
        submitForm.method = "POST";
        return submitForm;
    };

    //helper function to add elements to the form
    this.createNewFormElement=function(inputForm, elementName, elementValue) {
        var newElement = document.createElement("input");
        newElement.setAttribute("name", elementName);
        newElement.setAttribute("type", "hidden");
        newElement.setAttribute("value", elementValue);
        inputForm.appendChild(newElement);
        return newElement;
    }
}