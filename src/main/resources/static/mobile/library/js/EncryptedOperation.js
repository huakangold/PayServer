/**
 * Created by aa on 2016/8/23.
 */
function EncryptedOperation(){
    //给数据类添加加密串属性
    this.getEncryptedObject=function(obj,passward){
        var sortedKeys=Object.keys(obj).sort();
        var sign='';
        for(var i=0;i<sortedKeys.length;i++){
            //解码后才正则匹配，防止编码后无法识别
            if(decodeURIComponent(obj[sortedKeys[i]]).match(/[\u4e00-\u9fa5]/)){
                sign+='&'+(sortedKeys[i]+'='+decodeURIComponent(obj[sortedKeys[i]]).length);
            }
            else {
                sign += '&'+(sortedKeys[i] + '=' + obj[sortedKeys[i]] );
            }
         /*   if(i!==sortedKeys.length-1){
                sign+='&';
            }*/
        }

        obj.sign=CryptoJS.HmacSHA1(sign,passward).toString(CryptoJS.enc.Base64);
        return obj;
    }
}