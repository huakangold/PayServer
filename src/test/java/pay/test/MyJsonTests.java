//package pay.test;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.boot.test.WebIntegrationTest;
//import org.springframework.context.annotation.Bean;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.client.RestTemplate;
//
//import pay.Application;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebIntegrationTest("server.port:8082")
//public class MyJsonTests {
//	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//	 @Bean
//	  public RestTemplate restTemplate() {
//	      return new RestTemplate();
//	  }
//
//
//	 @Test
//	 public void testDeserialize() throws Exception {
//		 String url = "http://localhost:8082/testPost";
//		  MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
//	      map.add("bankName", "华康测试银行");
//	      map.add("bankCode", "007");
//
//	     String postResut = this.restTemplate().postForObject(url, map, String.class);
//		 logger.info("test  post resulst:{}" , postResut);
//
//
//		 String body = this.restTemplate().getForObject("http://localhost:8082/payServer/app/getBankList", String.class);
//		 System.out.println(body);
//	 }
//}
