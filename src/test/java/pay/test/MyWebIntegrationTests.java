//package pay.test;
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.SpringApplicationConfiguration;
//import org.springframework.boot.test.TestRestTemplate;
//import org.springframework.boot.test.WebIntegrationTest;
//import org.springframework.context.annotation.Bean;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.web.client.RestTemplate;
//
//import pay.Application;
//
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
////@WebAppConfiguration
//@WebIntegrationTest("server.port:8082")
//public class MyWebIntegrationTests {
//
//	  @Bean
//	  public RestTemplate restTemplate() {
//	      return new RestTemplate();
//	  }
//
//
//	  @Test
//	  public void exampleTest() {
//		  String body = this.restTemplate().getForObject("http://localhost:8082/", String.class);
//		  System.out.println(body);
//		  Assert.assertTrue(body.equals("Hello World!"));
//
//	  }
//}
