package pay.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pay.Application;
import pay.service.IFYResultCode;
import pay.serviceImpl.FYResultCodeService;

/**
 * Created by lipei on 2017/8/23.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest("server.port:8082")
public class ResultCodeTest {

    public static void main(String[] args) {
        IFYResultCode fyResultCodeService = new FYResultCodeService();
        String msg = fyResultCodeService.getCodeMsg("5002");
        System.out.println("msg = " + msg);

    }
}
